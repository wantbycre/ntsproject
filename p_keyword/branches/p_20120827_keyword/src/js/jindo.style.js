/**
 * @description 
 * @class jindo.Style
 * @names jindo.Style
 * @namespace
 * @author 김형진<hyeongjin.kim@nhn.com>
 * @version 1.0.0
 * @since July 25, 2012 
 * @copyright Copyright (c) 2012, NHN Technology Services inc. 
 */
jindo.Style = jindo.$Class({	
	/**
	 * @
	 * @private
	 * @type Wrapping Element
	 */
	_welWrap : null,
	
	/**
     * @description $init.
	 * @public
	 * @constructs
	 */
	$init : function() {		
		this._initElement();
		this._initEvent();
		this._setWrapWide(this._getDocumentSize());		
	},

	/**
     * @description 엘리먼트 설정
	 * @private
	 */
	_initElement : function() {
		this._welWrap = $Element("wrap");
	},

	/**
     * @description 이벤트 설정
	 * @private
	 */
	_initEvent : function() {
		jindo.$Fn(this._onResizeWindow, this).attach(window, "resize");		
	},

	/**
     * @description 화면 사이즈가 변동시 이벤트 발생
	 * @private
	 */
	_onResizeWindow : function(we) {		
		this._setWrapWide(this._getDocumentSize());
	},
	
	/**
     * @description 웹 문서 사이즈 가로 세로 너비를 반환
	 * @private
	 */
	_getDocumentSize : function(){
		return $Document(document).clientSize();
	},
	
	/**
     * @description 가로너비 1280px 이상 class="wrap_wide" 추가
	 * @private
	 */
	_setWrapWide : function(oSize){		
		if(oSize.width >= 1280){
			this._welWrap.addClass('wrap_wide');
		}else{
			this._welWrap.removeClass('wrap_wide');
		}
	}
});