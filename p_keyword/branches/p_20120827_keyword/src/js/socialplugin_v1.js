if (typeof console == "undefined" || typeof console.log == "undefined") {
	var console = { log: function() {}};
}

/******************************************* me2p* ******************************************/

if(typeof me2p=="undefined"){me2p={}}if(typeof me2p_config_charset=="undefined"){me2p_config_charset="utf-8"}if(typeof me2p_config_isMobile=="undefined"){me2p_config_isMobile=false}if(typeof me2p_config_autoRender=="undefined"){me2p_config_autoRender=false}if(typeof me2p_config_loginProxyDomain=="undefined"){me2p_config_loginProxyDomain="plugin.me2day.net"}if(typeof me2p_config_debug=="undefined"){me2p_config_debug=false}me2p.ElementClassNames={BUTTON_CONTAINER:"me2button_container",BUTTON:"me2button_button",COUNT_BUBBLE:"me2button_count_bubble",COUNT:"me2button_count",DIALOG:"me2button_layer",DIALOG_MESSAGE:"me2button_layer_message",DIALOG_SIGNIN:"me2button_layer_signin",DIALOG_CLOSE:"me2button_layer_close",DIALOG_WRITE_FORM:"me2button_post_write_form",DIALOG_PROFILE_IMG:"me2button_profile_img",DIALOG_WRITE_INPUT:"me2button_write_input",DIALOG_PINGBACK_CHECKBOX:"me2button_pingback_checkbox",DIALOG_TEXT_LENGTH:"me2button_text_length",DIALOG_SUBMIT_BUTTON:"me2button_submit_button",LAYER_INFO_LINK:"me2button_layer_info_link"};me2p.AttributeNames={URL:"href",PLUGIN_KEY:"plugin_key",BEFORE_METOO_CLASS:"before_metoo_class",LOADING_METOO_CLASS:"loading_metoo_class",AFTER_METOO_CLASS:"after_metoo_class",DIALOG_ICON_CLASS_SUCCESS:"success_icon_class",DIALOG_ICON_CLASS_ERROR:"error_icon_class"};if(!window.console){window.console={};console.log=function(){};console.warn=function(){};console.error=function(){}}me2p.log=function(a){if(me2p_config_debug){console.log(a)}};(function(){var o=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,j="sizcache"+(Math.random()+"").replace(".",""),p=0,s=Object.prototype.toString,h=false,g=true,r=/\\/g,v=/\r\n/g,x=/\W/;[0,0].sort(function(){g=false;return 0});var d=function(C,e,F,G){F=F||[];e=e||document;var I=e;if(e.nodeType!==1&&e.nodeType!==9){return[]}if(!C||typeof C!=="string"){return F}var z,K,N,y,J,M,L,E,B=true,A=d.isXML(e),D=[],H=C;do{o.exec("");z=o.exec(H);if(z){H=z[3];D.push(z[1]);if(z[2]){y=z[3];break}}}while(z);if(D.length>1&&k.exec(C)){if(D.length===2&&l.relative[D[0]]){K=t(D[0]+D[1],e,G)}else{K=l.relative[D[0]]?[e]:d(D.shift(),e);while(D.length){C=D.shift();if(l.relative[C]){C+=D.shift()}K=t(C,K,G)}}}else{if(!G&&D.length>1&&e.nodeType===9&&!A&&l.match.ID.test(D[0])&&!l.match.ID.test(D[D.length-1])){J=d.find(D.shift(),e,A);e=J.expr?d.filter(J.expr,J.set)[0]:J.set[0]}if(e){J=G?{expr:D.pop(),set:m(G)}:d.find(D.pop(),D.length===1&&(D[0]==="~"||D[0]==="+")&&e.parentNode?e.parentNode:e,A);K=J.expr?d.filter(J.expr,J.set):J.set;if(D.length>0){N=m(K)}else{B=false}while(D.length){M=D.pop();L=M;if(!l.relative[M]){M=""}else{L=D.pop()}if(L==null){L=e}l.relative[M](N,L,A)}}else{N=D=[]}}if(!N){N=K}if(!N){d.error(M||C)}if(s.call(N)==="[object Array]"){if(!B){F.push.apply(F,N)}else{if(e&&e.nodeType===1){for(E=0;N[E]!=null;E++){if(N[E]&&(N[E]===true||N[E].nodeType===1&&d.contains(e,N[E]))){F.push(K[E])}}}else{for(E=0;N[E]!=null;E++){if(N[E]&&N[E].nodeType===1){F.push(K[E])}}}}}else{m(N,F)}if(y){d(y,I,F,G);d.uniqueSort(F)}return F};d.uniqueSort=function(y){if(q){h=g;y.sort(q);if(h){for(var e=1;e<y.length;e++){if(y[e]===y[e-1]){y.splice(e--,1)}}}}return y};d.matches=function(e,y){return d(e,null,null,y)};d.matchesSelector=function(e,y){return d(y,null,null,[e]).length>0};d.find=function(E,e,F){var D,z,B,A,C,y;if(!E){return[]}for(z=0,B=l.order.length;z<B;z++){C=l.order[z];if((A=l.leftMatch[C].exec(E))){y=A[1];A.splice(1,1);if(y.substr(y.length-1)!=="\\"){A[1]=(A[1]||"").replace(r,"");D=l.find[C](A,e,F);if(D!=null){E=E.replace(l.match[C],"");break}}}}if(!D){D=typeof e.getElementsByTagName!=="undefined"?e.getElementsByTagName("*"):[]}return{set:D,expr:E}};d.filter=function(I,H,L,B){var D,e,G,N,K,y,A,C,J,z=I,M=[],F=H,E=H&&H[0]&&d.isXML(H[0]);while(I&&H.length){for(G in l.filter){if((D=l.leftMatch[G].exec(I))!=null&&D[2]){y=l.filter[G];A=D[1];e=false;D.splice(1,1);if(A.substr(A.length-1)==="\\"){continue}if(F===M){M=[]}if(l.preFilter[G]){D=l.preFilter[G](D,F,L,M,B,E);if(!D){e=N=true}else{if(D===true){continue}}}if(D){for(C=0;(K=F[C])!=null;
C++){if(K){N=y(K,D,C,F);J=B^N;if(L&&N!=null){if(J){e=true}else{F[C]=false}}else{if(J){M.push(K);e=true}}}}}if(N!==undefined){if(!L){F=M}I=I.replace(l.match[G],"");if(!e){return[]}break}}}if(I===z){if(e==null){d.error(I)}else{break}}z=I}return F};d.error=function(e){throw new Error("Syntax error, unrecognized expression: "+e)};var b=d.getText=function(B){var z,A,e=B.nodeType,y="";if(e){if(e===1||e===9){if(typeof B.textContent==="string"){return B.textContent}else{if(typeof B.innerText==="string"){return B.innerText.replace(v,"")}else{for(B=B.firstChild;B;B=B.nextSibling){y+=b(B)}}}}else{if(e===3||e===4){return B.nodeValue}}}else{for(z=0;(A=B[z]);z++){if(A.nodeType!==8){y+=b(A)}}}return y};var l=d.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,CLASS:/\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/},leftMatch:{},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(e){return e.getAttribute("href")},type:function(e){return e.getAttribute("type")}},relative:{"+":function(D,y){var A=typeof y==="string",C=A&&!x.test(y),E=A&&!C;if(C){y=y.toLowerCase()}for(var z=0,e=D.length,B;z<e;z++){if((B=D[z])){while((B=B.previousSibling)&&B.nodeType!==1){}D[z]=E||B&&B.nodeName.toLowerCase()===y?B||false:B===y}}if(E){d.filter(y,D,true)}},">":function(D,y){var C,B=typeof y==="string",z=0,e=D.length;if(B&&!x.test(y)){y=y.toLowerCase();for(;z<e;z++){C=D[z];if(C){var A=C.parentNode;D[z]=A.nodeName.toLowerCase()===y?A:false}}}else{for(;z<e;z++){C=D[z];if(C){D[z]=B?C.parentNode:C.parentNode===y}}if(B){d.filter(y,D,true)}}},"":function(A,y,C){var B,z=p++,e=u;if(typeof y==="string"&&!x.test(y)){y=y.toLowerCase();B=y;e=a}e("parentNode",y,z,A,B,C)},"~":function(A,y,C){var B,z=p++,e=u;if(typeof y==="string"&&!x.test(y)){y=y.toLowerCase();B=y;e=a}e("previousSibling",y,z,A,B,C)}},find:{ID:function(y,z,A){if(typeof z.getElementById!=="undefined"&&!A){var e=z.getElementById(y[1]);return e&&e.parentNode?[e]:[]}},NAME:function(z,C){if(typeof C.getElementsByName!=="undefined"){var y=[],B=C.getElementsByName(z[1]);for(var A=0,e=B.length;A<e;A++){if(B[A].getAttribute("name")===z[1]){y.push(B[A])}}return y.length===0?null:y}},TAG:function(e,y){if(typeof y.getElementsByTagName!=="undefined"){return y.getElementsByTagName(e[1])}}},preFilter:{CLASS:function(A,y,z,e,D,E){A=" "+A[1].replace(r,"")+" ";if(E){return A}for(var B=0,C;(C=y[B])!=null;B++){if(C){if(D^(C.className&&(" "+C.className+" ").replace(/[\t\n\r]/g," ").indexOf(A)>=0)){if(!z){e.push(C)}}else{if(z){y[B]=false}}}}return false},ID:function(e){return e[1].replace(r,"")},TAG:function(y,e){return y[1].replace(r,"").toLowerCase()},CHILD:function(e){if(e[1]==="nth"){if(!e[2]){d.error(e[0])}e[2]=e[2].replace(/^\+|\s*/g,"");var y=/(-?)(\d*)(?:n([+\-]?\d*))?/.exec(e[2]==="even"&&"2n"||e[2]==="odd"&&"2n+1"||!/\D/.test(e[2])&&"0n+"+e[2]||e[2]);e[2]=(y[1]+(y[2]||1))-0;e[3]=y[3]-0}else{if(e[2]){d.error(e[0])}}e[0]=p++;return e},ATTR:function(B,y,z,e,C,D){var A=B[1]=B[1].replace(r,"");if(!D&&l.attrMap[A]){B[1]=l.attrMap[A]}B[4]=(B[4]||B[5]||"").replace(r,"");if(B[2]==="~="){B[4]=" "+B[4]+" "}return B},PSEUDO:function(B,y,z,e,C){if(B[1]==="not"){if((o.exec(B[3])||"").length>1||/^\w/.test(B[3])){B[3]=d(B[3],null,null,y)}else{var A=d.filter(B[3],y,z,true^C);if(!z){e.push.apply(e,A)}return false}}else{if(l.match.POS.test(B[0])||l.match.CHILD.test(B[0])){return true}}return B},POS:function(e){e.unshift(true);return e}},filters:{enabled:function(e){return e.disabled===false&&e.type!=="hidden"},disabled:function(e){return e.disabled===true},checked:function(e){return e.checked===true},selected:function(e){if(e.parentNode){e.parentNode.selectedIndex
}return e.selected===true},parent:function(e){return !!e.firstChild},empty:function(e){return !e.firstChild},has:function(z,y,e){return !!d(e[3],z).length},header:function(e){return(/h\d/i).test(e.nodeName)},text:function(z){var e=z.getAttribute("type"),y=z.type;return z.nodeName.toLowerCase()==="input"&&"text"===y&&(e===y||e===null)},radio:function(e){return e.nodeName.toLowerCase()==="input"&&"radio"===e.type},checkbox:function(e){return e.nodeName.toLowerCase()==="input"&&"checkbox"===e.type},file:function(e){return e.nodeName.toLowerCase()==="input"&&"file"===e.type},password:function(e){return e.nodeName.toLowerCase()==="input"&&"password"===e.type},submit:function(y){var e=y.nodeName.toLowerCase();return(e==="input"||e==="button")&&"submit"===y.type},image:function(e){return e.nodeName.toLowerCase()==="input"&&"image"===e.type},reset:function(y){var e=y.nodeName.toLowerCase();return(e==="input"||e==="button")&&"reset"===y.type},button:function(y){var e=y.nodeName.toLowerCase();return e==="input"&&"button"===y.type||e==="button"},input:function(e){return(/input|select|textarea|button/i).test(e.nodeName)},focus:function(e){return e===e.ownerDocument.activeElement}},setFilters:{first:function(y,e){return e===0},last:function(z,y,e,A){return y===A.length-1},even:function(y,e){return e%2===0},odd:function(y,e){return e%2===1},lt:function(z,y,e){return y<e[3]-0},gt:function(z,y,e){return y>e[3]-0},nth:function(z,y,e){return e[3]-0===y},eq:function(z,y,e){return e[3]-0===y}},filter:{PSEUDO:function(z,E,D,F){var e=E[1],y=l.filters[e];if(y){return y(z,D,E,F)}else{if(e==="contains"){return(z.textContent||z.innerText||b([z])||"").indexOf(E[3])>=0}else{if(e==="not"){var A=E[3];for(var C=0,B=A.length;C<B;C++){if(A[C]===z){return false}}return true}else{d.error(e)}}}},CHILD:function(z,B){var A,H,D,G,e,C,F,E=B[1],y=z;switch(E){case"only":case"first":while((y=y.previousSibling)){if(y.nodeType===1){return false}}if(E==="first"){return true}y=z;case"last":while((y=y.nextSibling)){if(y.nodeType===1){return false}}return true;case"nth":A=B[2];H=B[3];if(A===1&&H===0){return true}D=B[0];G=z.parentNode;if(G&&(G[j]!==D||!z.nodeIndex)){C=0;for(y=G.firstChild;y;y=y.nextSibling){if(y.nodeType===1){y.nodeIndex=++C}}G[j]=D}F=z.nodeIndex-H;if(A===0){return F===0}else{return(F%A===0&&F/A>=0)}}},ID:function(y,e){return y.nodeType===1&&y.getAttribute("id")===e},TAG:function(y,e){return(e==="*"&&y.nodeType===1)||!!y.nodeName&&y.nodeName.toLowerCase()===e},CLASS:function(y,e){return(" "+(y.className||y.getAttribute("class"))+" ").indexOf(e)>-1},ATTR:function(C,A){var z=A[1],e=d.attr?d.attr(C,z):l.attrHandle[z]?l.attrHandle[z](C):C[z]!=null?C[z]:C.getAttribute(z),D=e+"",B=A[2],y=A[4];return e==null?B==="!=":!B&&d.attr?e!=null:B==="="?D===y:B==="*="?D.indexOf(y)>=0:B==="~="?(" "+D+" ").indexOf(y)>=0:!y?D&&e!==false:B==="!="?D!==y:B==="^="?D.indexOf(y)===0:B==="$="?D.substr(D.length-y.length)===y:B==="|="?D===y||D.substr(0,y.length+1)===y+"-":false},POS:function(B,y,z,C){var e=y[2],A=l.setFilters[e];if(A){return A(B,z,y,C)}}}};var k=l.match.POS,c=function(y,e){return"\\"+(e-0+1)};for(var f in l.match){l.match[f]=new RegExp(l.match[f].source+(/(?![^\[]*\])(?![^\(]*\))/.source));l.leftMatch[f]=new RegExp(/(^(?:.|\r|\n)*?)/.source+l.match[f].source.replace(/\\(\d+)/g,c))}var m=function(y,e){y=Array.prototype.slice.call(y,0);if(e){e.push.apply(e,y);return e}return y};try{Array.prototype.slice.call(document.documentElement.childNodes,0)[0].nodeType}catch(w){m=function(B,A){var z=0,y=A||[];if(s.call(B)==="[object Array]"){Array.prototype.push.apply(y,B)}else{if(typeof B.length==="number"){for(var e=B.length;z<e;z++){y.push(B[z])}}else{for(;B[z];z++){y.push(B[z])}}}return y}}var q,n;if(document.documentElement.compareDocumentPosition){q=function(y,e){if(y===e){h=true;return 0}if(!y.compareDocumentPosition||!e.compareDocumentPosition){return y.compareDocumentPosition?-1:1}return y.compareDocumentPosition(e)&4?-1:1}}else{q=function(F,E){if(F===E){h=true;return 0}else{if(F.sourceIndex&&E.sourceIndex){return F.sourceIndex-E.sourceIndex}}var C,y,z=[],e=[],B=F.parentNode,D=E.parentNode,G=B;
if(B===D){return n(F,E)}else{if(!B){return -1}else{if(!D){return 1}}}while(G){z.unshift(G);G=G.parentNode}G=D;while(G){e.unshift(G);G=G.parentNode}C=z.length;y=e.length;for(var A=0;A<C&&A<y;A++){if(z[A]!==e[A]){return n(z[A],e[A])}}return A===C?n(F,e[A],-1):n(z[A],E,1)};n=function(y,e,z){if(y===e){return z}var A=y.nextSibling;while(A){if(A===e){return -1}A=A.nextSibling}return 1}}(function(){var y=document.createElement("div"),z="script"+(new Date()).getTime(),e=document.documentElement;y.innerHTML="<a name='"+z+"'/>";e.insertBefore(y,e.firstChild);if(document.getElementById(z)){l.find.ID=function(B,C,D){if(typeof C.getElementById!=="undefined"&&!D){var A=C.getElementById(B[1]);return A?A.id===B[1]||typeof A.getAttributeNode!=="undefined"&&A.getAttributeNode("id").nodeValue===B[1]?[A]:undefined:[]}};l.filter.ID=function(C,A){var B=typeof C.getAttributeNode!=="undefined"&&C.getAttributeNode("id");return C.nodeType===1&&B&&B.nodeValue===A}}e.removeChild(y);e=y=null})();(function(){var e=document.createElement("div");e.appendChild(document.createComment(""));if(e.getElementsByTagName("*").length>0){l.find.TAG=function(y,C){var B=C.getElementsByTagName(y[1]);if(y[1]==="*"){var A=[];for(var z=0;B[z];z++){if(B[z].nodeType===1){A.push(B[z])}}B=A}return B}}e.innerHTML="<a href='#'></a>";if(e.firstChild&&typeof e.firstChild.getAttribute!=="undefined"&&e.firstChild.getAttribute("href")!=="#"){l.attrHandle.href=function(y){return y.getAttribute("href",2)}}e=null})();if(document.querySelectorAll){(function(){var e=d,A=document.createElement("div"),z="__sizzle__";A.innerHTML="<p class='TEST'></p>";if(A.querySelectorAll&&A.querySelectorAll(".TEST").length===0){return}d=function(L,C,G,K){C=C||document;if(!K&&!d.isXML(C)){var J=/^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(L);if(J&&(C.nodeType===1||C.nodeType===9)){if(J[1]){return m(C.getElementsByTagName(L),G)}else{if(J[2]&&l.find.CLASS&&C.getElementsByClassName){return m(C.getElementsByClassName(J[2]),G)}}}if(C.nodeType===9){if(L==="body"&&C.body){return m([C.body],G)}else{if(J&&J[3]){var F=C.getElementById(J[3]);if(F&&F.parentNode){if(F.id===J[3]){return m([F],G)}}else{return m([],G)}}}try{return m(C.querySelectorAll(L),G)}catch(H){}}else{if(C.nodeType===1&&C.nodeName.toLowerCase()!=="object"){var D=C,E=C.getAttribute("id"),B=E||z,N=C.parentNode,M=/^\s*[+~]/.test(L);if(!E){C.setAttribute("id",B)}else{B=B.replace(/'/g,"\\$&")}if(M&&N){C=C.parentNode}try{if(!M||N){return m(C.querySelectorAll("[id='"+B+"'] "+L),G)}}catch(I){}finally{if(!E){D.removeAttribute("id")}}}}}return e(L,C,G,K)};for(var y in e){d[y]=e[y]}A=null})()}(function(){var e=document.documentElement,z=e.matchesSelector||e.mozMatchesSelector||e.webkitMatchesSelector||e.msMatchesSelector;if(z){var B=!z.call(document.createElement("div"),"div"),y=false;try{z.call(document.documentElement,"[test!='']:sizzle")}catch(A){y=true}d.matchesSelector=function(D,F){F=F.replace(/\=\s*([^'"\]]*)\s*\]/g,"='$1']");if(!d.isXML(D)){try{if(y||!l.match.PSEUDO.test(F)&&!/!=/.test(F)){var C=z.call(D,F);if(C||!B||D.document&&D.document.nodeType!==11){return C}}}catch(E){}}return d(F,null,null,[D]).length>0}}})();(function(){var e=document.createElement("div");e.innerHTML="<div class='test e'></div><div class='test'></div>";if(!e.getElementsByClassName||e.getElementsByClassName("e").length===0){return}e.lastChild.className="e";if(e.getElementsByClassName("e").length===1){return}l.order.splice(1,0,"CLASS");l.find.CLASS=function(y,z,A){if(typeof z.getElementsByClassName!=="undefined"&&!A){return z.getElementsByClassName(y[1])}};e=null})();function a(y,D,C,G,E,F){for(var A=0,z=G.length;A<z;A++){var e=G[A];if(e){var B=false;e=e[y];while(e){if(e[j]===C){B=G[e.sizset];break}if(e.nodeType===1&&!F){e[j]=C;e.sizset=A}if(e.nodeName.toLowerCase()===D){B=e;break}e=e[y]}G[A]=B}}}function u(y,D,C,G,E,F){for(var A=0,z=G.length;A<z;A++){var e=G[A];if(e){var B=false;e=e[y];while(e){if(e[j]===C){B=G[e.sizset];break}if(e.nodeType===1){if(!F){e[j]=C;e.sizset=A}if(typeof D!=="string"){if(e===D){B=true;break}}else{if(d.filter(D,[e]).length>0){B=e;break}}}e=e[y]}G[A]=B}}}if(document.documentElement.contains){d.contains=function(y,e){return y!==e&&(y.contains?y.contains(e):true)
}}else{if(document.documentElement.compareDocumentPosition){d.contains=function(y,e){return !!(y.compareDocumentPosition(e)&16)}}else{d.contains=function(){return false}}}d.isXML=function(e){var y=(e?e.ownerDocument||e:0).documentElement;return y?y.nodeName!=="HTML":false};var t=function(z,e,D){var C,E=[],B="",F=e.nodeType?[e]:e;while((C=l.match.PSEUDO.exec(z))){B+=C[0];z=z.replace(l.match.PSEUDO,"")}z=l.relative[z]?z+"*":z;for(var A=0,y=F.length;A<y;A++){d(z,F[A],E,D)}return d.filter(B,E)};window.me2p.Sizzle=d})();me2p.Button=function(b,a){this.isOnLoaded=false;this.isMetooRequesting=false;this._value=b;this.$value=function(){return this._value};this.url=a;this.pluginKey=null;this.elements={};this.elements.button=null;this.elements.countContainer=null;this.elements.count=null;this.elements.dialog=null;this.dialog=null;this.loginHelper=null;this.isPostExist=false;this.$init=function(){this.pluginKey=this.$value().getAttribute(me2p.AttributeNames.PLUGIN_KEY);this.elements.button=me2p.Sizzle("."+me2p.ElementClassNames.BUTTON,this.$value())[0];this.elements.countContainer=me2p.Sizzle("."+me2p.ElementClassNames.COUNT_BUBBLE,this.$value())[0];this.elements.count=me2p.Sizzle("."+me2p.ElementClassNames.COUNT,this.$value())[0];this.elements.dialog=me2p.Sizzle("."+me2p.ElementClassNames.DIALOG,this.$value())[0];this.dialog=new me2p.Dialog(this);this.onClickAction=me2p.Util.fn(function(d){var c=me2p.Service.getWebPageInfo();if(me2p.Service.errorResponse&&me2p.Service.errorResponse.code){this.showError(me2p.Service.errorResponse)}else{this.metoo()}if(!d){return false}if(d.preventDefault){d.preventDefault()}else{d.returnValue=false}return false},this).bind();this.elements.button.onclick=me2p.Util.fn(function(c){if(!me2p.Main.isWebPageInfoRequested){me2p.updateElementsForCallback(this.onClickAction)}else{this.onClickAction()}},this).bind()};this.metoo=function(){var c=me2p.Service.getWebPageInfo();this.isPostExist=c.postInfoMap[this.url];if(this.isMetooRequesting||this.dialog.isOpen()){return false}if(me2p.Service.isLoggedIn()){this.onBeforeMetoo();this.isMetooRequesting=true;this.showLoadingImage();var h={url:this.url,security_token:c.securityToken[this.url]};if(this.pluginKey){h.plugin_key=this.pluginKey}try{if(!c.postInfoMap[this.url].postId){h.title=me2p.Main.title}}catch(d){}var g=me2p.Util.fn(function(l){var j=l;if(j.code=="0"){if(j.result.postInfo&&j.result.postInfo.postId){me2p.Service.updatePostInfo(j.result.postInfo);me2p.MetooHandler.updateAll()}this.onAfterMetoo()}else{if(j.code=="1002"){if(me2p_config_isMobile){me2p.Service.requestWebPageInfo(me2p.Service.webPageInfoParams,me2p.Util.fn(this.login,this).bind())}else{this.login()}this.hideLoadingImage();this.isMetooRequesting=false;return}for(var e in j){me2p.log(e+"="+j[e])}}this.hideLoadingImage();this.showDialog(j);this.isMetooRequesting=false},this).bind();var f=me2p.Util.fn(function(){this.hideLoadingImage();this.isMetooRequesting=false;this.showError()},this).bind();me2p.Service.requestMetoo(h,g,f)}else{this.login()}};this.login=function(){this.loginHelper=(!this.loginHelper)?new me2p.LoginHelper():this.loginHelper;if(me2p_config_isMobile){var c="";var e="";var d=me2p.Service.getWebPageInfo();if(d.securityToken){c=d.securityToken[this.url]||""}e=d.timestamp||"";this.loginHelper.gotoLoginPage({url:this.url,plugin_key:this.pluginKey,redirect_url:location.href,security_token:c,timestamp:e})}else{this.loginHelper.openLoginWindow({pluginType:"button",url:this.url},me2p.Util.fn(this.metoo,this).bind())}};this.showLoadingImage=function(){me2p.Util.addClass(this.elements.button,this.$value().getAttribute(me2p.AttributeNames.LOADING_METOO_CLASS))};this.hideLoadingImage=function(){me2p.Util.removeClass(this.elements.button,this.$value().getAttribute(me2p.AttributeNames.LOADING_METOO_CLASS))};this.showDialog=function(c){this.onBeforeShowLayer();this.dialog.show(c)};this.showError=function(c){if(!c){this.dialog.showDefaultError()}this.dialog.show(c)};this.updateUI=function(c){me2p.Util.addClass(this.elements.button,this.$value().getAttribute(me2p.AttributeNames.BEFORE_METOO_CLASS));
if(!c){c=me2p.Service.getWebPageInfo().postInfoMap[this.url]}if(!c){this.elements.countContainer.style.display="none";return}this._updateCount(c);this._updateMetooStatus(c);if(!this.isOnLoaded){this.isOnLoaded=true;this.onLoad()}};this._updateCount=function(c){var e,f,d;e=c.metooCount;f=c.permalink;if(e>=999500000){d="∞"}else{if(e>=1000000){d=Math.round(parseFloat(e/1000000))+"M"}else{if(e>=10000){d=Math.round(parseFloat(e/1000))+"K"}else{if(e>0){d=me2p.Util.numberWithDelimiter(e)}else{d=0}}}}if(e>0){this.elements.countContainer.style.display="";this.elements.count.innerHTML=me2p.Util.numberWithDelimiter(e)}if(f){this.elements.count.setAttribute("target","_blank");this.elements.count.setAttribute("href",f)}};this._updateMetooStatus=function(c){var d=c.metooStatus;if(d=="0"||d=="1"){this._setMetooed()}};this._setMetooed=function(){me2p.Util.removeClass(this.elements.button,this.$value().getAttribute(me2p.AttributeNames.BEFORE_METOO_CLASS));me2p.Util.addClass(this.elements.button,this.$value().getAttribute(me2p.AttributeNames.AFTER_METOO_CLASS))};this.onLoad=me2p.Util.EmptyFunction;this.onBeforeMetoo=me2p.Util.EmptyFunction;this.onAfterMetoo=me2p.Util.EmptyFunction;this.onBeforeShowLayer=me2p.Util.EmptyFunction;this.onShowLayer=me2p.Util.EmptyFunction;this.onCloseLayer=me2p.Util.EmptyFunction;this.closeLayer=function(){this.dialog.close()};this.registerEvent=function(d,c){this[d]=c};this.$init()};me2p.MetooHandler={buttonList:[],buttonMap:{},pluginKey:null,urls:[],reset:function(){this.buttonList=[];this.buttonMap={};this.pluginKey=null;this.urls=[]},register:function(d,c,a){if(typeof a=="undefined"){a=0}if(!this.buttonMap[c]||!this.buttonMap[c][a]){var b=new me2p.Button(d,c);this.buttonList.push(b);if(!this.buttonMap[c]){this.buttonMap[c]=[]}this.buttonMap[c][a]=b;this.urls.push(c)}else{me2p.log("['"+c+"']["+a+"] has already been registered.")}if(this.buttonMap[c][a]){if(!this.pluginKey){this.pluginKey=this.buttonMap[c][a].pluginKey}}},updateAll:function(){for(var b=0,a=this.buttonList.length;b<a;b++){if(this.buttonList[b]){this.buttonList[b].updateUI()}}},get:function(a){return new this.ButtonHandler(this.buttonMap[a])},getAll:function(){return new this.ButtonHandler(me2p.MetooHandler.buttonList)},ButtonHandler:function(a){this.list=a;this.registerEvent=function(e,d){if(this.list){for(var c=0,b=this.list.length;c<b;c++){if(this.list[c]){this.list[c].registerEvent(e,d)}}}else{console.log("Button Object does not exist.")}},this.closeLayer=function(){for(var c=0,b=this.list.length;c<b;c++){if(this.list[c]){this.list[c].closeLayer()}}}}};me2p.MessageText={"016":"미투데이 플러그인이 정상적으로 작동되고 있지 않습니다.","8000":"내 미투데이 생성에 실패했습니다. 단체회원인 경우 미투데이 생성을 하실 수 없습니다."};me2p.MessageTemplate={M004:'<strong><a href="http://me2day.net/{=userId}" target="_blank">{=nickname}</a>님의 <a href="{=permalink}" target="_blank">포스트</a>에 <a href="http://me2day.net/{=loginUserId}/metoo" target="_blank">미투</a>했습니다.</strong>',M004_1:'<strong><a href="http://me2day.net/{=userId}" target="_blank">{=nickname}</a>님의 <a href="{=permalink}" target="_blank">포스트</a>에 이미 <a href="http://me2day.net/{=loginUserId}/metoo" target="_blank">미투</a>했습니다.</strong>',M009:'<strong><a href="http://me2day.net/{=userId}" target="_blank">{=nickname}</a>님의 <a href="{=permalink}" target="_blank">포스트</a>가 생성되었습니다.</strong>',M009_1:'<strong><a href="http://me2day.net/{=userId}" target="_blank">{=nickname}</a>님의 <a href="{=permalink}" target="_blank">포스트</a>가 이미 생성되었습니다.</strong>',M010:'<a href="http://me2day.net/front/rules" target="_blank">미투데이 운영원칙</a>에 따라 활동 제한 조치중입니다.<br>활동 제한 기간 중에는 해당 활동이 제한됩니다.',M011:'<a href="http://me2day.net/{=ownerUserId}" target="_blank">{=ownerNickname}</a>님이 <a href="http://me2day.net/{=loginUserId}" target="_blank">{=loginNickname}</a>님을 차단했습니다.',M012:'<a href="http://me2day.net/{=ownerUserId}" target="_blank">{=ownerNickname}</a>님은 프로필사진, 휴대폰번호, 한줄소개 등이 등록되어 있지 않은 회원으로부터는 댓글을 받지 않습니다. <a class="'+me2p.ElementClassNames.LAYER_INFO_LINK+'" href="http://me2day.net/{=loginUserId}/setting?open_profile=true" target="_blank">프로필 업데이트 하러 가기</a>',M013:'<strong>미투데이 서비스가 점검 중입니다.</strong>점검시간 동안은 기능 이용이 제한됩니다. <a href="http://me2day.net/me2/blog" target="_blank" class="'+me2p.ElementClassNames.LAYER_INFO_LINK+'">미투데이 공지 보기</a>',M014:"<strong>일시적인 오류가 발생했습니다.</strong>이용에 불편을 드린 점 진심으로 사과드립니다.",M016:"<strong>"+me2p.MessageText["016"]+"</strong>",M019:"<strong>잘못된 요청입니다.</strong> {=message}",M020:"<strong>일시중지 중에는 이용하실 수 없는 기능입니다.</strong>",M021:'나의 미투데이 (<a href="http://me2day.net/{=createdId}" target="_blank" class="'+me2p.ElementClassNames.LAYER_INFO_LINK+'">http://me2day.net/{=createdId}</a>)의 <br /> <a href="http://me2day.net/{=createdId}/metoo" target="_blank" class="me2_post">미투한글</a>에서 확인할 수 있습니다.',M8000:"<strong>"+me2p.MessageText["8000"]+"</strong>"};
me2p.MessageTemplate2={M004:'<strong><a href="{=permalink}" target="_blank">포스트</a>에 <a href="http://me2day.net/{=loginUserId}/metoo" target="_blank">미투</a>했습니다.</strong>',M004_1:'<strong><a href="{=permalink}" target="_blank">포스트</a>에 이미 <a href="http://me2day.net/{=loginUserId}/metoo" target="_blank">미투</a>했습니다.</strong>',M009:"<strong>생성되었습니다.</strong>",M009_1:"<strong>이미 생성되었습니다.</strong>",M010:'<a href="http://me2day.net/front/rules" target="_blank">미투데이 운영원칙</a>에 따라 활동 제한 조치중입니다.<br>활동 제한 기간 중에는 해당 활동이 제한됩니다.',M011:"차단되었기 때문에 미투를 할 수가 없습니다.",M012:'프로필사진, 휴대폰번호, 한줄소개 등이 등록되어 있지 않은 회원으로부터는 댓글을 받지 않습니다. <a class="'+me2p.ElementClassNames.LAYER_INFO_LINK+'" href="http://me2day.net/{=loginUserId}/setting?open_profile=true" target="_blank">프로필 업데이트 하러 가기</a>',M013:'<strong>미투데이 서비스가 점검 중입니다.</strong>점검시간 동안은 기능 이용이 제한됩니다. <a href="http://me2day.net/me2/blog" target="_blank" class="'+me2p.ElementClassNames.LAYER_INFO_LINK+'">미투데이 공지 보기</a>',M014:"<strong>일시적인 오류가 발생했습니다.</strong>이용에 불편을 드린 점 진심으로 사과드립니다.",M016:"<strong>"+me2p.MessageText["016"]+"</strong>",M019:"<strong>잘못된 요청입니다.</strong> {=message}",M020:"<strong>일시중지 중에는 이용하실 수 없는 기능입니다.</strong>",M021:'나의 미투데이 (<a href="http://me2day.net/{=createdId}" target="_blank" class="'+me2p.ElementClassNames.LAYER_INFO_LINK+'">http://me2day.net/{=createdId}</a>)의 <br /> <a href="http://me2day.net/{=createdId}/metoo" target="_blank" class="me2_post">미투한글</a>에서 확인할 수 있습니다.',M8000:"<strong>"+me2p.MessageText["8000"]+"</strong>"};me2p.MessageTemplate_DA={M004:"미투했습니다.",M004_1:"이미 미투했습니다.",M009:"생성되었습니다.",M009_1:"이미 생성되었습니다.",M010:"활동 제한 조치중입니다.",M011:"차단되어 미투를 할 수 없습니다.",M012:"",M013:"미투데이 서비스가 점검 중입니다.",M014:"일시적인 오류가 발생했습니다.",M016:'플러그인이 정상적으로 작동되 않습니다."',M019:"잘못된 요청입니다.",M020:"일시중지 중에는 이용하실 수 없는 기능입니다.",M021:"",M8000:"단체회원인 경우 미투데이 생성을 하실 수 없습니다."};me2p.Dialog=function(a){this._isOpen=false;this.button=a;this.$value=function(){return this.button.elements.dialog};this.elements={};this.elements.signin=null;this.elements.message=null;this.elements.closeBt=null;this.elements.postBox=null;this.postBox=null;this.attr={};this.attr.successIconClass="";this.attr.errorIconClass="";this.isCommentWritable=function(b){if(b){if(b.code=="0"||b.code=="1"||b.code=="2"||b.code=="3"){return true}return false}return false};this.$init=function(){this.elements.message=me2p.Sizzle("."+me2p.ElementClassNames.DIALOG_MESSAGE,this.$value())[0];this.elements.signin=me2p.Sizzle("."+me2p.ElementClassNames.DIALOG_SIGNIN,this.$value())[0];this.elements.closeBt=me2p.Sizzle("."+me2p.ElementClassNames.DIALOG_CLOSE,this.$value())[0];this.elements.postBox=me2p.Sizzle("."+me2p.ElementClassNames.DIALOG_WRITE_FORM,this.$value())[0];this.attr.successIconClass=this.$value().getAttribute(me2p.AttributeNames.DIALOG_ICON_CLASS_SUCCESS);this.attr.errorIconClass=this.$value().getAttribute(me2p.AttributeNames.DIALOG_ICON_CLASS_ERROR);this.elements.closeBt.onclick=me2p.Util.fn(function(){this.close()},this).bind()};this.isOpen=function(){return this._isOpen};this.show=function(b){if(!b){this.showDefaultError();return}this._setIcon(b.code);var f=this._getMessageMarkup(b);this.updateMessage(f);var d=me2p.Service.getWebPageInfo();var e=d.loginUser.createdId;if(e){var c=this._getSigninMarkup(e);this._updateHTML(this.elements.signin,c);this.elements.signin.style.display="";d.loginUser.createdId=null}else{this.elements.signin.style.display="none"}this.$value().style.display="";if(this.isCommentWritable(b)){if(this.elements.postBox){this.postBox=(this.postBox?this.postBox:new me2p.PostBox(this));this.postBox.show()}}this.button.onShowLayer(this.$value());this._isOpen=true};this.updateMessage=function(b){this._updateHTML(this.elements.message,b)};this.close=function(){this.button.onCloseLayer();this.$value().style.display="none";this._isOpen=false};this.showDefaultError=function(){alert(me2p.MessageText["016"])};this._updateHTML=function(b,c){b.innerHTML=c};this._isCommentWritable=function(b){if(b=="0"||b=="1"||b=="2"||b=="3"){return true}else{return false}};this._setIcon=function(b){var c=this.elements.message;if(this._isCommentWritable(b)){me2p.Util.removeClass(c,this.attr.errorIconClass);
me2p.Util.addClass(c,this.attr.successIconClass)}else{me2p.Util.removeClass(c,this.attr.successIconClass);me2p.Util.addClass(c,this.attr.errorIconClass)}};this._getSigninMarkup=function(d){var b=me2p.Util.Template(me2p.MessageTemplate.M021);var c=b.process({createdId:d});return c||""};this._getMessageMarkup=function(d){if(!d){return}var f,b;var g="";var e=me2p.Service.getWebPageInfo();var c=(d.result&&d.result.postInfo)?d.result.postInfo:null;switch(d.code){case"0":if(c){if(c.metooStatus=="0"){b="M004"}else{if(c.metooStatus=="1"){b="M004_1"}else{if(c.metooStatus=="2"){b="M009"}else{if(c.metooStatus=="3"){if(this.button.isPostExist){b="M009_1"}else{b="M009"}}else{b="M016"}}}}if(e.ownerUser){f=me2p.Util.Template(me2p.MessageTemplate[b]);g=f.process({nickname:e.ownerUser.nickname,userId:e.ownerUser.userId,permalink:d.result.postInfo.permalink,loginUserId:e.loginUser.userId})}else{f=me2p.Util.Template(me2p.MessageTemplate2[b]);g=f.process({permalink:d.result.postInfo.permalink,loginUserId:e.loginUser.userId})}}else{g=d.message}break;case"1004":f=me2p.Util.Template(me2p.MessageTemplate.M019);g=f.process({message:d.message});break;case"1105":f=me2p.Util.Template(me2p.MessageTemplate.M020);g=f.process();break;case"1106":f=me2p.Util.Template(me2p.MessageTemplate.M010);g=f.process();break;case"1107":if(e.ownerUser){f=me2p.Util.Template(me2p.MessageTemplate.M011);g=f.process({ownerNickname:e.ownerUser.nickname,ownerUserId:e.ownerUser.userId,loginNickname:e.loginUser.nickname,loginUserId:e.loginUser.userId})}else{f=me2p.Util.Template(me2p.MessageTemplate2.M011);g=f.process()}break;case"1108":if(e.ownerUser){f=me2p.Util.Template(me2p.MessageTemplate.M012);g=f.process({ownerNickname:e.ownerUser.nickname,ownerUserId:e.ownerUser.userId,loginUserId:e.loginUser.userId})}else{f=me2p.Util.Template(me2p.MessageTemplate2.M012);g=f.process()}break;case"3001":g=me2p.Util.Template(me2p.MessageTemplate.M013).process();break;case"1011":case"1201":g=d.message;break;case"1202":case"1203":case"1204":case"1209":g=me2p.Util.Template(me2p.MessageTemplate.M016).process();break;case"8000":g=me2p.Util.Template(me2p.MessageTemplate.M8000).process();break;default:if(d.code>=2000){g=me2p.Util.Template(me2p.MessageTemplate.M014).process()}else{f=me2p.Util.Template(me2p.MessageTemplate.M019);g=f.process({message:d.message})}}return g};this.$init()};me2p.PostBox=function(a){this.dialog=a;this.$value=function(){return this.dialog.elements.postBox};this.elements={};this.elements.profileImage=null;this.elements.writeInput=null;this.elements.pingbackCheckbox=null;this.elements.textLength=null;this.elements.submitButton=null;this.isRequesting=false;this.$init=function(){this.elements.container=this.dialog.elements.postBox;this.elements.profileImage=me2p.Sizzle("."+me2p.ElementClassNames.DIALOG_PROFILE_IMG,this.$value())[0];this.elements.profileImage.src=me2p.Service.getWebPageInfo().loginUser.profileImage;this.elements.writeInput=me2p.Sizzle("."+me2p.ElementClassNames.DIALOG_WRITE_INPUT,this.$value())[0];this.elements.pingbackCheckbox=me2p.Sizzle("."+me2p.ElementClassNames.DIALOG_PINGBACK_CHECKBOX,this.$value())[0];this.elements.textLength=me2p.Sizzle("."+me2p.ElementClassNames.DIALOG_TEXT_LENGTH,this.$value())[0];this.elements.submitButton=me2p.Sizzle("."+me2p.ElementClassNames.DIALOG_SUBMIT_BUTTON,this.$value())[0];this.elements.submitButton.onclick=me2p.Util.fn(function(b){this.submit();if(!b){return false}if(b.preventDefault){b.preventDefault()}else{b.returnValue=false}return false},this).bind()};this.show=function(){this.elements.container.style.display=""};this.close=function(){this.elements.container.style.display="none"};this.submit=function(){if(this.isRequesting){return false}var c=this.elements.writeInput.value;if(c!=""){this.isRequesting=true;var b=c.replace(/\n/g," ");var e=me2p.Service.APIUrl.comment;var d=this.dialog.button;var f=me2p.Service.getWebPageInfo();var j={body:b,plugin_key:d.pluginKey,pingback:this.elements.pingbackCheckbox.checked,url:d.url,security_token:f.securityToken[d.url]};var h=me2p.Util.fn(function(l){var k=l;if(k.code=="0"){var m=me2p.Service.getWebPageInfo();
var n=me2p.Util.Template(me2p.MessageTemplate.M006);var o=n.process({nickname:m.ownerUser.nickname,userId:m.ownerUser.userId,permalink:k.result.postInfo.permalink});this.dialog.updateMessage(o);this.elements.writeInput.value=""}else{this.dialog.updateMessage(k.message)}this.close();this.isRequesting=false},this).bind();var g=me2p.Util.fn(function(){this.isMetooRequesting=false;this.dialog.showDefaultError()},this).bind();me2p.Service.writeComment(j,h,g)}};this.$init()};me2p.Messenger={bInitialed:false,flashUrl:"http://static.plugin.me2day.com/swf/NDomainBridge.swf",flashVer:"1.0.4",flashConnectId:null,fId:null,origin:"http://"+me2p_config_loginProxyDomain,bInitialed:false,retryCount:0,isPostMessageSupported:function(){return window.postMessage?true:false},Sender:function(a){this.$init=function(){if(me2p.Messenger.isPostMessageSupported()){me2p.Messenger.bInitialed=true}else{me2p.Messenger.flashConnectId=a;this.initSenderFlash()}};this.initSenderFlash=function(){var b={};b.initParams='{"prefix": "sender_", "type": 1}';var e={};e.wmode="window";e.allowScriptAccess="always";e.color="#FFFFFF";e.flashVars=b;var d=nhn.Me2DayFlashObject.generateTag(me2p.Messenger.flashUrl+"?sender"+me2p.Messenger.flashVer,"DomainBridgeSender","1","1",e);var c=document.createElement("div");c.style.position="absolute";c.style.top="-1000px";c.style.left="-1000px";document.body.appendChild(c);c.innerHTML=d};this.sendMessage=function(b,d){if(!me2p.Messenger.bInitialed){setTimeout(me2p.Util.fn(function(){this.sendMessage(b,d)},this).bind(),200);return false}var c={methodName:b,methodParam:me2p.Util.jsonToString(d)};c=me2p.Util.jsonToString(c);if(me2p.Messenger.isPostMessageSupported()){window.parent.postMessage(c,"*")}else{sendNotification(c)}};this.$init()},Receiver:function(){this.$init=function(){if(me2p.Messenger.isPostMessageSupported()){var fn=me2p.Util.fn(this.receiveMessage,this).bind();window.addEventListener?window.addEventListener("message",fn,false):window.attachEvent("onmessage",fn);me2p.Messenger.bInitialed=true}else{me2p.Messenger.flashConnectId="fId"+me2p.Util.getUinqId();this.initReceiverFlash()}};this.receiveMessage=function(event){if(!event){return}if(event.origin!==me2p.Messenger.origin){return}var oData=me2p.Util.toJson(event.data);try{eval(oData.methodName+"("+me2p.Util.jsonToString(oData.methodParam)+")")}catch(e){window._me2p_debug=oData;me2p.log("receiveMessage="+e)}};this.initReceiverFlash=function(){var flashVarsObj={};flashVarsObj.initParams='{"prefix": "receiver_", "type": 0}';var obj={};obj.wmode="window";obj.allowScriptAccess="always";obj.color="#FFFFFF";obj.flashVars=flashVarsObj;var flashTag=nhn.Me2DayFlashObject.generateTag(me2p.Messenger.flashUrl+"?receiver"+me2p.Messenger.flashVer,"DomainBridgeReceiver","1","1",obj);var flashWrapper=document.createElement("div");flashWrapper.style.position="absolute";flashWrapper.style.top="-1000px";flashWrapper.style.left="-1000px";document.body.appendChild(flashWrapper);flashWrapper.innerHTML=flashTag};this.$init()}};me2p.Service={APIUrl:{webPageInfo:"http://plugin.me2day.net/v1/plugin/"+(me2p_config_charset=="euc-kr"?"get_webpage_info_euckr.json":"get_webpage_info.json"),metoo:"http://plugin.me2day.net/v1/metoo/"+(me2p_config_charset=="euc-kr"?"metoo_euckr.json":"metoo.json"),comment:"http://plugin.me2day.net/v1/comment/create_comment.json",login:"http://me2day.net/account/login?login_from=plugin",loginProxy:"http://"+me2p_config_loginProxyDomain+"/html/naver/login_proxy.html",loginComplete:"http://"+me2p_config_loginProxyDomain+"/html/naver/login_complete.html"},webPageInfoParams:null,afterGetWebPageInfoAction:{},createdId:null,webPageInfo:{loginUser:{},ownerUser:{},postInfoList:[],securityToken:{},postInfoMap:{}},errorResponse:null,isWpRequesting:false,createdId:null,isLoggedIn:function(){return(this.webPageInfo.loginUser&&this.webPageInfo.loginUser.userId)},getWebPageInfo:function(){return this.webPageInfo},requestWebPageInfo:function(b,c){if(this.isWpRequesting){return false}else{this.isWpRequesting=true}var a=this.APIUrl.webPageInfo;if(b){if(typeof b!="string"){this.webPageInfoParams=me2p.Util.hashToQueryString(hsParams)
}else{this.webPageInfoParams=b}}if(a.indexOf("?")!=-1){a+=("&"+this.webPageInfoParams)}else{a+=("?"+this.webPageInfoParams)}if(c){this.afterGetWebPageInfoAction.updateUI=c}me2p.Util.requestJsonp(a,me2p.Util.fn(this._requestWebPageInfoOnSuccess,this).bind())},_requestWebPageInfoOnSuccess:function(d){var c=d;if(c.code=="0"){this.errorResponse=null;this.webPageInfo=c.result;if(!this.isToolbarChecked){this.checkToolbar(c.result)}this.webPageInfo.postInfoMap=this.webPageInfo.postInfoMap||{};if(this.webPageInfo.postInfoList){for(var g=0,a=this.webPageInfo.postInfoList.length;g<a;g++){this.webPageInfo.postInfoMap[this.webPageInfo.postInfoList[g].url]=this.webPageInfo.postInfoList[g]}}if(!this.webPageInfo.loginUser){this.webPageInfo.loginUser={}}if(this.createdId){this.webPageInfo.loginUser.createdId=this.createdId;this.createdId=null}if(this.afterGetWebPageInfoAction){for(var h in this.afterGetWebPageInfoAction){try{this.afterGetWebPageInfoAction[h](c)}catch(j){}}}this.afterGetWebPageInfoAction=[]}else{if(this.afterGetWebPageInfoAction.updateUI){this.afterGetWebPageInfoAction.updateUI()}for(var b in c){me2p.log(b+"="+c[b])}this.errorResponse=c}this.isWpRequesting=false;if(!me2p.Main.isWebPageInfoRequested){me2p.Main.isWebPageInfoRequested=true}},updatePostInfo:function(b){var c=false;for(var a in this.webPageInfo.postInfoList){if(this.webPageInfo.postInfoList[a].url==b.url){this.webPageInfo.postInfoList[a]=b;c=true}}if(!c){this.webPageInfo.postInfoList.push(b)}this.webPageInfo.postInfoMap[b.url]=b},requestMetoo:function(c,e,b){var a=this.APIUrl.metoo;var d="";if(typeof c!="string"){d=me2p.Util.hashToQueryString(c)}else{d=c}if(a.indexOf("?")!=-1){a+=("&"+d)}else{a+=("?"+d)}me2p.Util.requestJsonp(a,e,b)},writeComment:function(c,e,b){var a=this.APIUrl.comment;var d="";if(typeof c!="string"){d=me2p.Util.hashToQueryString(c)}else{d=c}if(a.indexOf("?")!=-1){a+=("&"+d)}else{a+=("?"+d)}me2p.Util.requestJsonp(a,e,b)},isToolbarChecked:false,checkToolbar:function(d){var b=null;function h(m){if(!m){return 0}var e=0;for(var l in m){if((l+"").indexOf("http:")!=-1){e++;b=l}}return e}this.isToolbarChecked=true;if(h(d.securityToken)!=1){return}var k=null;if(d.postInfoList&&d.postInfoList.length>0){k=d.postInfoList[0]}var j=null;try{if(window.ActiveXObject){j=new ActiveXObject("NaverToolbar.Me2API")}}catch(g){}try{if(window.ActiveXObject&&j){var a=d.securityToken[b];var f=k?k.metooCount:0;var c=k?k.metooStatus:9;console.log("NaverToolbar.Me2API > NaverToolbar.UpdateMetooInfo() called. ");console.log("url="+b);console.log("pluginKey="+me2p.MetooHandler.pluginKey);console.log("securityToken="+a);console.log("metooCount="+f);console.log("metooStatus="+c);j.UpdateMetooInfo(b,me2p.MetooHandler.pluginKey,a,f,c)}}catch(g){}}};me2p.Util={EmptyFunction:new Function(),requestJsonp:function(b,g,a){var c=document.head?document.head:document.getElementsByTagName("head")[0];var d=document.createElement("script");var e="me2p_"+new Date().getTime();var f=(typeof a=="function")?setTimeout(a,5000):null;window[e]=function(){if(f){clearInterval(f)}g.apply(null,arguments);window[e]=null;c.removeChild(d)};if(b.indexOf("?")!=-1){b+="&_callback=window."+e}else{b+="?_callback=window."+e}d.src=b;d.charset=me2p_config_charset;c.appendChild(d)},queryStringToHash:function(c){if(c==""){return{}}var h=c.split(/&/g),j,k,b,a={},f=false;for(var d=0;d<h.length;d++){k=h[d].substring(0,j=h[d].indexOf("=")),f=false;try{b=decodeURIComponent(h[d].substring(j+1))}catch(g){f=true;b=decodeURIComponent(unescape(h[d].substring(j+1)))}if(k.substr(k.length-2,2)=="[]"){k=k.substring(0,k.length-2);if(typeof a[k]=="undefined"){a[k]=[]}a[k][a[k].length]=f?escape(b):b}else{a[k]=f?escape(b):b}}return a},hashToQueryString:function(d){var c=[],e=null,a=0;for(var b in d){if(d.hasOwnProperty(b)){if(typeof(e=d[b])=="object"&&(e&&e.constructor==Array)){for(i=0;i<e.length;i++){c[c.length]=encodeURIComponent(b)+"[]="+encodeURIComponent(e[i]+"")}}else{c[c.length]=encodeURIComponent(b)+"="+encodeURIComponent(d[b]+"")}}}return c.join("&")},numberWithDelimiter:function(c,a){c=c+"",a=a||",";var b=c.split(".");b[0]=b[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g,"$1"+a);
return b.join(".")},fn:function(func,thisObject){var cl=arguments.callee;if(func instanceof cl){return func}if(!(this instanceof cl)){return new cl(func,thisObject)}this._events=[];this._tmpElm=null;this._key=null;if(typeof func=="function"){this._func=func;this._this=thisObject}else{if(typeof func=="string"&&typeof thisObject=="string"){this._func=eval("false||function("+func+"){"+thisObject+"}")}}},Template:function(d){var c=null,a="";var b=arguments.callee;if(d instanceof b){return d}if(!(this instanceof b)){return new b(d)}if(typeof d=="undefined"){d=""}else{if((c=document.getElementById(d)||d)&&c.tagName&&(a=c.tagName.toUpperCase())&&(a=="TEXTAREA"||(a=="SCRIPT"&&c.getAttribute("type")=="text/template"))){d=(c.value||c.innerHTML).replace(/^\s+|\s+$/g,"")}}this._str=d+""},getUinqId:function(){var a=function(){return"Uniq_"+Math.random().toString().replace(/0\./,"")};var b=a();while(document.getElementById(b)){b=a()}return b},strToJson:function(sObject){try{if(/^(?:\s*)[\{\[]/.test(sObject)){sObject=eval("("+sObject+")")}else{sObject=sObject}}catch(e){sObject={}}return sObject},toJson:function(a){if(!a){return{}}if(typeof a=="string"){return this.strToJson(a)}return a},jsonToString:function(a){var b={$:function(c){if(typeof c=="object"&&c==null){return"null"}if(typeof c=="undefined"){return'""'}if(typeof c=="boolean"){return c?"true":"false"}if(typeof c=="string"){return this.s(c)}if(typeof c=="number"){return c}if(c instanceof Array){return this.a(c)}if(c instanceof Object){return this.o(c)}},s:function(d){var f={'"':'\\"',"\\":"\\\\","\n":"\\n","\r":"\\r","\t":"\\t"};var g=function(c){return(typeof f[c]!="undefined")?f[c]:c};return'"'+d.replace(/[\\"'\n\r\t]/g,g)+'"'},a:function(d){var f="[",h="",g=d.length;for(var e=0;e<g;e++){if(typeof d[e]=="function"){continue}f+=h+this.$(d[e]);if(!h){h=","}}return f+"]"},o:function(f){var e="{",g="";for(var d in f){if(f.hasOwnProperty(d)){if(typeof f[d]=="function"){continue}e+=g+this.s(d)+":"+this.$(f[d]);if(!g){g=","}}}return e+"}"}};if(window.JSON&&window.JSON.stringify){return window.JSON.stringify(a)}else{return b.$(a)}},hasClass:function(a,b){return new RegExp("(\\s|^)"+b+"(\\s|$)").test(a.className)},addClass:function(a,b){if(!this.hasClass(a,b)){a.className+=" "+b}},removeClass:function(a,b){if(this.hasClass(a,b)){a.className=a.className.replace(new RegExp("(\\s|^)"+b+"(\\s|$)")," ").replace(/\s+/g," ").replace(/^\s|\s$/,"")}}};me2p.Util.fn.prototype.bind=function(){var d=arguments;var g=this._func;var e=this._this;var c=function(){var a=arguments;if(d.length){a=d.concat(a)}return g.apply(e,a)};return c};me2p.Util.Template.splitter=/(?!\\)[\{\}]/g;me2p.Util.Template.pattern=/^(?:if (.+)|elseif (.+)|for (?:(.+)\:)?(.+) in (.+)|(else)|\/(if|for)|=(.+)|js (.+)|set (.+))$/;me2p.Util.Template.prototype.process=function(data){if(!data){data={}}var key="\x01";var leftBrace="\x02";var rightBrace="\x03";var tpl=(" "+this._str+" ").replace(/\\{/g,leftBrace).replace(/\\}/g,rightBrace).replace(/(?!\\)\}\{/g,"}"+key+"{").split(me2p.Util.Template.splitter),i=tpl.length;var map={'"':'\\"',"\\":"\\\\","\n":"\\n","\r":"\\r","\t":"\\t","\f":"\\f"};var reg=[/(["'](?:(?:\\.)+|[^\\["']+)*["']|[a-zA-Z_][\w\.]*)/g,/[\n\r\t\f"\\]/g,/^\s+/,/\s+$/,/#/g];var cb=[function(m){return(m.substring(0,1)=='"'||m.substring(0,1)=="'"||m=="null")?m:"d."+m},function(m){return map[m]||m},"",""];var stm=[];var lev=0;tpl[0]=tpl[0].substr(1);tpl[i-1]=tpl[i-1].substr(0,tpl[i-1].length-1);if(i<2){return tpl[0]}tpl=tpl;var delete_info;while(i--){if(i%2){tpl[i]=tpl[i].replace(me2p.Util.Template.pattern,function(){var m=arguments;if(m[10]){return m[10].replace(/(\w+)(?:\s*)=(?:\s*)(?:([a-zA-Z0-9_]+)|(.+))$/g,function(){var mm=arguments;var str="d."+mm[1]+"=";if(mm[2]){str+="d."+mm[2]}else{str+=mm[3].replace(/(=(?:[a-zA-Z_][\w\.]*)+)/g,function(m){return(m.substring(0,1)=="=")?"d."+m.replace("=",""):m})}return str})+";"}if(m[9]){return"s[i++]="+m[9].replace(/(=(?:[a-zA-Z_][\w\.]*)+)/g,function(m){return(m.substring(0,1)=="=")?"d."+m.replace("=",""):m})+";"}if(m[8]){return"s[i++]= d."+m[8]+";"}if(m[1]){return"if("+m[1].replace(reg[0],cb[0]).replace(/d\.(typeof) /,"$1 ").replace(/ d\.(instanceof) d\./," $1 ")+"){"
}if(m[2]){return"}else if("+m[2].replace(reg[0],cb[0]).replace(/d\.(typeof) /,"$1 ").replace(/ d\.(instanceof) d\./," $1 ")+"){"}if(m[5]){delete_info=m[4];var _aStr=[];_aStr.push("var t#=d."+m[5]+"||{},p#=isArray(t#),i#=0;");_aStr.push("for(var x# in t#){");_aStr.push("if(!t#.hasOwnProperty(x#)){continue;}");_aStr.push("  if( (p# && isNaN(i#=parseInt(x#,10))) || (!p# && !t#.propertyIsEnumerable(x#)) ) continue;");_aStr.push("  d."+m[4]+"=t#[x#];");_aStr.push(m[3]?"d."+m[3]+"=p#?i#:x#;":"");return _aStr.join("").replace(reg[4],lev++)}if(m[6]){return"}else{"}if(m[7]){if(m[7]=="for"){return"delete d."+delete_info+"; };"}else{return"};"}}return m[0]})}else{if(tpl[i]==key){tpl[i]=""}else{if(tpl[i]){tpl[i]='s[i++]="'+tpl[i].replace(reg[1],cb[1])+'";'}}}}tpl=tpl.join("").replace(new RegExp(leftBrace,"g"),"{").replace(new RegExp(rightBrace,"g"),"}");var _aStr=[];_aStr.push("var s=[],i=0;");_aStr.push('function isArray(o){ return Object.prototype.toString.call(o) == "[object Array]" };');_aStr.push(tpl);_aStr.push('return s.join("");');tpl=eval("false||function(d){"+_aStr.join("")+"}");tpl=tpl(data);return tpl};me2p.LoginHelper=function(){this.messengerFrame=null;this.divContainer=null;this.openLoginWindow=function(c,b){var a=me2p.Service.APIUrl.loginComplete+"?"+me2p.Util.hashToQueryString(c);var d="hide_back_button=true&redirect_url="+encodeURIComponent(a);setTimeout(me2p.Util.fn(function(){me2p.Service.afterGetWebPageInfoAction.onLoginSuccess=b},this).bind(),1000);if(me2p.Messenger.flashConnectId){d+="&fId="+me2p.Messenger.flashConnectId}this.messengerFrame=document.createElement("iframe");this.messengerFrame.src=me2p.Service.APIUrl.loginProxy+"?"+d;this.divContainer=document.createElement("div");this.divContainer.style.position="absolute";this.divContainer.style.top="0px";this.divContainer.style.left="-10000px";this.divContainer.appendChild(this.messengerFrame);document.body.appendChild(this.divContainer)},this.gotoLoginPage=function(b){var c="redirect_url="+encodeURIComponent("http://plugin.me2day.net/mobile/metoo/metoo.nhn?"+me2p.Util.hashToQueryString(b));var a=me2p.Service.APIUrl.login+"&"+c;top.location.href=a}};if(typeof nhn=="undefined"){nhn={}}nhn.Me2DayFlashObject=(function(){var g={};var m="F"+new Date().getTime()+parseInt(Math.random()*1000000);var q=/MSIE/i.test(navigator.userAgent);var p=/FireFox/i.test(navigator.userAgent);var d=/Chrome/i.test(navigator.userAgent);var r="className, style, __flashID, classid, codebase, class, width, height, name, src, align, id, type, object, embed, movie, forwardInstall, requireVersion";var b=(navigator.appVersion.toLowerCase().indexOf("win")!=-1)?true:false;var s=(navigator.userAgent.indexOf("Opera")!=-1)?true:false;var e=function(z){var v,x,y;if(z==null){z=25}try{x=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");v=x.GetVariable("$version");if(v){return f(v)}}catch(y){}for(var w=z;w>0;w--){try{x=new ActiveXObject("ShockwaveFlash.ShockwaveFlash."+w);v=x.GetVariable("$version")}catch(y){continue}if(v){return f(v)}}return -1};var f=function(w){var v=w.split(" ")[1].split(",");return{major:v[0],minor:v[1],revision:v[2]}};var c=function(w){var y=-1;if(navigator.plugins!=null&&navigator.plugins.length>0){if(navigator.plugins["Shockwave Flash 2.0"]||navigator.plugins["Shockwave Flash"]){var B=navigator.plugins["Shockwave Flash 2.0"]?" 2.0":"";var C=navigator.plugins["Shockwave Flash"+B].description;var x=C.split(" ");var z=x[2].split(".");var v=z[0];var D=z[1];var A=x[3];if(A==""){A=x[4]}if(A[0]=="d"){A=A.substring(1)}else{if(A[0]=="r"){A=A.substring(1);if(A.indexOf("d")>0){A=A.substring(0,A.indexOf("d"))}}}y={major:v,minor:D,revision:A}}}else{if(navigator.userAgent.toLowerCase().indexOf("webtv/2.6")!=-1){y=4}else{if(navigator.userAgent.toLowerCase().indexOf("webtv/2.5")!=-1){y=3}else{if(navigator.userAgent.toLowerCase().indexOf("webtv")!=-1){y=2}else{if(q&&b&&!s){y=e(w)}}}}}return y};var n=function(x,w,v){version=c(x);if(version==-1){return false}else{if(version!=0){if(version.major>parseFloat(x)){return true}else{if(version.major==parseFloat(x)){if(version.minor>parseFloat(w)){return true
}else{if(version.minor==parseFloat(w)){if(version.revision>=parseFloat(v)){return true}}}}}return false}}};var a=function(){alert("default install run")};var u=function(v,x,w){if(typeof v.attachEvent!="undefined"){v.attachEvent("on"+x,w)}else{v.addEventListener(x,w,true)}};var h=function(v,x){var y="";var B=true;var w="";var z;for(var A in v){if(B){B=false}else{y+=x}z=v[A];switch(typeof(z)){case"string":y+=A+"="+encodeURIComponent(z);break;case"number":y+=A+"="+encodeURIComponent(z.toString());break;case"boolean":y+=A+"="+(z?"true":"false");break;default:}}return y};var o=function(){obj=document.getElementsByTagName("OBJECT");for(var w=0,v;v=obj[w];w++){v.style.display="none";for(var y in v){if(typeof(v[y])=="function"){try{if(v.hasOwnProperty(y)){v[y]=null}}catch(x){}}}}};var j=function(z){z=z||window.event;var y=z.wheelDelta/(d?360:120);if(!y){y=-z.detail/3}var w=z.target||z.srcElement;if(!(new RegExp("(^|\b)"+m+"_([a-z0-9_$]+)(\b|$)","i").test(w.className))){return}var v=RegExp.$2;var B="layerX" in z?z.layerX:z.offsetX;var A="layerY" in z?z.layerY:z.offsetY;try{if(!w[v](y,B,A)){if(z.preventDefault){z.preventDefault()}else{z.returnValue=false}}}catch(x){}};var l=function(w){var x=null;var z=/Safari/.test(navigator.userAgent);var v=/MSIE/.test(navigator.userAgent);var A=function(C){var B={left:0,top:0};if(C.parentNode.tagName.toLowerCase()=="object"){C=C.parentNode}for(var E=C,D=E.offsetParent;E=E.parentNode;){if(E.offsetParent){B.left-=E.scrollLeft;B.top-=E.scrollTop}if(E==D){B.left+=C.offsetLeft+E.clientLeft;B.top+=C.offsetTop+E.clientTop;if(!E.offsetParent){B.left+=E.offsetLeft;B.top+=E.offsetTop}D=E.offsetParent;C=E}}return B};var y=function(C){var B={left:0,top:0};for(var D=C;D;D=D.offsetParent){B.left+=D.offsetLeft;B.top+=D.offsetTop}for(var D=C.parentNode;D;D=D.parentNode){if(D.tagName=="BODY"){break}if(D.tagName=="TR"){B.top+=2}B.left-=D.scrollLeft;B.top-=D.scrollTop}return B};return(z?A:y)(w)};var t=function(){var v=/MSIE/.test(navigator.userAgent);if(v){var x=document.documentElement.scrollLeft||document.body.scrollLeft;var w=document.documentElement.scrollTop||document.body.scrollTop;return{scrollX:x,scrollY:w}}else{return{scrollX:window.pageXOffset,scrollY:window.pageYOffset}}};var k=function(){var v=/MSIE/.test(navigator.userAgent);var w={};if(v){w.nInnerWidth=document.documentElement.clientWidth||document.body.clientWidth;w.nInnerHeight=document.documentElement.clientHeight||document.body.clientHeight}else{w.nInnerWidth=window.innerWidth;w.nInnerHeight=window.innerHeight}return w};g.showAt=function(v,w){document.getElementById(v).innerHTML=w};g.show=function(y,z,A,C,v,B,x){if(v&&v.requireVersion){var w=v.requireVersion.split(".");var D=n(w[0],w[1],w[2]);if(!D){if(v.forwardInstall){v.forwardInstall()}else{a()}return null}}document.write(g.generateTag(y,z,A,C,v,B,x))};g.generateTag=function(D,E,F,I,w,G,A){F=F||"100%";I=I||"100%";A=A||"9,0,0,0";G=G||"middle";var H=g.getDefaultOption();if(w){if(w.flashVars){if(typeof(w.flashVars)=="object"){w.flashVars=h(w.flashVars,"&")}w.flashVars+="&"}else{w.flashVars=""}w.flashVars+="__flashID="+E;for(var J in w){if(r.indexOf(J)>=0){continue}H[J]=w[J]}}var K="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000";var z="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version="+A;var x="position:relative !important;";var y=m+"_"+H.wheelHandler;var B=[];var v=[];B.push('<object classid="'+K+'" codebase="'+z+'" class="'+y+'" style="'+x+'" " width="'+F+'" height="'+I+'" id="'+E+'" align="'+G+'">');B.push('<param name="movie" value="'+D+'" />');v.push('<embed width="'+F+'" height="'+I+'" name="'+E+'" class="'+y+'" style="'+x+'" " src="'+D+'" align="'+G+'" ');for(var C in H){B.push('<param name="'+C+'" value="'+H[C]+'" />');v.push(C+'="'+H[C]+'" ')}v.push('type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />');B.push(v.join(""));B.push("</object>");if(u){u(window,"unload",o);u(document,!p?"mousewheel":"DOMMouseScroll",j);u=null}return(q)?B.join(""):v.join("")};g.getDefaultOption=function(){return{quality:"high",bgColor:"#FFFFFF",allowScriptAccess:"always",wmode:"window",menu:"false",allowFullScreen:"true",wheelHandler:"messenger"}
};g.find=function(w,v){v=v||document;return v[w]||v.all[w]};g.getPlayerVersion=function(){return c()};g.setWidth=function(v,w){g.find(v).width=w};g.setHeight=function(v,w){g.find(v).height=w};g.setSize=function(x,w,v){g.find(x).height=v;g.find(x).width=w};g.getPositionObj=function(w){var x=g.find(w);if(x==null){return null}var v=l(x);var y=t();var z={};z.absoluteX=v.left;z.absoluteY=v.top;z.scrolledX=z.absoluteX-y.scrollX;z.scrolledY=z.absoluteY-y.scrollY;z.browserWidth=k().nInnerWidth;return z};g.getSSCLogParam=function(){var v=[];if(window.g_ssc){v.push("ssc="+g_ssc)}else{v.push("ssc=decide.me")}if(window.g_pid){v.push("&p="+g_pid)}if(window.g_query){v.push("&q="+encodeURIComponent(g_query))}if(window.g_sid){v.push("&s="+g_sid)}return v.join("")};return g})();function receiver_init(b){if(!me2p.Messenger){me2p.log("receiver messenger initialize not yet");return}else{}var c=me2p.Messenger.flashConnectId;var a=nhn.Me2DayFlashObject.find(b);a.connect(c);me2p.Messenger.bInitialed=true}function receiver_error(a,b){me2p.log("receiver_error : name = "+a+", message = "+b)}function sender_init(b){if(!me2p.Messenger){me2p.log("sender messenger initialize not yet");return}var c=me2p.Messenger.flashConnectId;var a=nhn.Me2DayFlashObject.find(b);a.connect(c);me2p.Messenger.bInitialed=true}function sender_error(a,b){}function sendNotification(b){var a=nhn.Me2DayFlashObject.find("DomainBridgeSender");a.sendNotification(b)}me2p.options={};me2p.parseElements=function(){me2p.Main.parsePluginElements()};me2p.updateElements=function(){me2p.Main.createParams();me2p.Main.reqWebPageInfoAndUpdateUI();if(!me2p_config_isMobile){me2p.Main.initMessenger()}};me2p.updateElementsForCallback=function(a){me2p.Main.createParams();me2p.Service.afterGetWebPageInfoAction.callback_DA=a;me2p.Service.requestWebPageInfo(me2p.Main.webPageInfoParams,null);if(!me2p_config_isMobile){me2p.Main.initMessenger()}};me2p.setOptions=function(a){var a=(!a)?{}:a;if(typeof a.request_api=="undefined"){a.request_api=true}if(typeof a.message_template=="undefined"){a.message_template=1}me2p.options.request_api=a.request_api;me2p.options.message_template=a.message_template};me2p.setMessageTemplate=function(){if(me2p.options.message_template==3){me2p.MessageTemplate=me2p.MessageTemplate_DA;me2p.MessageTemplate2=me2p.MessageTemplate_DA}};me2p.initialize=function(a){me2p.setOptions(a);me2p.setMessageTemplate();me2p.parseElements();if(me2p.options.request_api){me2p.updateElements()}};me2p.Main={messenger:null,title:null,webPageInfoParams:null,urlMap:{},isWebPageInfoRequested:false,parsePluginElements:function(){var f=function(){me2p.Main.urlMap={};me2p.MetooHandler.reset()};f();var g=document.getElementsByTagName("meta");var h;for(var d=0,c=g.length;d<c;d++){if(g[d].getAttribute("property")=="me2:title"){h=g[d].content}}if(!h){for(var d=0,c=g.length;d<c;d++){if(g[d].getAttribute("property")=="og:title"){h=g[d].content}}}if(!h){h=document.title}this.title=h;var b=me2p.Sizzle("."+me2p.ElementClassNames.BUTTON_CONTAINER);for(var e=0,c=b.length;e<c;e++){var a=b[e].getAttribute(me2p.AttributeNames.URL);this.urlMap[a]=true}for(var a in this.urlMap){var j=me2p.Sizzle("."+me2p.ElementClassNames.BUTTON_CONTAINER+"["+me2p.AttributeNames.URL+"='"+a+"']");for(var e=0,c=j.length;e<c;e++){me2p.MetooHandler.register(j[e],a,e)}}},createParams:function(){var e="";var b=[];for(var c in this.urlMap){if(c.indexOf("http")<0){continue}b.push(c)}for(var d=0,a=b.length;d<a;d++){e+=("url="+encodeURIComponent(b[d])+(d==(a-1)?"":"&"))}if(e==""){return}if(me2p.MetooHandler.pluginKey){e+="&plugin_key="+encodeURIComponent(me2p.MetooHandler.pluginKey)}e+="&plugin_referer="+encodeURIComponent(location.href);this.webPageInfoParams=e},reqWebPageInfoAndUpdateUI:function(a){if(a){a=me2p.Util.toJson(a);if(a.createdId){me2p.Service.createdId=a.createdId}}var b=me2p.Util.fn(function(){me2p.MetooHandler.updateAll()},this).bind();me2p.Service.requestWebPageInfo(this.webPageInfoParams,b)},initMessenger:function(){if(this.messenger==null){this.messenger=new me2p.Messenger.Receiver()}},receiverTest:function(a){me2p.log("receiverTest="+a)},showError:function(a){a=me2p.Util.toJson(a);
if(a.error=="8000"){alert(me2p.MessageText["8000"])}else{alert(me2p.MessageText["016"])}return}};if(me2p_config_autoRender){(function(){if(document.addEventListener){window.addEventListener("load",me2p.initialize,false)}else{if(document.attachEvent){window.attachEvent("onload",me2p.initialize)}else{var a=window.onload;window.onload=function(){me2p.initialize();if(a){a()}}}}})()};

/****************************************** nEvent ******************************************/

if (typeof nec == 'undefined') {
	nec = {};
}

if (typeof ecsrv == 'undefined') {
	var ecsrv = "ec.naver.com"; // server domian
}

if (typeof ecModule == 'undefined') {
	var ecModule = "ec"; // module name
}

nec.ecImg = {};
nec.eccnt = 0;
nec.version = "0.0.9";

function ectrack(key, value) {
	var eurl = "";
	var ptc = (window.location.protocol=="https:")?"https:":"http:";

	eurl = ptc + "//" + ecsrv + "/" + ecModule + "?";
	eurl += "ek="+encodeURIComponent(key);
	eurl += "&ev="+encodeURIComponent(document.URL);
	eurl += "&u="+encodeURIComponent(value);

	if (document.images)  {
		var timestr = new Date().getTime();
		eurl += "&time="+ timestr;		// Aviod image cache
		var h = window.nec.eccnt++;
		var o = new Image();
		window.nec.ecImg[h] = o;
		o.onerror = (o.onload = (o.onabort = function() {
			delete window.nec.ecImg[h];
		}));

		o.src = eurl;
	}

	return true;
}

function ectrackurl(key, value) {
	var eurl = "";
	var timestr = new Date().getTime();
	var ptc = (window.location.protocol=="https:")?"https:":"http:";

	eurl = ptc + "//" + ecsrv + "/" + ecModule + "?";
	eurl += "ek="+encodeURIComponent(key);
	eurl += "&ev="+encodeURIComponent(document.URL);
	eurl += "&u="+encodeURIComponent(value);
	eurl += "&time="+ timestr;

	return eurl;
}

/****************************************** nEvent ******************************************/
if (typeof spi == "undefined") {
	var spi = {};
}

spi.util = {
    requestJsonp: function (u, p, g, cb) {
        var c = document.head ? document.head : document.getElementsByTagName("head")[0];
        var d = document.createElement("script");
        var e = "spi_" + new Date().getTime();
        var f = (typeof a == "function") ? setTimeout(a, 5000) : null;
        var q = "";
        p = p || {};
        window[e] = function () {
            if (f) {
                clearInterval(f);
            }
            g.apply(null, arguments);
            window[e] = null;
            c.removeChild(d);
        };
        if (u.indexOf("?") != -1) {
            u += ("&" + (typeof cb != 'undefined' ? cb : "_callback") + "=window." + e);
        } else {
            u += ("?" + (typeof cb != 'undefined' ? cb : "_callback") + "=window." + e);
        }
		for ( k in p ) {
			if ( p.hasOwnProperty(k) ) {
				q += encodeURIComponent(k) + "=" + encodeURIComponent(p[k]) + "&";
			}
		}
        d.src = (u + "&" + q);
        c.appendChild(d);
    },
    strToJson: function (sObject) {
        try {
            if (/^(?:\s*)[\{\[]/.test(sObject)) {
                sObject = eval("(" + sObject + ")");
            }
        } catch (e) {
            sObject = {};
        }
        return sObject;
    },
    toJson: function (a) {
        if (!a) {
            return {};
        }
        if (typeof a == "string") {
            return this.strToJson(a);
        }
        return a;
    },
    fn: function (func, thisObject) {
        var cl = arguments.callee;
        if (func instanceof cl) {
            return func;
        }
        if (!(this instanceof cl)) {
            return new cl(func, thisObject);
        }
        this._events = [];
        this._tmpElm = null;
        this._key = null;
        if (typeof func == "function") {
            this._func = func;
            this._this = thisObject;
        } else {
            if (typeof func == "string" && typeof thisObject == "string") {
                this._func = eval("false||function(" + func + "){" + thisObject + "}");
            }
        }
    },
    offset : function(oEl, nTop, nLeft) {
    	if(!oEl){
    		return false;
    	}

    	var oPhantom = null;

    	// setter
    	if (typeof nTop == 'number' && typeof nLeft == 'number') {
    		if (isNaN(parseInt(oEl.style.top , 10))) {
    			oEl.style.top = "0px";
    		}

    		if (isNaN(parseInt(oEl.style.left ,10))) {
    			oEl.style.left = "0px";
    		}

    		var oPos = this.offset(oEl);
    		var oGap = { top : nTop - oPos.top, left : nLeft - oPos.left };

    		oEl.style.top = parseInt(oEl.style.top , 10) + oGap.top + 'px';
    		oEl.style.left = parseInt(oEl.style.left ,10) + oGap.left + 'px';

    		return this;
    	}

    	// getter
    	var bSafari = /Safari/.test(navigator.userAgent);
    	var bIE = /MSIE/.test(navigator.userAgent);
    	var nVer = bIE?navigator.userAgent.match(/(?:MSIE) ([0-9.]+)/)[1]:0;

    	var fpSafari = function(oEl) {
    		var oPos = { left : 0, top : 0 };
    		for (var oParent = oEl, oOffsetParent = oParent.offsetParent; oParent = oParent.parentNode; ) {
    			if (oParent.offsetParent) {
    				oPos.left -= oParent.scrollLeft;
    				oPos.top -= oParent.scrollTop;
    			}

    			if (oParent == oOffsetParent) {
    				oPos.left += oEl.offsetLeft + oParent.clientLeft;
    				oPos.top += oEl.offsetTop + oParent.clientTop ;

    				if (!oParent.offsetParent) {
    					oPos.left += oParent.offsetLeft;
    					oPos.top += oParent.offsetTop;
    				}

    				oOffsetParent = oParent.offsetParent;
    				oEl = oParent;
    			}
    		}

    		return oPos;
    	};

    	var fpOthers = function(oEl) {
    		var oPos = { left : 0, top : 0 };

    		var oDoc = oEl.ownerDocument || oEl.document || document;
    		var oHtml = oDoc.documentElement;
    		var oBody = oDoc.body;

    		if (oEl.getBoundingClientRect) { // has getBoundingClientRect
    			if (!oPhantom) {
    				var bHasFrameBorder = (window == top);
    				if(!bHasFrameBorder){
    					try{
    						bHasFrameBorder = (window.frameElement && window.frameElement.frameBorder == 1);
    					}catch(e){}
    				}
    				if ((bIE && nVer < 8 && window.external) && bHasFrameBorder) {
    					oPhantom = { left : 2, top : 2 };
    					oBase = null;

    				} else {
    					oPhantom = { left : 0, top : 0 };
    				}
    			}

    			var box = oEl.getBoundingClientRect();
    			if (oEl !== oHtml && oEl !== oBody) {
    				oPos.left = box.left - oPhantom.left;
    				oPos.top = box.top - oPhantom.top;

    				oPos.left += oHtml.scrollLeft || oBody.scrollLeft;
    				oPos.top += oHtml.scrollTop || oBody.scrollTop;
    			}

    		} else if (oDoc.getBoxObjectFor) { // has getBoxObjectFor
    			var box = oDoc.getBoxObjectFor(oEl);
    			var vpBox = oDoc.getBoxObjectFor(oHtml || oBody);

    			oPos.left = box.screenX - vpBox.screenX;
    			oPos.top = box.screenY - vpBox.screenY;
    		} else {
    			for (var o = oEl; o; o = o.offsetParent) {
    				oPos.left += o.offsetLeft;
    				oPos.top += o.offsetTop;
    			}

    			for (var o = oEl.parentNode; o; o = o.parentNode) {
    				if (o.tagName == 'BODY') break;
    				if (o.tagName == 'TR') oPos.top += 2;

    				oPos.left -= o.scrollLeft;
    				oPos.top -= o.scrollTop;
    			}
    		}

    		return oPos;
    	};

    	return (bSafari ? fpSafari : fpOthers)(oEl);
    }
};

spi.util.fn.prototype.bind = function () {
    var d = arguments;
    var g = this._func;
    var e = this._this;
    var c = function () {
            var a = arguments;
            if (d.length) {
                a = d.concat(a);
            }
            return g.apply(e, a);
        };
    return c;
};

/******************************************** spi ********************************************/

if (typeof nhn == 'undefined') {
	nhn = {};
}

/**
 * 네이버 소셜플러그인
 *
 * @namespace
 * @name nhn.socialplugin
 * @author demitry
 */
nhn.socialplugin = {

	spiObj : null,
	arSpiObjs : null,

	drawSpiLayer : function(htInitData, spiLayerId) {
		this.spiObj = new SocialPlugIn();
		this.spiObj.setMultiMode(false);
		this.spiObj.drawSpiLayer(htInitData, spiLayerId);
	},

	drawMultiSpiLayer : function(arParam) {
		var i = 0;

		if (arParam == null || arParam == 'undefined') {
			return;
		}

		this.arSpiObjs = [];

		for (var i = 0; i < arParam.length; i++) {
			this.arSpiObjs[i] = new SocialPlugIn();
			this.arSpiObjs[i].setMultiMode(true);
			this.arSpiObjs[i].drawSpiLayer(arParam[i].data, arParam[i].layerId);
		}

		me2p.initialize();

		for (i = 0; i < arSpiObjs.length; i++) {
			this.arSpiObjs[i]._initializeMe2p();
		}
	},

	destroy : function() {
		if (this.spiObj) {
			this.spiObj.destroy();
			this.spiObj = null;
		}
	}

};

/************************************** spi class ********************************************/

function SocialPlugIn() {

	/**
	 * 멀티 모드
	 */
	this._bMultiMode = false;

	/**
	 * 이벤트 핸들러 저장 객체
	 *
	 * @type {HashTable}
	 */
	this._htEvent = {};

	/**
	 * 엘리먼트 정보
	 *
	 * @type {HashTable}
	 */
	this._htElement = {};

	/**
	 * 초기화 데이터
	 *
	 * @type {HashTable}
	 */
	this._htInitData = {};

	/**
	 * 유니크 ID
	 *
	 * @type {String}
	 */
	this._sUniqueId = "";

	/**
	 * 유니크 레이어 ID
	 *
	 * @type {String}
	 */
	this._sSpiLayerId = "";

	/**
	 * 마크업 스타일
	 *
	 * @type {String}
	 */
	this._sMarkupStyle = "";

	/**
	 * CSS 스타일
	 *
	 * @type {String}
	 */
	this._sCssStyle = "";

	/**
	 * 플러그인 정렬
	 *
	 * @type {String}
	 */
	this._sAlign = "default";

	/**
	 * me 구독 안내 레이어 타이머
	 */
	this._tTimer = null;

	/**
	 * 사용자 북마크 임시 UrlId
	 *
	 * @type {String}
	 */
	this._sBmkUrlId = "";

	/**
	 * me2 카운트 임시 변수
	 */
	this._nMe2Count = 0;

	/**
	 * API 주소
	 *
	 * @type {HashTable}
	 */
	this._htAPI = {
//		"API000" : "http://dev.me.naver.com/isLoggedIn.nhn",					// 로그인체크
		"API000" : "http://gn.naver.com/getLoginStatusLv2.nhn",					// 로그인체크
		"API101" : "http://me2do.naver.com/common/requestJsonp.nhn",			// 단축URL
		"API201" : "http://relation.commcast.naver.com",						// me 구독하기
		"API301" : "http://bookmark.naver.com/api/exist_v2.nhn", 				// 북마크 확인
		"API302" : "http://bookmark.naver.com/apiSave.nhn", 					// 북마크 추가
		"API303" : "http://bookmark.naver.com/apiDelete.nhn",					// 북마크 삭제
//		"API501" : "http://mail.naver.com/write/extwrite.nhn", 					// 메일 보내기(구버전)
		"API501" : "http://mail.naver.com/write/tmpl", 							// 메일 보내기(신버전)
		"API502" : "http://cafe.naver.com/CafeScrapView.nhn", 					// 카페 보내기
		"API503" : "http://blog.naver.com/ScrapForm.nhn",			 			// 블로그 보내기
		"API504" : "http://plugin.me2day.net/v1/me2post/create_post_form.nhn",	// 미투 포스트
		"API505" : "https://www.facebook.com/sharer/sharer.php",				// 페이스북 보내기
		"API506" : "https://twitter.com/share", 								// 트위터 보내기
		"API507" : "http://yozm.daum.net/api/popup/prePost", 					// 요즘 보내기
		"API508" : "http://memo.naver.com/plugin/view.nhn",						// 메모 보내기
		"API509" : "https://calendar.naver.com/cplugin.nhn"						// 일정 보내기
	};

	/**
	 * nEvent 정의(클릭로그)
	 *
	 * @type {HashTable}
	 */
	this._htClickLog = {
		me       : {baseUrl : "http://social.naver.com/v/click", key : "pc.subs"},
		mecount  : {baseUrl : "http://social.naver.com/v/click", key : "pc.subs.count"},
		bookmark : {baseUrl : "http://social.naver.com/v/click", key : "pc.bmk"},
		me2      : {baseUrl : "http://social.naver.com/v/click", key : "pc.me2"},
		me2count : {baseUrl : "http://social.naver.com/v/click", key : "pc.me2.count"},
		release  : {baseUrl : "http://social.naver.com/v/click", key : "pc.snd"},
		copyurl  : {baseUrl : "http://social.naver.com/v/click", key : "pc.cpurl"},
		mail     : {baseUrl : "http://social.naver.com/v/click", key : "pc.sndmail"},
		blog     : {baseUrl : "http://social.naver.com/v/click", key : "pc.sndblog"},
		memo     : {baseUrl : "http://social.naver.com/v/click", key : "pc.sndmemo"},
		calendar : {baseUrl : "http://social.naver.com/v/click", key : "pc.sndclnd"},
		cafe     : {baseUrl : "http://social.naver.com/v/click", key : "pc.sndcafe"},
		me2post  : {baseUrl : "http://social.naver.com/v/click", key : "pc.sndme2"},
		facebook : {baseUrl : "http://social.naver.com/v/click", key : "pc.sndfb"},
		twitter  : {baseUrl : "http://social.naver.com/v/click", key : "pc.sndtw"},
		yozm     : {baseUrl : "http://social.naver.com/v/click", key : "pc.sndyz"},
		melink   : {baseUrl : "http://social.naver.com/v/click", key : "pc.melnk"},
		bmklink  : {baseUrl : "http://social.naver.com/v/click", key : "pc.bmklnk"},
		event    : {baseUrl : "http://social.naver.com/v/click", key : "pc.evt"}
	};

	/**
	 * nEvent 정의(등록로그)
	 *
	 * @type (HashTable}
	 */
	this._htFinishLog = {
		me       : {baseUrl : "http://social.naver.com/v/finish", key : "pc.subs"},
		bookmark : {baseUrl : "http://social.naver.com/v/finish", key : "pc.bmk"},
		me2      : {baseUrl : "http://social.naver.com/v/finish", key : "pc.m2"},
		mail     : {baseUrl : "http://social.naver.com/v/finish", key : "pc.sndmail"},
		blog     : {baseUrl : "http://social.naver.com/v/finish", key : "pc.sndblog"},
		cafe     : {baseUrl : "http://social.naver.com/v/finish", key : "pc.sndcafe"},
		me2post  : {baseUrl : "http://social.naver.com/v/finish", key : "pc.sndme2"}
	};

	/**
	 * CSS 정의
	 */
	// SVN
	/*this._htStyleSheet = {
		"standard"            : "http://svn.nhndesign.com/svnview/N_01_common/component/branches/p_111026_socialplugin/css/spi_standard.css",
		"standard_ie6"        : "http://svn.nhndesign.com/svnview/N_01_common/component/branches/p_111026_socialplugin/css/spi_standard_ie6.css"
	};*/
	// LOCAL
	/*this._htStyleSheet = {
		"standard"            : "http://local.spi.naver.com/css/20121108/spi_standard_20121108.css",
		"standard_ie6"        : "http://local.spi.naver.com/css/20121108/spi_standard_ie6_20121108.css"
	};*/
	// DEV
	/*this._htStyleSheet = {
		"standard"            : "http://dev.spi.naver.com/css/20121108/spi_standard_20121108.css",
		"standard_ie6"        : "http://dev.spi.naver.com/css/20121108/spi_standard_ie6_20121108.css"
	};*/
	// REAL
	/*this._htStyleSheet = {
		"standard"            : "http://spi.naver.com/css/20121108/spi_standard_20121108.css",
		"standard_ie6"        : "http://spi.naver.com/css/20121108/spi_standard_ie6_20121108.css"
	};*/
	// REAL(CDN)
	this._htStyleSheet = {
		"standard"            : "http://spi.naver.net/css/20121108/spi_standard_20121108.css",
		"standard_ie6"        : "http://spi.naver.net/css/20121108/spi_standard_ie6_20121108.css"
	};

	/**
	 * 마크업 정의
	 */
	this._htMarkUp = {
	    "standard" :
			"<div id='_spi_layer_id_' class='spi_default'>" +
			"	<ul class='spi_lst'>" +
			"		<li class='_spi_btn_me spi_btn_me' style='display:none;'><a href='#' class='_spi_btn_me'><span class='_spi_btn_me'></span>구독<em></em></a></li>" +
			"		<li class='_spi_btn_bookmark spi_btn_bookmark' style='display:none;'><a href='#' title='네이버 북마크를 사용해보세요' class='_spi_bmk_load _spi_btn_bookmark'><span class='_spi_btn_bookmark'></span>북마크<em></em></a></li>" +
			"		<li class='_spi_btn_me2day me2button_container spi_btn_me2day' plugin_key='_me2key_' href='_me2source_' before_metoo_class='none' after_metoo_class='spi_dimmed'><a href='#' class='_spi_btn_me2day me2button_button'><span class='_spi_btn_me2day'></span>미투<em class='_spi_btn_me2day'></em></a>" +
			"			<ul class='spi_cnt_tooltip me2button_count_bubble' style='display:none;'><li><strong><a href='#' class='_spi_btn_me2count me2button_count spi_cnt'>0</a></strong><em class='_spi_btn_me2count'></em></li></ul>" +
            "			<div class='_me2button_layer me2button_layer spi_ly_style' success_icon_class='spi_icon1' error_icon_class='spi_icon2' style='display:none'>" +
            "				<p class='me2button_layer_message spi_ly_msg spi_icon2'></p>" +
            "				<p class='me2button_layer_signin spi_ly_cfm'></p>" +
            "				<button type='button' title='닫기' class='me2button_layer_close spi_me2btn_close'><span></span><em>X</em></button>" +
            "			</div>" +
			"		</li>" +
			"		<li class='_spi_btn_release_ spi_btn_release'><a href='#' class='_spi_btn_release_'><span class='_spi_btn_release_ spi_bg_release'></span>보내기<em class='_spi_btn_release_'></em><span class='_spi_btn_release_ spi_bu_arr'></span></a>" +
			"			<ul class='_spi_lst_release spi_lst_release' style='display:none;'>" +
			//"				<li class='_spi_lnk_event event'><a href='#' class='_spi_lnk_event'><strong class='_spi_lnk_event tx_mm'>메모</strong>로 등록하고 <em class='_spi_lnk_event'><b class='_spi_lnk_event'>3,000</b>마일</em> 받으세요<span class='_spi_lnk_event ico_arr'></span></a></li>" +
			"				<li class='_spi_lnk_url spi_lnk_url' style='display:none;'><a href='#' class='_spi_lnk_url'><span class='_spi_lnk_url'></span><span class='_spi_lnk_url ls'>URL</span>복사</a></li>" +
			"				<li class='_spi_lnk_mail spi_lnk_mail' style='display:none;'><a href='#' class='_spi_lnk_mail'><span class='_spi_lnk_mail'></span>메일</a></li>" +
			"				<li class='_spi_lnk_cafe spi_lnk_cafe' style='display:none;'><a href='#' class='_spi_lnk_cafe'><span class='_spi_lnk_cafe'></span>카페</a></li>" +
			"				<li class='_spi_lnk_blog spi_lnk_blog' style='display:none;'><a href='#' class='_spi_lnk_blog'><span class='_spi_lnk_blog'></span>블로그</a></li>" +
			"				<li class='_spi_lnk_memo spi_lnk_memo' style='display:none;'><a href='#' class='_spi_lnk_memo'><span class='_spi_lnk_memo'></span>메모</a></li>" +
			"				<li class='_spi_lnk_calendar spi_lnk_calendar' style='display:none;'><a href='#' class='_spi_lnk_calendar'><span class='_spi_lnk_calendar'></span>일정</a></li>" +
			"				<li class='_spi_lnk_me2post spi_lnk_me2' style='display:none;'><a href='#' class='_spi_lnk_me2post'><span class='_spi_lnk_me2post'></span>미투데이</a></li>" +
			"				<li class='_spi_lnk_facebook spi_lnk_facebook' style='display:none;'><a href='#' class='_spi_lnk_facebook'><span class='_spi_lnk_facebook'></span>페이스북</a></li>" +
			"				<li class='_spi_lnk_twitter spi_lnk_twitter' style='display:none;'><a href='#' class='_spi_lnk_twitter'><span class='_spi_lnk_twitter'></span>트위터</a></li>" +
			"				<li class='_spi_lnk_yozm spi_lnk_yozm' style='display:none;'><a href='#' class='_spi_lnk_yozm'><span class='_spi_yozm'></span>요즘</a></li>" +
			"			</ul>" +
			"		</li>" +
			"	</ul>" +
			"	<div class='_ly_bmk_ok spi_ly_pop' style='display:none;left:57px;top:31px;width:242px'>" +
			"		<p class='spi_dsc'>북마크 되었습니다.<br> <a href='http://me.naver.com/tab/bookmark.nhn' target='_blank' class='_ly_bmk_ok_cls _ly_bmk_lnk spi_impact'>네이버me 북마크함 가기</a></p>" +
			"		<button class='_ly_bmk_ok_cls spi_close' type='button'><span class='_ly_bmk_ok_cls'></span><em class='_ly_bmk_ok_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_bmk_cnf spi_ly_pop' style='display:none;left:57px;top:31px;width:243px'>" +
			"		<p class='spi_dsc'>현재 북마크 되어있습니다.<br> 북마크를 해제하시겠습니까?</p>" +
			"		<div class='spi_btn'>" +
			"			<a href='#' class='spi_y'><span class='_ly_bmk_cnf_y'></span>예</a>" +
			"			<a href='#' class='spi_n'><span class='_ly_bmk_cnf_n'></span>아니오</a>" +
			"		</div>" +
			"		<button class='_ly_bmk_cnf_cls spi_close' type='button'><span class='_ly_bmk_cnf_cls'></span><em class='_ly_bmk_cnf_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_bmk_err spi_ly_pop' style='display:none;left:60px;top:31px;width:243px'>" +
			"		<p class='spi_dsc'>서버 접속이 원활하지 않습니다.<br> 잠시 후 다시 시도해 주십시오.</p>" +
			"		<button class='_ly_bmk_err_cls spi_close' type='button'><span class='_ly_bmk_err_cls'></span><em class='_ly_bmk_err_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_bmk_ros spi_ly_pop' style='display:none;left:60px;top:31px;width:243px'>" +
			"		<p class='spi_dsc'>북마크 서비스 점검 중으로,<br>현재 북마크 읽기만 가능하오니<br>이용에 참고해 주시기 바랍니다.</p>" +
			"		<button class='_ly_bmk_ros_cls spi_close' type='button'><span class='_ly_bmk_ros_cls'></span><em class='_ly_bmk_ros_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_url_ok spi_ly_pop' style='display:none;left:57px;top:31px;width:243px'>" +
            "		<p class='spi_dsc'>URL이 복사되었습니다.<br>원하는 곳에 붙여넣기(Ctrl+V)해 주세요.</p>" +
            "		<button type='button' class='_ly_url_ok_cls spi_close'><span class='_ly_url_ok_cls'></span><em class='_ly_url_ok_cls'>X</em></button>" +
            "	</div>" +
            "	<div class='_ly_pop spi_ly_pop ly_pop_v1' style='display:none;left:0;top:-82px;width:213px'>" +
            "		<p class='_ly_pop spi_dsc'>이 _feed_info_ 마음에 드셨다면<br><a href='http://me.naver.com/' target='_blank' class='_ly_pop _ly_me_lnk spi_impact'>네이버me</a>에서 편하게 받아보세요.</p>" +
            "		<div class='_ly_pop spi_ico_arr'></div>" +
            "	</div>" +
			"</div>",
		"text" :
	        "<div id='_spi_layer_id_' class='spi_default spi_default_v1'>" +
			"   <ul class='spi_lst _text_style_'>" +
			"   	<li class='_spi_btn_me spi_btn_me' style='display:none;'><a href='#' class='_spi_btn_me _text_class_' title='네이버me 구독함에서 편하게 받아보세요.'><span class='_spi_btn_me'></span>구독하기<em class='_spi_btn_me'></em></a></li>" +
			"   	<li class='_spi_btn_bookmark spi_btn_bookmark' style='display:none;'><a href='#' title='네이버 북마크를 사용해보세요' class='_spi_bmk_load _spi_btn_bookmark _text_class_'><span class='_spi_btn_bookmark'></span>북마크<em class='_spi_sep1 _bar_class_'></em></a></li>" +
			"		<li class='_spi_btn_me2day me2button_container spi_btn_me2day' plugin_key='_me2key_' href='_me2source_' before_metoo_class='none' after_metoo_class='spi_dimmed'><a href='#' class='_spi_btn_me2day me2button_button _text_class_'><span class='_spi_btn_me2day'></span>미투<em class='_spi_sep2 _bar_class_'></em></a>" +
			"			<ul class='spi_cnt_tooltip me2button_count_bubble' style='display:none;'><li><strong><a href='#' class='_spi_btn_me2count me2button_count spi_cnt'>0</a></strong><em class='_me_sep2'></em></li></ul>" +
            "			<div class='_me2button_layer me2button_layer spi_ly_style' success_icon_class='spi_icon1' error_icon_class='spi_icon2' style='display:none'>" +
            "				<p class='me2button_layer_message spi_ly_msg spi_icon2'></p>" +
            "				<p class='me2button_layer_signin spi_ly_cfm'></p>" +
            "				<button type='button' title='닫기' class='me2button_layer_close spi_me2btn_close'><span></span><em>X</em></button>" +
            "			</div>" +
			"		</li>" +
			"   	<li class='_spi_btn_release_ spi_btn_release'><a href='#' class='_spi_btn_release_ _text_class_'><span class='_spi_btn_release_ spi_bg_release'></span>보내기<em class='_spi_sep3 _bar_class_'></em><span class='_spi_btn_release_ spi_bu_arr'></span></a>" +
			"      		<ul class='_spi_lst_release spi_lst_release' style='display:none;'>" +
			//"				<li class='_spi_lnk_event event'><a href='#' class='_spi_lnk_event'><strong class='_spi_lnk_event tx_mm'>메모</strong>로 등록하고 <em class='_spi_lnk_event'><b class='_spi_lnk_event'>3,000</b>마일</em> 받으세요<span class='_spi_lnk_event ico_arr'></span></a></li>" +
			"				<li class='_spi_lnk_url spi_lnk_url' style='display:none;'><a href='#' class='_spi_lnk_url'><span class='_spi_lnk_url'></span><span class='_spi_lnk_url ls'>URL</span>복사</a></li>" +
			"       		<li class='_spi_lnk_mail spi_lnk_mail' style='display:none;'><a href='#' class='_spi_lnk_mail'><span class='_spi_lnk_mail'></span>메일</a></li>" +
			"       		<li class='_spi_lnk_cafe spi_lnk_cafe' style='display:none;'><a href='#' class='_spi_lnk_cafe'><span class='_spi_lnk_cafe'></span>카페</a></li>" +
			"       		<li class='_spi_lnk_blog spi_lnk_blog' style='display:none;'><a href='#' class='_spi_lnk_blog'><span class='_spi_lnk_blog'></span>블로그</a></li>" +
			"				<li class='_spi_lnk_memo spi_lnk_memo' style='display:none;'><a href='#' class='_spi_lnk_memo'><span class='_spi_lnk_memo'></span>메모</a></li>" +
			"				<li class='_spi_lnk_calendar spi_lnk_calendar' style='display:none;'><a href='#' class='_spi_lnk_calendar'><span class='_spi_lnk_calendar'></span>일정</a></li>" +
			"       		<li class='_spi_lnk_me2post spi_lnk_me2' style='display:none;'><a href='#' class='_spi_lnk_me2post'><span class='_spi_lnk_me2post'></span>미투데이</a></li>" +
			"       		<li class='_spi_lnk_facebook spi_lnk_facebook' style='display:none;'><a href='#' class='_spi_lnk_facebook'><span class='_spi_lnk_facebook'></span>페이스북</a></li>" +
			"       		<li class='_spi_lnk_twitter spi_lnk_twitter' style='display:none;'><a href='#' class='_spi_lnk_twitter'><span class='_spi_lnk_twitter'></span>트위터</a></li>" +
			"			    <li class='_spi_lnk_yozm spi_lnk_yozm' style='display:none;'><a href='#' class='_spi_lnk_yozm'><span class='_spi_yozm'></span>요즘</a></li>" +
			"       	</ul>" +
			"   	</li>" +
			"   </ul>" +
			"	<div class='_ly_bmk_ok spi_ly_pop' style='display:none;left:57px;top:31px;width:242px'>" +
			"		<p class='spi_dsc'>북마크 되었습니다.<br> <a href='http://me.naver.com/tab/bookmark.nhn' target='_blank' class='_ly_bmk_ok_cls _ly_bmk_lnk spi_impact'>네이버me 북마크함 가기</a></p>" +
			"		<button class='_ly_bmk_ok_cls spi_close' type='button'><span class='_ly_bmk_ok_cls'></span><em class='_ly_bmk_ok_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_bmk_cnf spi_ly_pop' style='display:none;left:57px;top:31px;width:243px'>" +
			"		<p class='spi_dsc'>현재 북마크 되어있습니다.<br> 북마크를 해제하시겠습니까?</p>" +
			"		<div class='spi_btn'>" +
			"			<a href='#' class='spi_y'><span class='_ly_bmk_cnf_y'></span>예</a>" +
			"			<a href='#' class='spi_n'><span class='_ly_bmk_cnf_n'></span>아니오</a>" +
			"		</div>" +
			"		<button class='_ly_bmk_cnf_cls spi_close' type='button'><span class='_ly_bmk_cnf_cls'></span><em class='_ly_bmk_cnf_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_bmk_err spi_ly_pop' style='display:none;left:60px;top:31px;width:243px'>" +
			"		<p class='spi_dsc'>서버 접속이 원활하지 않습니다.<br> 잠시 후 다시 시도해 주십시오.</p>" +
			"		<button class='_ly_bmk_err_cls spi_close' type='button'><span class='_ly_bmk_err_cls'></span><em class='_ly_bmk_err_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_bmk_ros spi_ly_pop' style='display:none;left:60px;top:31px;width:243px'>" +
			"		<p class='spi_dsc'>북마크 서비스 점검 중으로,<br>현재 북마크 읽기만 가능하오니<br>이용에 참고해 주시기 바랍니다.</p>" +
			"		<button class='_ly_bmk_ros_cls spi_close' type='button'><span class='_ly_bmk_ros_cls'></span><em class='_ly_bmk_ros_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_url_ok spi_ly_pop' style='display:none;left:57px;top:31px;width:243px'>" +
            "		<p class='spi_dsc'>URL이 복사되었습니다.<br>원하는 곳에 붙여넣기(Ctrl+V)해 주세요.</p>" +
            "		<button type='button' class='_ly_url_ok_cls spi_close'><span class='_ly_url_ok_cls'></span><em class='_ly_url_ok_cls'>X</em></button>" +
            "	</div>" +
            "	<div class='_ly_pop spi_ly_pop ly_pop_v1' style='display:none;left:0;top:-82px;width:213px'>" +
            "		<p class='_ly_pop spi_dsc'>이 _feed_info_ 마음에 드셨다면<br><a href='http://me.naver.com/' target='_blank' class='_ly_pop _ly_me_lnk spi_impact'>네이버me</a>에서 편하게 받아보세요.</p>" +
            "		<div class='_ly_pop spi_ico_arr'></div>" +
            "	</div>" +
			"</div>",
		"community" :
			"<div id='_spi_layer_id_' class='spi_default spi_default_v1 spi_icon'>" +
			"   <ul class='spi_lst _text_style_'>" +
			"   	<li class='_spi_btn_me spi_btn_me' style='display:none;'><a href='#' class='_spi_btn_me _text_class_' title='네이버me 구독함에서 편하게 받아보세요.'><span class='_spi_btn_me'></span>구독<em class='_spi_btn_me'></em></a></li>" +
			"   	<li class='_spi_btn_bookmark spi_btn_bookmark' style='display:none;'><a href='#' title='네이버 북마크를 사용해보세요' class='_spi_bmk_load _spi_btn_bookmark _text_class_'><span class='_spi_btn_bookmark'></span>북마크<em class='_spi_sep1 _bar_class_'></em></a></li>" +
			"		<li class='_spi_btn_me2day me2button_container spi_btn_me2day' plugin_key='_me2key_' href='_me2source_' before_metoo_class='none' after_metoo_class='spi_dimmed'><a href='#' class='_spi_btn_me2day me2button_button _text_class_'><span class='_spi_btn_me2day'></span>미투<em class='_spi_sep2 _bar_class_'></em></a>" +
			"			<ul class='spi_cnt_tooltip me2button_count_bubble' style='display:none;'><li><strong><a href='#' class='_spi_btn_me2count me2button_count spi_cnt'>0</a></strong><em class='_me_sep2'></em></li></ul>" +
            "			<div class='_me2button_layer me2button_layer spi_ly_style' success_icon_class='spi_icon1' error_icon_class='spi_icon2' style='display:none'>" +
            "				<p class='me2button_layer_message spi_ly_msg spi_icon2'></p>" +
            "				<p class='me2button_layer_signin spi_ly_cfm'></p>" +
            "				<button type='button' title='닫기' class='me2button_layer_close spi_me2btn_close'><span></span><em>X</em></button>" +
            "			</div>" +
			"		</li>" +
			"   	<li class='_spi_btn_release_ spi_btn_release'><a href='#' class='_spi_btn_release_ _text_class_'><span class='_spi_btn_release_ spi_bg_release'></span>보내기<em class='_spi_sep3 _bar_class_'></em><span class='_spi_btn_release_ spi_bu_arr'></span></a>" +
			"      		<ul class='_spi_lst_release spi_lst_release' style='display:none;'>" +
			//"				<li class='_spi_lnk_event event'><a href='#' class='_spi_lnk_event'><strong class='_spi_lnk_event tx_mm'>메모</strong>로 등록하고 <em class='_spi_lnk_event'><b class='_spi_lnk_event'>3,000</b>마일</em> 받으세요<span class='_spi_lnk_event ico_arr'></span></a></li>" +
			"				<li class='_spi_lnk_url spi_lnk_url' style='display:none;'><a href='#' class='_spi_lnk_url'><span class='_spi_lnk_url'></span><span class='_spi_lnk_url ls'>URL</span>복사</a></li>" +
			"       		<li class='_spi_lnk_mail spi_lnk_mail' style='display:none;'><a href='#' class='_spi_lnk_mail'><span class='_spi_lnk_mail'></span>메일</a></li>" +
			"       		<li class='_spi_lnk_cafe spi_lnk_cafe' style='display:none;'><a href='#' class='_spi_lnk_cafe'><span class='_spi_lnk_cafe'></span>카페</a></li>" +
			"       		<li class='_spi_lnk_blog spi_lnk_blog' style='display:none;'><a href='#' class='_spi_lnk_blog'><span class='_spi_lnk_blog'></span>블로그</a></li>" +
			"				<li class='_spi_lnk_memo spi_lnk_memo' style='display:none;'><a href='#' class='_spi_lnk_memo'><span class='_spi_lnk_memo'></span>메모</a></li>" +
			"				<li class='_spi_lnk_calendar spi_lnk_calendar' style='display:none;'><a href='#' class='_spi_lnk_calendar'><span class='_spi_lnk_calendar'></span>일정</a></li>" +
			"       		<li class='_spi_lnk_me2post spi_lnk_me2' style='display:none;'><a href='#' class='_spi_lnk_me2post'><span class='_spi_lnk_me2post'></span>미투데이</a></li>" +
			"       		<li class='_spi_lnk_facebook spi_lnk_facebook' style='display:none;'><a href='#' class='_spi_lnk_facebook'><span class='_spi_lnk_facebook'></span>페이스북</a></li>" +
			"       		<li class='_spi_lnk_twitter spi_lnk_twitter' style='display:none;'><a href='#' class='_spi_lnk_twitter'><span class='_spi_lnk_twitter'></span>트위터</a></li>" +
			"			    <li class='_spi_lnk_yozm spi_lnk_yozm' style='display:none;'><a href='#' class='_spi_lnk_yozm'><span class='_spi_yozm'></span>요즘</a></li>" +
			"       	</ul>" +
			"   	</li>" +
			"   </ul>" +
			"	<div class='_ly_bmk_ok spi_ly_pop' style='display:none;left:57px;top:31px;width:242px'>" +
			"		<p class='spi_dsc'>북마크 되었습니다.<br> <a href='http://me.naver.com/tab/bookmark.nhn' target='_blank' class='_ly_bmk_ok_cls _ly_bmk_lnk spi_impact'>네이버me 북마크함 가기</a></p>" +
			"		<button class='_ly_bmk_ok_cls spi_close' type='button'><span class='_ly_bmk_ok_cls'></span><em class='_ly_bmk_ok_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_bmk_cnf spi_ly_pop' style='display:none;left:57px;top:31px;width:242px'>" +
			"		<p class='spi_dsc'>현재 북마크 되어있습니다.<br> 북마크를 해제하시겠습니까?</p>" +
			"		<div class='spi_btn'>" +
			"			<a href='#' class='spi_y'><span class='_ly_bmk_cnf_y'></span>예</a>" +
			"			<a href='#' class='spi_n'><span class='_ly_bmk_cnf_n'></span>아니오</a>" +
			"		</div>" +
			"		<button class='_ly_bmk_cnf_cls spi_close' type='button'><span class='_ly_bmk_cnf_cls'></span><em class='_ly_bmk_cnf_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_bmk_err spi_ly_pop' style='display:none;left:60px;top:31px;width:243px'>" +
			"		<p class='spi_dsc'>서버 접속이 원활하지 않습니다.<br> 잠시 후 다시 시도해 주십시오.</p>" +
			"		<button class='_ly_bmk_err_cls spi_close' type='button'><span class='_ly_bmk_err_cls'></span><em class='_ly_bmk_err_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_bmk_ros spi_ly_pop' style='display:none;left:60px;top:31px;width:243px'>" +
			"		<p class='spi_dsc'>북마크 서비스 점검 중으로,<br>현재 북마크 읽기만 가능하오니<br>이용에 참고해 주시기 바랍니다.</p>" +
			"		<button class='_ly_bmk_ros_cls spi_close' type='button'><span class='_ly_bmk_ros_cls'></span><em class='_ly_bmk_ros_cls'>X</em></button>" +
			"	</div>" +
			"	<div class='_ly_url_ok spi_ly_pop' style='display:none;left:57px;top:31px;width:243px'>" +
            "		<p class='spi_dsc'>URL이 복사되었습니다.<br>원하는 곳에 붙여넣기(Ctrl+V)해 주세요.</p>" +
            "		<button type='button' class='_ly_url_ok_cls spi_close'><span class='_ly_url_ok_cls'></span><em class='_ly_url_ok_cls'>X</em></button>" +
            "	</div>" +
            "	<div class='_ly_pop spi_ly_pop ly_pop_v1' style='display:none;left:0;top:-82px;width:213px'>" +
            "		<p class='_ly_pop spi_dsc'>이 _feed_info_ 마음에 드셨다면<br><a href='http://me.naver.com/' target='_blank' class='_ly_pop _ly_me_lnk spi_impact'>네이버me</a>에서 편하게 받아보세요.</p>" +
            "		<div class='_ly_pop spi_ico_arr'></div>" +
            "	</div>" +
			"</div>"
	};

	/**
	 * 소셜플러그인 멀티모드 설정
	 *
	 * @param bMode true:멀티모드,false:싱글모드
	 */
	this.setMultiMode = function(bMode) {
		this._bMultiMode = bMode;
	};

	/**
	 * 소셜플러그인 Call 함수
	 *
	 * @param htInitData 초기데이터
	 * @param spiLayerId 소셜플러그인 레이어ID
	 */
	this.drawSpiLayer = function(htInitData, spiLayerId) {
		this._htInitData = htInitData || {};

		// 마크업 구성
		this._prepareMarkUp(spiLayerId);

		// 엘리먼트 정보 세팅
		this._setElement();

		// 이벤트 등록
		this._setupEvent();

		// 마크업 렌더링
		this._renderAll();

		// 비 멀티 모드 처리
		if (this._bMultiMode == false) {
			this._initializeMe2p();
		}
	};

	/**
	 * 소셜플러그인 제거 함수
	 */
	this.destroy = function() {
		var oSpiLayer = document.getElementById(this._sSpiLayerId);
		oSpiLayer.parentNode.removeChild(oSpiLayer);
		this._detroyEvent();
		this._htElement = {};
		this._htInitData = {};
	};

	/**
	 * Display On/Off 여부 확인
	 */
	this._isDisplayOn = function(btn) {
		var bDisplayOn = false;
		var htBtnData = this._htInitData[btn];

		if (typeof htBtnData == "undefined") {
			bDisplayOn = true;
		}
		else if (typeof htBtnData["display"] == "undefined") {

			bDisplayOn = true;
		}
		else if (htBtnData["display"] === "on") {
			bDisplayOn = true;
		}

		return bDisplayOn;
	};

	/**
	 * 마크업을 생성 및 디스플레이 한다.
	 */
	this._prepareMarkUp = function(spiLayerId) {
		var style = "standard", text = "black", cssStyle = "standard";
		var markupStr = "", layerAlign = "default";

		// 소셜플러그인 스타일
		if (this._htInitData["style"]) {
			style = this._htInitData["style"];
		}

		// 정렬 스타일
		if (this._htInitData["align"]) {
			this._sAlign = this._htInitData["align"];
		}

		// 마크업 & CSS 스타일
		switch (style) {
			case "standard":
				this._sMarkupStyle = "standard";
				this._sCssStyle    = "standard";
				break;
			case "text":
				this._sMarkupStyle = "text";
				this._sCssStyle    = "standard";
				break;
			case "community":
				this._sMarkupStyle = "community";
				this._sCssStyle    = "standard";
				break;
			case "standard_quirks":
				this._sMarkupStyle = "standard";
				this._sCssStyle    = "standard";
				break;
			case "text_quirks":
				this._sMarkupStyle = "text";
				this._sCssStyle    = "standard";
				break;
			case "community_quirks":
				this._sMarkupStyle = "community";
				this._sCssStyle    = "standard";
				break;
			default:
				this._sMarkupStyle = "standard";
				this._sCssStyle    = "standard";
				break;
		}

		markupStr = this._htMarkUp[this._sMarkupStyle];

		// 랜덤 ID 설정
		this._sUniqueId = this._getRandomId();

		// 레이어ID 설정
		this._sSpiLayerId = "spi_layer_" + this._sUniqueId;
		markupStr = markupStr.replace(/_spi_layer_id_/gi, this._sSpiLayerId);

		// 텍스트 컬러 스타일 설정
		if (this._htInitData["text"]) {
			text = this._htInitData["text"];
			if ((this._sMarkupStyle.indexOf("text") > -1 || this._sMarkupStyle.indexOf("community") > -1) && text == "white") {
				markupStr = markupStr.replace(/_text_style_/gi, "spi_txt");
			}
			else {
				markupStr = markupStr.replace(/_text_style_/gi, "");
			}
		}

		// 카페/블로그 요청 임시 텍스트&바 클래스 설정
		if (this._sMarkupStyle.indexOf("text") > -1 || this._sMarkupStyle.indexOf("community") > -1) {
			if (this._isNotNull(this._htInitData["textClass"])) {
				markupStr = markupStr.replace(/_text_class_/gi, this._htInitData["textClass"]);
			}
			else {
				markupStr = markupStr.replace(/_text_class_/gi, "");
			}

			if (this._isNotNull(this._htInitData["barClass"])) {
				markupStr = markupStr.replace(/_bar_class_/gi, this._htInitData["barClass"]);
			}
			else {
				markupStr = markupStr.replace(/_bar_class_/gi, "");
			}
		}

		// me 구독 안내레이어 멘트
		if (this._isDisplayOn("me")) {
			markupStr = markupStr.replace(/_feed_info_/gi, this._htInitData["me"]["feedInfo"]);
		}

		// 미투 플러그인 설정
		markupStr = markupStr.replace(/_me2key_/gi, this._htInitData["me2key"]);
		markupStr = markupStr.replace(/_me2source_/gi, this._getSourceUrl("me2day"));

		// 보내기 레이어 유니크ID 설정
		markupStr = markupStr.replace(/_spi_btn_release_/gi, "_spi_btn_release");

		// CSS 로딩
		this._appendStyleSheet(this._sCssStyle);

		// 마크업 출력
		document.getElementById(spiLayerId).innerHTML = markupStr;
	};

	/**
	 * CSS 동적추가 함수
	 */
	this._appendStyleSheet = function(style) {
		var styleId = "spi_style_" + style;

		if (this._isNotNull(document.getElementById(styleId))) {
			return;
		}

		var elHead = document.getElementsByTagName("head")[0];
		var isWindows = (navigator.userAgent.indexOf("Windows") > -1);
		var isIE = (navigator.userAgent.indexOf("MSIE") > -1);
		var isIE6 = (navigator.appVersion.indexOf("MSIE 6") > -1);

		var elStyle = document.createElement("link");
		elStyle.id = styleId;
		elStyle.rel = "stylesheet";
		elStyle.type = "text/css";
		elStyle.href = this._htStyleSheet[style + (isWindows && isIE && isIE6 ? "_ie6" : "")];
		elHead.appendChild(elStyle);
	};

	/**
	 * 엘리먼트 정보 세팅함수
	 *
	 * @param {Element} elContainer 좌측메뉴 영역 Container 엘리먼트
	 */
	this._setElement = function() {
		var style = this._htInitData["style"];
		var spiArea = document.getElementById(this._sSpiLayerId);

		this._htElement["spi_area"]    = spiArea;
		this._htElement["spi_lst"]     = this._getElementByClass(spiArea, "spi_lst");			// Buttons
		this._htElement["me"]          = this._getElementByClass(spiArea, "_spi_btn_me");		// 구독
		this._htElement["bookmark"]    = this._getElementByClass(spiArea, "_spi_btn_bookmark");	// 북마크
		this._htElement["me2"]         = this._getElementByClass(spiArea, "_spi_btn_me2day");	// 미투
		this._htElement["me2count"]    = this._getElementByClass(spiArea, "_spi_btn_me2count");	// 미투카운트
		this._htElement["me2layer"]    = this._getElementByClass(spiArea, "_me2button_layer");	// 미투레이어
		this._htElement["release_btn"] = this._getElementByClass(spiArea, "_spi_btn_release");	// 보내기버튼
		this._htElement["release"]     = this._getElementByClass(spiArea, "_spi_lst_release");	// 보내기
		this._htElement["cpurl"]       = this._getElementByClass(spiArea, "_spi_lnk_url");		// URL복사
		this._htElement["mail"]        = this._getElementByClass(spiArea, "_spi_lnk_mail");		// 메일
		this._htElement["cafe"]        = this._getElementByClass(spiArea, "_spi_lnk_cafe");		// 카페
		this._htElement["blog"]        = this._getElementByClass(spiArea, "_spi_lnk_blog");		// 블로그
		this._htElement["memo"]        = this._getElementByClass(spiArea, "_spi_lnk_memo");		// 메모
		this._htElement["calendar"]    = this._getElementByClass(spiArea, "_spi_lnk_calendar");	// 일정
		this._htElement["me2post"]     = this._getElementByClass(spiArea, "_spi_lnk_me2post");	// 미투하기
		this._htElement["facebook"]    = this._getElementByClass(spiArea, "_spi_lnk_facebook");	// 페이스북
		this._htElement["twitter"]     = this._getElementByClass(spiArea, "_spi_lnk_twitter");	// 트위터
		this._htElement["yozm"]        = this._getElementByClass(spiArea, "_spi_lnk_yozm");		// 요즘
		this._htElement["bmk_ok"]      = this._getElementByClass(spiArea, "_ly_bmk_ok");		// 북마크(추가성공)
		this._htElement["bmk_cnf"]     = this._getElementByClass(spiArea, "_ly_bmk_cnf");		// 북마크(삭제컨펌)
		this._htElement["bmk_err"]     = this._getElementByClass(spiArea, "_ly_bmk_err");		// 북마크(통신오류)
		this._htElement["bmk_ros"]     = this._getElementByClass(spiArea, "_ly_bmk_ros");		// 북마크(점검중)
		this._htElement["cpurl_ok"]    = this._getElementByClass(spiArea, "_ly_url_ok");		// URL복사 안내
		this._htElement["me_pop"]      = this._getElementByClass(spiArea, "_ly_pop");			// me구독 안내
		//this._htElement["event"]       = this._getElementByClass(spiArea, "_spi_lnk_event");	// 이벤트

		// 구분자 엘리먼트
		if (style.indexOf("text") > -1 || style.indexOf("community") > -1) {
			this._htElement["sep1"] = this._getElementByClass(spiArea, "_spi_sep1");
			this._htElement["sep2"] = this._getElementByClass(spiArea, "_spi_sep2");
			this._htElement["sep3"] = this._getElementByClass(spiArea, "_spi_sep3");
		}

		// console.log(this._htElement);
	};

	/**
	 * 이벤트 핸들러를 등록한다.
	 */
	this._setupEvent = function() {
		// onclick
		this._attachEvent(this._htElement["spi_area"], "click", spi.util.fn(this._onClick, this).bind());
		//this._attachEvent(document, "click", spi.util.fn(this._onClick, this).bind());

		// onmouseover
		this._attachEvent(this._htElement["me"], "mouseover", spi.util.fn(this._onMouseOver, this).bind());
		this._attachEvent(this._htElement["me_pop"], "mouseover", spi.util.fn(this._onMouseOver, this).bind());

		// onmouseout
		this._attachEvent(this._htElement["me"], "mouseout", spi.util.fn(this._onMouseOut, this).bind());
		this._attachEvent(this._htElement["me_pop"], "mouseout", spi.util.fn(this._onMouseOut, this).bind());
	};

	/**
	 * 이벤트 핸들러를 제거한다.
	 */
	this._detroyEvent = function() {
		// onclick
		this._detachEvent(this._htElement["spi_area"], "click", spi.util.fn(this._onClick, this).bind());

		// onmouseover
		this._detachEvent(this._htElement["me"], "mouseover", spi.util.fn(this._onMouseOver, this).bind());
		this._detachEvent(this._htElement["me_pop"], "mouseover", spi.util.fn(this._onMouseOver, this).bind());

		// onmouseout
		this._detachEvent(this._htElement["me"], "mouseout", spi.util.fn(this._onMouseOut, this).bind());
		this._detachEvent(this._htElement["me_pop"], "mouseout", spi.util.fn(this._onMouseOut, this).bind());
	};

	/*
	 * 마우스 클릭 이벤트 핸들러
	 *
	 * @param {$Event} weClick 마우스 클릭 이벤트 객체
	 */
	this._onClick = function() {
		var sClickLog = null;
		var e = this._htElement;
		var ev = arguments[0] || window.event;
		var el = ev.target || ev.srcElement;
		//console.log(el);

		if (this._hasClass(el, "_spi_btn_me")) {
			sClickLog = "me";
			this._preventEvent(ev);
			this._openMeFeedPopup();
		}
		else if (this._hasClass(el, "_spi_btn_bookmark")) {
			sClickLog = "bookmark";
			this._preventEvent(ev);
			this._checkBookmarkData();
		}
		else if (this._hasClass(el, "_spi_btn_me2day")) {
			sClickLog = "me2";
			this._preventEvent(ev);
			this._closeAllLayer("me2day");
		}
		else if (this._hasClass(el, "_spi_btn_me2count")) {
			sClickLog = "me2count";
			this._closeAllLayer("me2day");
		}
		else if (this._hasClass(el, "_spi_lnk_event")) {
			sClickLog = "event";
			this._preventEvent(ev);
			this._sendEventLog("click", sClickLog);
			window.open('http://blog.naver.com/naver_diary/150147535886', '', '');
		}
		else if (this._hasClass(el, "_spi_lnk_url")) {
			sClickLog = "copyurl";
			this._preventEvent(ev);
			this._openCopyUrl();
		}
		else if (this._hasClass(el, "_spi_lnk_mail")) {
			sClickLog = "mail";
			this._preventEvent(ev);
			this._openMailPopup();
		}
		else if (this._hasClass(el, "_spi_lnk_cafe")) {
			sClickLog = "cafe";
			this._preventEvent(ev);
			this._openCafePopup();
		}
		else if (this._hasClass(el, "_spi_lnk_blog")) {
			sClickLog = "blog";
			this._preventEvent(ev);
			this._openBlogPopup();
		}
		else if (this._hasClass(el, "_spi_lnk_memo")) {
			sClickLog = "memo";
			this._preventEvent(ev);
			this._openMemoPopup();
		}
		else if (this._hasClass(el, "_spi_lnk_calendar")) {
			sClickLog = "calendar";
			this._preventEvent(ev);
			this._openCalendarPopup();
		}
		else if (this._hasClass(el, "_spi_lnk_me2post")) {
			sClickLog = "me2post";
			this._preventEvent(ev);
			this._openMe2PostPopup();
		}
		else if (this._hasClass(el, "_spi_lnk_facebook")) {
			sClickLog = "facebook";
			this._preventEvent(ev);
			this._openFacebookPopup();
		}
		else if (this._hasClass(el, "_spi_lnk_twitter")) {
			sClickLog = "twitter";
			this._preventEvent(ev);
			this._openTwitterPopup();
		}
		else if (this._hasClass(el, "_spi_lnk_yozm")) {
			sClickLog = "yozm";
			this._preventEvent(ev);
			this._openYozmPopup();
		}
		else if (this._hasClass(el, "_spi_btn_release")) {
			sClickLog = "release";
			this._preventEvent(ev);
			this._closeAllLayer("release");
			this._toggle(e["release"]);
		}
		else if (this._hasClass(el, "_ly_bmk_ok_cls")) {
			this._hide(e["bmk_ok"]);
			if (this._hasClass(el, "_ly_bmk_lnk")) {
				sClickLog = "bmklink";
			}
		}
		else if (this._hasClass(el, "_ly_bmk_cnf_y")) {
			this._preventEvent(ev);
			this._hide(e["bmk_cnf"]);
			this._deleteBookmarkData();
		}
		else if (this._hasClass(el, "_ly_bmk_cnf_n")) {
			this._preventEvent(ev);
			this._hide(e["bmk_cnf"]);
		}
		else if (this._hasClass(el, "_ly_bmk_cnf_cls")) {
			this._preventEvent(ev);
			this._hide(e["bmk_cnf"]);
		}
		else if (this._hasClass(el, "_ly_bmk_err_cls")) {
			this._preventEvent(ev);
			this._hide(e["bmk_err"]);
		}
		else if (this._hasClass(el, "_ly_bmk_ros_cls")) {
			this._preventEvent(ev);
			this._hide(e["bmk_ros"]);
		}
		else if (this._hasClass(el, "_ly_url_ok_cls")) {
			this._preventEvent(ev);
			this._hide(e["cpurl_ok"]);
		}
		else if (this._hasClass(el, "_ly_me_lnk")) {
			sClickLog = "melink";
		}

		// 보내기 레이어 닫기
		if (!this._hasClass(el, "_spi_btn_release")) {
			this._hide(e["release"]);
		}

		// nEvent Log 전송
		this._sendEventLog("click", sClickLog);
	};

	/**
	 * 기본 이벤트 막기(<a> 태그 href 속성)
	 *
	 * @param {$Event} weMouseOver 마우스 오버 이벤트 객체
	 */
	this._preventEvent = function(event) {
		if (event.preventDefault) {
			event.preventDefault();
		}
		else {
			event.returnValue = false;
		}
	};

	/**
	 * 마우스 오버 이벤트 핸들러
	 *
	 * @param {$Event} weMouseOver 마우스 오버 이벤트 객체
	 */
	this._onMouseOver = function() {
		var e = this._htElement;
		var ev = arguments[0] || window.event;
		var el = ev.target || ev.srcElement;

		if (this._hasClass(el, "_spi_btn_me") || this._hasClass(el, "_ly_pop")) {
			clearTimeout(this._tTimer);
			this._show(e["me_pop"]);
			spi.util.offset(e["me_pop"], spi.util.offset(e["me_pop"]).top, spi.util.offset(e["me"]).left);
		}
	};

	/**
	 * 마우스 아웃 이벤트 핸들러
	 *
	 * @param {$Event} weMouseOut 마우스 아웃 이벤트 객체
	 */
	this._onMouseOut = function() {
		var e = this._htElement;
		var ev = arguments[0] || window.event;
		var el = ev.target || ev.srcElement;

		if (this._hasClass(el, "_spi_btn_me") || this._hasClass(el, "_ly_pop")) {
			this._tTimer = setTimeout(
				spi.util.fn(function() {
					this._hide(e["me_pop"]);
					this._tTimer = null;
				}, this).bind(), 1000);
		}
	};

	/**
	 * 오픈 된 모든 레이어를 닫는다.
	 */
	this._closeAllLayer = function(exceptLayer) {
		var e = this._htElement;

		if (exceptLayer != "bmk_ok") {
			this._hide(e["bmk_ok"]);
		}

		if (exceptLayer != "bmk_cnf") {
			this._hide(e["bmk_cnf"]);
		}

		if (exceptLayer != "bmk_err") {
			this._hide(e["bmk_err"]);
		}

		if (exceptLayer != "bmk_ros") {
			this._hide(e["bmk_ros"]);
		}

		if (exceptLayer != "cpurl_ok") {
			this._hide(e["cpurl_ok"]);
		}

		if (exceptLayer != "release") {
			this._hide(e["release"]);
		}

		if (exceptLayer != "me2day") {
			if (this._isDisplayOn("me2day")) {
				me2p.MetooHandler.get(this._getSourceUrl("me2day")).closeLayer();
			}
		}
	};

	/**
	 * 각 버튼의 display 속성에 따라 버튼을 on/off 처리 한다.
	 */
	this._renderAll = function() {
		var st = this._htInitData["style"],
			el = this._htElement;

		this._isDisplayOn("me")			? this._show(el["me"])			: this._hide(el["me"]);
		this._isDisplayOn("bookmark")	? this._show(el["bookmark"])	: this._hide(el["bookmark"]);
		this._isDisplayOn("me2day")		? this._show(el["me2"])			: this._hide(el["me2"]);
		this._isDisplayOn("postbtn")	? this._show(el["release_btn"])	: this._hide(el["release_btn"]);
		this._isDisplayOn("copyurl")	? this._show(el["cpurl"])		: this._hide(el["cpurl"]);
		this._isDisplayOn("mail")		? this._show(el["mail"])		: this._hide(el["mail"]);
		this._isDisplayOn("cafe")		? this._show(el["cafe"])		: this._hide(el["cafe"]);
		this._isDisplayOn("blog")		? this._show(el["blog"])		: this._hide(el["blog"]);
		this._isDisplayOn("memo")		? this._show(el["memo"])		: this._hide(el["memo"]);
		this._isDisplayOn("calendar")	? this._show(el["calendar"])	: this._hide(el["calendar"]);
		this._isDisplayOn("me2post")	? this._show(el["me2post"])		: this._hide(el["me2post"]);
		this._isDisplayOn("facebook")	? this._show(el["facebook"])	: this._hide(el["facebook"]);
		this._isDisplayOn("twitter")	? this._show(el["twitter"])		: this._hide(el["twitter"]);
		this._isDisplayOn("yozm") 		? this._show(el["yozm"])		: this._hide(el["yozm"]);
		//this._isDisplayOn("memo") 		? this._show(el["event"])		: this._hide(el["event"]); // 메모 이벤트

		if (st.indexOf("text") > -1 || st.indexOf("community") > -1) {
			if (this._isDisplayOn("postbtn") && (this._isDisplayOn("me") || this._isDisplayOn("bookmark") || this._isDisplayOn("me2day"))) {
				// do nothing
			}
			else{
				this._hide(el["sep3"]);
			}

			if (this._isDisplayOn("me2day") && (this._isDisplayOn("me") || this._isDisplayOn("bookmark"))) {
				// do nothing
			}
			else{

				this._hide(el["sep2"]);
			}

			if (this._isDisplayOn("bookmark") && (this._isDisplayOn("me"))) {
				// do nothing
			}
			else{
				this._hide(el["sep1"]);
			}
		}
	};

	/**
	 * 미투버튼 초기화 및 Callback 등록
	 */
	this._initializeMe2p = function() {
		var sourceUrl = this._getSourceUrl("me2day");

		// 싱글모드 일때만 초기화 수행
		if (this._bMultiMode == false) {
			me2p.initialize();
		}

		// 미투 하기 전
		me2p.MetooHandler.get(sourceUrl).registerEvent(
			"onBeforeMetoo",
			spi.util.fn(function() {
				this._nMe2Count = parseInt(this._htElement["me2count"].innerHTML);
			}, this).bind());

		// 미투 완료 후
		me2p.MetooHandler.get(sourceUrl).registerEvent(
			"onAfterMetoo",
			spi.util.fn(function() {
				if (this._nMe2Count < parseInt(this._htElement["me2count"].innerHTML)) {
					this._sendEventLog("finish", "me2");
				}
			}, this).bind());

		// 미투 메시지 레이어 열리기 직전
		me2p.MetooHandler.get(sourceUrl).registerEvent(
			"onBeforeShowLayer",
			spi.util.fn(function() {
				this._closeAllLayer();
			}, this).bind());

		// 미투 메시지 레이어 열린 후
		me2p.MetooHandler.get(sourceUrl).registerEvent(
			"onShowLayer",
			spi.util.fn(function() {
				var elMe2Btn   = this._htElement["me2"],
					elMe2Layer = this._htElement["me2layer"];
				spi.util.offset(elMe2Layer,
					spi.util.offset(elMe2Btn).top + elMe2Btn.offsetHeight + 3,
					spi.util.offset(elMe2Btn).left - (this._sAlign == "right" ? elMe2Layer.offsetWidth - elMe2Btn.offsetWidth : 0));
			}, this).bind());
	};

	/**
	 * me 구독하기 팝업
	 */
	this._openMeFeedPopup = function() {
		if (this._isNotNull(this._htInitData["me"]["fCallback"]) &&
				typeof this._htInitData["me"]["fCallback"] == "function") {
			this._htInitData["me"]["fCallback"]();
			return;
		}

		var target = "_feedPopup", formId = "_feedPopupForm";
		var htParams = this._htInitData["me"];
		var elForm = document.getElementById(formId);

		if (elForm == null) {
			elForm = document.createElement("form");
			elForm.id = formId;
			elForm.action = this._htAPI["API201"] + htParams["targetUrl"];
			elForm.method = "post";
			elForm.target = target;
		}

		this._appendChilds(elForm,
				[
				 this._createInputElement("hidden", "popupType",     htParams["popupType"]),
				 this._createInputElement("hidden", "callbackUrl",   htParams["callbackUrl"]),
				 this._createInputElement("hidden", "serviceId",     htParams["serviceId"]),
				 this._createInputElement("hidden", "feedGroupId",   htParams["feedGroupId"]),
				 this._createInputElement("hidden", "feedGroupUrl",  htParams["feedGroupUrl"]),
				 this._createInputElement("hidden", "feedGroupName", htParams["feedGroupName"]),
				 this._createInputElement("hidden", "ek",            this._htFinishLog["me"]["key"] + "." + this._htInitData["evkey"]),
				 this._createInputElement("hidden", "ev",            this._htFinishLog["me"]["baseUrl"]),
				 ]);

		this._appendChilds(elForm, this._createInputElementArr("hidden", "feedId",   htParams["feedId"]));
		this._appendChilds(elForm, this._createInputElementArr("hidden", "feedName", htParams["feedName"]));
		this._appendChilds(elForm, this._createInputElementArr("hidden", "selected", htParams["selected"]));
		this._appendChilds(elForm, this._createInputElementArr("hidden", "feedType", htParams["feedType"]));

		document.body.appendChild(elForm);

		this._openPopupToCenter("", target, 380, 400, 'location=no,resize=no,scrollbars=no,resizable=no');

		this._setFormCharset(elForm, 'utf-8');
		elForm.submit();
		this._setFormCharset(elForm, 'utf-8');

		elForm.parentNode.removeChild(elForm);
	};

	/**
	 * 북마크 존재 검사 Ajax
	 */
	this._checkBookmarkData = function () {
		var params = "[\"" + this._getSourceUrl("bookmark") + "\"]";

		this._setBookmarkLoading("on");

		spi.util.requestJsonp(
			this._htAPI['API301'],
			{
				urls : params
			},
			spi.util.fn(this._onCheckBookmarkData, this).bind());
	};

	/**
	 * 북마크 존재 검사 Ajax CallBack
	 */
	this._onCheckBookmarkData = function(oResponse) {
		var htResult = spi.util.toJson(oResponse);

		this._setBookmarkLoading("off");

		if (htResult.bookmarkError) {
			var result = htResult.bookmarkError;
			if (result.responseCode == 900) {
				// 비로그인 상태
				if (this._isNotNull(this._htInitData["onLoginRedirect"]) &&
						typeof this._htInitData["onLoginRedirect"] == "function") {
					this._htInitData["onLoginRedirect"]();
				}
				else {
					this._redirectLoginPage(document.location.href);
				}
			}
			else {
				// 파라미터 오류 or 기타 오류
				this._showBookmarkLayer(this._htElement["bmk_err"]);
			}
		}
		else if (htResult.list && htResult.list.length == 1) {
			var result = htResult.list[0];
			if (result.exist == false) {
				this._postBookmarkData();
			}
			else {
				this._sBmkUrlId = result.urlId;
				this._showBookmarkLayer(this._htElement["bmk_cnf"]);
			}
		}
		else {
			this._showBookmarkLayer(this._htElement["bmk_err"]);
		}
	};

	/**
	 * 북마크 추가 처리 Ajax
	 */
	this._postBookmarkData = function() {
		var titleStr = this._htInitData["title"];
		var source = this._htInitData["evkey"];
		var urlStr = this._getSourceUrl("bookmark");

		this._setBookmarkLoading("on");

		spi.util.requestJsonp(
			this._htAPI['API302'],
			{
				title     : titleStr,
				url       : urlStr,
				isPrivate : true,
				type      : 'B',
				source    : source
			},
			spi.util.fn(this._onPostBookmarkData, this).bind());
	};

	/**
	 * 북마크 추가 처리 Ajax CallBack
	 */
	this._onPostBookmarkData = function(oResponse) {
		var htResult = spi.util.toJson(oResponse);

		this._setBookmarkLoading("off");

		if (htResult && htResult.bookmarkSave) {
			var result = htResult.bookmarkSave;
			if (result.ros == 1) {
				this._showBookmarkLayer(this._htElement["bmk_ros"]);
			}
			else {
				if ((result.responseCode == 0) || (result.responseCode == 1)) {
					this._showBookmarkLayer(this._htElement["bmk_ok"]);
					this._sendEventLog("finish", "bookmark");

				}
				else {
					this._showBookmarkLayer(this._htElement["bmk_err"]);
				}
			}
		}
		else {
			this._showBookmarkLayer(this._htElement["bmk_err"]);
		}
	};

	/**
	 * 북마크 삭제 처리 Ajax
	 */
	this._deleteBookmarkData = function() {
		var urlId = this._sBmkUrlId;

		this._setBookmarkLoading("on");

		spi.util.requestJsonp(
			this._htAPI['API303'],
			{
				urlId : urlId
			},
			spi.util.fn(this._onDeleteBookmarkData, this).bind());
	};

	/**
	 * 북마크 삭제 처리 Ajax CallBack
	 */
	this._onDeleteBookmarkData = function(oResponse) {
		var htResult = spi.util.toJson(oResponse);

		this._setBookmarkLoading("off");

		if (htResult && htResult.bookmarkDelete) {
			var result = htResult.bookmarkDelete;
			if (result.ros == 1) {
				this._showBookmarkLayer(this._htElement["bmk_ros"]);
			}
			else {
				if ((result.responseCode == 0) || (result.responseCode == 1)) {
					// do nothing
				}
				else {
					this._showBookmarkLayer(this._htElement["bmk_err"]);
				}
			}
		}
		else {
			this._showBookmarkLayer(this._htElement["bmk_err"]);
		}
	};

	/**
	 * 북마크 로딩 이미지 제어
	 */
	this._setBookmarkLoading = function(style) {
		var sLoadStyle = '', sEndStyle = '', sSepDisp = '';
		var elBookmark = this._getElementByClass(this._htElement['spi_area'], '_spi_bmk_load');
		var sLoadIcon = (this._htInitData['style'] == 'standard' ? 'ico_loading2.gif' : 'ico_loading.gif');
		var sBarClass = (this._htInitData['barClass'] ? this._htInitData['barClass'] : '');

		if (this._htInitData['style'] == 'text' || this._htInitData['style'] == 'community') {
			if (this._isDisplayOn('bookmark') && this._isDisplayOn('me')) {
				sSepDisp = 'block';
			} else {
				sSepDisp = 'none';
			}
		}

		sLoadStyle = '<img src="http://bookmarkimgs.naver.com/img/' + sLoadIcon + '" ' +
				'width="14" height="14" alt="로딩중" class="loading">북마크' +
				'<em class="_spi_sep1 ' + sBarClass + '" style="display:' + sSepDisp + ';"></em>';
		sEndStyle = '<span class="_spi_btn_bookmark"></span>북마크' +
				'<em class="_spi_sep1 ' + sBarClass + '" style="display:' + sSepDisp + ';"></em>';

		if (style == "on") {
			elBookmark.innerHTML = sLoadStyle;
		} else {
			elBookmark.innerHTML = sEndStyle;
		}
	};

	/**
	 * 북마크 관련 레이어 표시
	 */
	this._showBookmarkLayer = function(el) {
		var elBmk = this._htElement["bookmark"],
			style = this._htInitData["style"];

		this._closeAllLayer();
		this._show(el);

		spi.util.offset(el,
			spi.util.offset(elBmk).top + elBmk.offsetHeight + 3,
			spi.util.offset(elBmk).left - (this._sAlign == "right" ? el.offsetWidth - elBmk.offsetWidth : 0));
	};

	/**
	 * URL 복사
	 */
	this._openCopyUrl = function() {
		var g4_is_gecko  = navigator.userAgent.toLowerCase().indexOf("gecko") != -1;
		var g4_is_ie     = navigator.userAgent.toLowerCase().indexOf("msie") != -1;

		this._closeAllLayer();

		if (g4_is_gecko) {
			prompt("아래의 URL을 복사(Ctrl+C)하여\n원하는 곳에 붙여넣기(Ctrl+V)하세요.", this._htInitData["source"]);
		}
		else {
			var elRelease   = this._htElement["release_btn"],
				elCopyUrlOk = this._htElement["cpurl_ok"];

			window.clipboardData.setData("Text", this._htInitData["source"]);
			this._show(elCopyUrlOk);

			spi.util.offset(elCopyUrlOk,
					spi.util.offset(elRelease).top + elRelease.offsetHeight + 3,
					spi.util.offset(elRelease).left + elRelease.offsetWidth - elCopyUrlOk.offsetWidth);
		}
	};

	/**
	 * 메일 팝업
	 */
	this._openMailPopup = function() {
		this._getLogined(spi.util.fn(this._onPostMail, this).bind());
	};

	/**
	 * 메일 보내기 처리
	 */
	this._onPostMail = function(oResponse) {
		var htResult = spi.util.toJson(oResponse);
		var target = "_postMailPopup";
		var evkey = this._htFinishLog["mail"]["key"] + "." + this._htInitData["evkey"];
		var evvalue = this._htFinishLog["mail"]["baseUrl"];
		var url = this._htAPI["API501"] +
			"?svc=" + this._htInitData["mail"]["srvid"] +
			"&tmplUrl=" + encodeURIComponent(this._htInitData["mail"]["srvurl"]) +
			"&callbackUrl=" + encodeURIComponent(ectrackurl(evkey, evvalue));

		if (htResult.result == "success" && htResult.success == true) {
			this._openPopupToCenter(url, target, 900, 770);
		}
		else {
			this._openLoginForm(url, target, 900, 770);
		}
	};

	/**
	 * 카페 팝업
	 */
	this._openCafePopup = function() {
		var evkey = this._htFinishLog["cafe"]["key"] + "." + this._htInitData["evkey"];
		var evvalue = this._htFinishLog["cafe"]["baseUrl"];
		var target = "_cafePopup", formId = "_cafePopupForm";
		var htParams = this._htInitData["cafe"];
		var elForm = document.getElementById(formId);

		if (elForm == null) {
			elForm = document.createElement("form");
			elForm.id = formId;
			elForm.target = target;

			// 카페 Proxy Action 사용
			if (this._isNotNull(htParams["proxyAction"]) && this._isNotNull(htParams["proxyActionUrl"]) &&
					htParams["proxyAction"] === "on") {
				elForm.action = htParams["proxyActionUrl"];
				elForm.method = "get";
				this._appendGetParam(elForm, htParams["proxyActionUrl"]);
			}
			else {
				elForm.action = this._htAPI["API502"];
				elForm.method = "post";
			}
		}

		this._appendChilds(elForm,
			[
			 this._createInputElement("hidden", "source_type",     htParams["sourceType"]),
			 this._createInputElement("hidden", "title",           this._htInitData["title"]),
			 this._createInputElement("hidden", "source_title",    htParams["sourceTitle"]),
			 this._createInputElement("hidden", "source_url",      this._htInitData["source"]),
			 this._createInputElement("hidden", "source_contents", htParams["sourceContents"]),
			 this._createInputElement("hidden", "source_form",     htParams["sourceForm"]),
			 this._createInputElement("hidden", "callbackUrl",     ectrackurl(evkey, evvalue))
			]);

		document.body.appendChild(elForm);

		this._openPopupToCenter("", target, 380, 400);

		this._setFormCharset(elForm, 'euc-kr');
		elForm.submit();
		this._setFormCharset(elForm, 'utf-8');

		elForm.parentNode.removeChild(elForm);
	};

	/**
	 * 블로그 팝업
	 */
	this._openBlogPopup = function() {
		var evkey = this._htFinishLog["blog"]["key"] + "." + this._htInitData["evkey"];
		var evvalue = this._htFinishLog["blog"]["baseUrl"];
		var target = "_blogPopup", formId = "_blogPopupForm";
		var htParams = this._htInitData["blog"];
		var elForm = document.getElementById(formId);

		if (elForm == null) {
			elForm = document.createElement("form");
			elForm.id = formId;
			elForm.target = target;

			// 블로그 Proxy Action 사용
			if (this._isNotNull(htParams["proxyAction"]) && this._isNotNull(htParams["proxyActionUrl"]) &&
					htParams["proxyAction"] === "on") {
				elForm.action = htParams["proxyActionUrl"];
				elForm.method = "get";
				this._appendGetParam(elForm, htParams["proxyActionUrl"]);
			}
			else {
				elForm.action = this._htAPI["API503"];
				elForm.method = "post";
			}
		}

		this._appendChilds(elForm,
			[
			 this._createInputElement("hidden", "blogId",          htParams["blogId"]),
			 this._createInputElement("hidden", "source_type",     htParams["sourceType"]),
			 this._createInputElement("hidden", "title",           this._htInitData["title"]),
			 this._createInputElement("hidden", "source_title",    htParams["sourceTitle"]),
			 this._createInputElement("hidden", "source_url",      this._htInitData["source"]),
			 this._createInputElement("hidden", "source_contents", htParams["sourceContents"]),
			 this._createInputElement("hidden", "source_form",     htParams["sourceForm"]),
			 this._createInputElement("hidden", "callbackType",    "image"),
			 this._createInputElement("hidden", "callbackUrl",     ectrackurl(evkey, evvalue))
			]);

		document.body.appendChild(elForm);

		this._openPopupToCenter("", target, 380, 400);

		this._setFormCharset(elForm, 'euc-kr');
		elForm.submit();
		this._setFormCharset(elForm, 'utf-8');

		elForm.parentNode.removeChild(elForm);
	};

	/**
	 * 메모 팝업
	 */
	this._openMemoPopup = function() {
		var target = "_memoPostPopup";
		var postUrl = this._htAPI["API508"] +
			"?linkDisplayText=" + encodeURIComponent(this._htInitData["title"]) +
			"&linkUrl=" + encodeURIComponent(this._htInitData["source"]);

		this._openPopupToCenter(postUrl, target, 540, 520);

	};

	/**
	 * 일정 팝업
	 */
	this._openCalendarPopup = function() {
		var target = "_calendarPostPopup", formId = "_cakendarPopupForm";
		var elForm = document.getElementById(formId);

		if (elForm == null) {
			elForm = document.createElement("form");
			elForm.id = formId;
			elForm.target = target;
			elForm.method = "get";
			elForm.action = this._htAPI["API509"];
		}

		this._appendChilds(elForm,
			[
			 this._createInputElement("hidden", "action",          "popup"),
			 this._createInputElement("hidden", "serviceId",       "41"),
			 this._createInputElement("hidden", "method",          "popup"),
			 this._createInputElement("hidden", "editable",        "true"),
			 this._createInputElement("hidden", "idx",             "1"),
			 this._createInputElement("hidden", "encoded",         "true"),
			 this._createInputElement("hidden", "content",         encodeURIComponent(this._htInitData["title"])),
			 this._createInputElement("hidden", "pageInfo",        encodeURIComponent(this._htInitData["title"])),
			 this._createInputElement("hidden", "pageUrl",         this._htInitData["source"]),
			 this._createInputElement("hidden", "origin",          document.location.href)
			]);

		document.body.appendChild(elForm);

		this._openPopupToCenter("", target, 540, 520, "location=no,resize=no,scrollbars=no,resizable=no");

		this._setFormCharset(elForm, 'utf-8');
		elForm.submit();
		this._setFormCharset(elForm, 'utf-8');

		elForm.parentNode.removeChild(elForm);
	};

	/**
	 * 미투 포스트 팝업
	 */
	this._openMe2PostPopup = function() {
		var htParams = this._htInitData["me2post"];

		if (htParams && this._isNotNull(htParams["shortUrl"]) && htParams["shortUrl"] === "off") {
			this._postMe2day(this._htInitData["source"]);
		}
		else {
			this._getShortUrl(this._htInitData["source"], spi.util.fn(this._onPostMe2day, this).bind());
		}
	};

	/**
	 * 미투 포스트 처리
	 *
	 * @param oResponse
	 */
	this._onPostMe2day = function(oResponse) {
		var htResult = spi.util.toJson(oResponse);

		if (htResult.code == "200") {
			this._postMe2day(htResult.result.url);
		}
	};

	/**
	 * 미투 포스트 처리
	 */
	this._postMe2day = function(sourceUrl) {
		var postUrl = "", target = "_me2postPopup";
		var bodyStr = "", shortUrl = "", tagStr = "", tagArr = null;

		// 미투 보내기 소환문법 적용
		bodyStr = this._htInitData["title"];
		bodyStr = bodyStr.replace(/"/gi, '\＂');
		bodyStr = bodyStr.replace(/&quot;/gi, '\＂');
		bodyStr = "＂" + bodyStr + "＂:" + sourceUrl;

		// 미투 태그 파라미터 생성
		if (this._isNotNull(this._htInitData["tags"])) {
			tagArr = this._htInitData["tags"];
			for (var i = 0; i < tagArr.length; i++) {
				if (i != 0) {
					tagStr = tagStr + "+";
				}
				tagStr = tagStr + tagArr[i];
			}
		}

		var postUrl = this._htAPI["API504"] +
			"?body=" + encodeURIComponent(bodyStr) +
			"&tag=" + encodeURIComponent(tagStr) +
			"&svc_type=2" +
			"&ek=" + encodeURIComponent(this._htFinishLog["me2post"]["key"] + "." + this._htInitData["evkey"]) +
			"&ev=" + encodeURIComponent(this._htFinishLog["me2post"]["baseUrl"]) +
			"&redirect_url=" + encodeURIComponent(document.location.href);

		this._openPopupToCenter(postUrl, target, 540, 520);
	};

	/**
	 * 페이스북 팝업
	 */
	this._openFacebookPopup = function() {
		var url = this._htAPI["API505"] + "?u=";

		if (this._isNotNull(this._htInitData["facebook"]) && this._isNotNull(this._htInitData["facebook"]["sourceUrl"])) {
			url = url + encodeURIComponent(this._htInitData["facebook"]["sourceUrl"]);
		}
		else {
			url = url + encodeURIComponent(this._htInitData["source"]);
		}

		this._openPopupToCenter(url, "facebookPopup", 804, 574);
	};

	/**
	 * 트위터 팝업
	 */
	this._openTwitterPopup = function() {
		this._getShortUrl(this._htInitData["source"], spi.util.fn(this._onPostTwitter, this).bind());
	};

	/**
	 * 트위터 포스팅 처리
	 * @param oResponse
	 */
	this._onPostTwitter = function(oResponse) {
		var text = "";
		var htResult = spi.util.toJson(oResponse);

		text = this._htInitData["title"];
		text = text.replace(/"/gi, '\＂');
		text = text.replace(/&quot;/gi, '\＂');

		if (htResult.code == "200") {
			var url = this._htAPI["API506"] +
				"?text=" + encodeURIComponent(text) +
				"&url=" + encodeURIComponent(htResult.result.url);
			this._openPopupToCenter(url, "twitterPopup", 640, 350);
		}
	};

	/**
	 * 요즘 팝업
	 */
	this._openYozmPopup = function() {
		var prefix = "", url = "";

		prefix = this._htInitData["title"];
		prefix = prefix.replace(/"/gi, '\＂');
		prefix = prefix.replace(/&quot;/gi, '\＂');

		url = this._htAPI["API507"] +
			"?prefix=" + encodeURIComponent(prefix) +
			"&link=" + encodeURIComponent(this._htInitData["source"]);

		this._openPopupToCenter(url, "yozmPopup", 455, 330);
	};

	/****************************************** 유틸리티 함수 ******************************************/

	/**
	 * 기능 별 별도 컨텐트URL 설정 얻기
	 */
	this._getSourceUrl = function(key) {
		var rtnStr = this._htInitData["source"];

		if (this._isNotNull(this._htInitData[key]) &&
				this._isNotNull(this._htInitData[key]["sourceUrl"])) {
			rtnStr = this._htInitData[key]["sourceUrl"];
		}

		return rtnStr;
	};

	/**
	 * 클래스명으로 엘리먼트 검색(첫번째 매칭 엘리먼트)
	 *
	 * @param el 소스 엘리먼트
	 * @param cls 검색 클래스명
	 */
	this._getElementByClass = function(el, cls) {
		return me2p.Sizzle("." + cls, el)[0];
	};

	/**
	 * 이벤트 추가 함수
	 *
	 * @param o 오브젝트
	 * @param e 이벤트명(click, mouseover, mouseout)
	 * @param h 이벤트 핸들러 func.
	 */
	this._attachEvent = function(o, e, h) {
		if (o.addEventListener) {
			o.addEventListener(e, h, false);
		} else if (o.attachEvent) {
			o.attachEvent("on" + e, h);
		} else {
			switch (e) {
				case "click":
					if (o.onclick) {
						o.onclick = h;
					}
					break;
				case "mouseover":
					if (o.onmouseover) {
						o.onmouseover = h;
					}
					break;
				case "mouseout":
					if (o.onmouseover) {
						o.onmouseout = h;
					}
					break;
			}
		}
	};

	/**
	 * 이벤트 제거 함수
	 *
	 * @param o
	 * @param e
	 */
	this._detachEvent = function(o, e, h) {
		if (o.removeEventListener) {
			o.removeEventListener(e, h, false);
		} else if (o.detachEvent) {
			o.detachEvent("on" + e, h);
		} else {
			switch (e) {
				case "click":
					if (o.onclick) {
						o.onclick = null;
					}
					break;
				case "mouseover":
					if (o.onmouseover) {
						o.onmouseover = null;
					}
					break;
				case "mouseout":
					if (o.onmouseout) {
						o.onmouseout = h;
					}
					break;
			}
		}
	};

	/**
	 * CSS display toggle
	 *
	 * @param e 엘리먼트
	 */
	this._toggle = function(e) {
		if (!e) return;
		if (e.style.display == 'none') {
			e.style.display = 'block';
		} else {
			e.style.display = 'none';
		}
	};

	/**
	 * CSS display hide
	 *
	 * @param e 엘리먼트
	 */
	this._hide = function(e) {
		if (!e) return;
		e.style.display = 'none';
	};

	/**
	 * CSS display show
	 *
	 * @param e 엘리먼트
	 */
	this._show = function(e) {
		if (!e) return;
		e.style.display = 'block';
	};

	/**
	 * CSS display show
	 *
	 * @param e 엘리먼트
	 */
	this._inlineShow = function(e) {
		if (!e) return;
		e.style.display = 'inline-block';
	};

	/**
	 * CSS Class 검사
	 *
	 * @param e 엘리먼트
	 * @param c CSS 클래스명
	 * @returns true/false
	 */
	this._hasClass = function(e, c) {
		return e.className.match(new RegExp('(\\s|^)' + c + '(\\s|$)'));
	};

	/**
	 * CSS Class 추가
	 *
	 * @param e 엘리먼트
	 * @param c CSS 클래스명
	 */
	this._addClass = function(e, c) {
		var nc = "";

		if(!e.className) {
			e.className = c;
		}
		else {
			nc = e.className;
			nc += " ";
			nc += c;
			e.className = nc;
		}
	};

	/**
	 * CSS Class 제거
	 *
	 * @param e 엘리먼트
	 * @param c CSS 클래스명
	 */
	this._removeClass = function(e, c) {
		if (this._hasClass(e, c)) {
			var reg = new RegExp('(\\s|^)' + c + '(\\s|$)');
			e.className = e.className.replace(reg, ' ');
		}
	};

	/**
	 * 랜덤한 값을 가져온다.
	 *
	 * @return {String} 랜덤값
	 */
	this._getRandomId = function() {
		return String(parseInt(Math.random() * 10000000000, 10));
	};

	/**
	 * Form CharacterSet 변경
	 *
	 * @param f 폼엘리먼트
	 * @param c Charset
	 */
	this._setFormCharset = function(f, c) {
		if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
			f.acceptCharset = c;
		} else {
			document.charset = c;
		}
	};

	/**
	 * NULL 체크 함수
	 */
	this._isNotNull = function(value) {
		return (value == null || value == 'undefined' || value == 'null') ? false : true;
	};

	/**
	 * QueryString 파싱 후 폼 엘리먼트로 추가
	 */
	this._appendGetParam = function(form, url) {
		var i, key, value, queryStr, arrParams, arrParam;
		if (url.indexOf("?") > -1) {
			queryStr = url.substring(url.indexOf("?") + 1);
			arrParams = queryStr.split("&");
			for (i = 0; i < arrParams.length; i++) {
				arrParam = arrParams[i].split("=");
				key = arrParam[0]; value = arrParam[1];
				form.appendChild(this._createInputElement("hidden", key, value));
			}
		}
	};

	/**
	 * 엘리먼트 배열 추가 함수
	 */
	this._appendChilds = function(t, els) {
		if (this._isNotNull(els)) {
			for (var i = 0; i < els.length; i++) {
				if (this._isNotNull(els[i])) {
					t.appendChild(els[i]);
				}
			}
		}
	};

	/**
	 * 엘리먼트 생성 함수
	 */
	this._createInputElement = function(t, n, v) {
		var el = null;
		if (this._isNotNull(v) == true) {
			el = document.createElement("input");
			el.type = t; el.name = n; el.value = v;
		}
		return el;
	};

	/**
	 * 엘리먼트 배열 생성 함수
	 */
	this._createInputElementArr = function(t, n, vs) {
		var elemArr = [];

		if (this._isNotNull(vs)) {
			try {
				for ( var i = 0; i < vs.length; i++) {
					elemArr[i] = this._createInputElement(t, n, vs[i]);
				}
			} catch (e) {
				throw e;
			}
		}

		return elemArr;
	};

	/**
	 * 화면 중앙에 팝업 띄우기
	 */
	this._openPopupToCenter = function(u, t, w, h, o) {
		var isIE = (navigator.userAgent.indexOf("MSIE") > -1);
		var isChrome = (navigator.userAgent.indexOf('Chrome') > -1);
		var left = (screen.width) ? (screen.width - w)/2 : 0;
		var top = (screen.height) ? (screen.height - h)/2 : 0;
		var opt = 'height=' + h + ',width=' + w + ',top=' + top + ',left=' + left +
					+ (o ? ',' + o : ',location=no,resize=yes,scrollbars=yes,resizable=yes');
		var win = null;

		try {
			win = window.open(u, t, opt);

			if (win & isChrome) {
				if (win.screenX == 'undefined' || win.screenX < 0) {
					alert("브라우저의 팝업표시가 가능하도록 설정을 변경해주세요.");
					return false;
			    };
			}
			else {
				if (!win) {
					if (!isIE) {
						alert("브라우저의 팝업표시가 가능하도록 설정을 변경해주세요.");
					}
					return false;
				}
			}
		}
		catch (e) {
			// do nothing
		}

		return false;
	};

	/**
	 * nEvent 로그 전송
	 */
	this._sendEventLog = function(type, event) {
		try {
			if (type === "click" && typeof this._htClickLog[event] != "undefined") {
				ectrack(this._htClickLog[event]["key"] + "." + this._htInitData["evkey"], this._htClickLog[event]["baseUrl"]);
			}
			else if (type === "finish" && typeof this._htFinishLog[event] != "undefined") {
				ectrack(this._htFinishLog[event]["key"] + "." + this._htInitData["evkey"], this._htFinishLog[event]["baseUrl"]);
			}
		}
		catch (e) {
			throw e;
		}
	};

	/**
	 * 로그인 여부 체크
	 *
	 * @param callback
	 */
	this._getLogined = function(callback) {
		spi.util.requestJsonp(this._htAPI['API000'], {}, callback, 'callback');
	};

	/**
	 * 단축 URL API 호출
	 *
	 * @param longUrl
	 * @param callback
	 */
	this._getShortUrl = function(longUrl, callback) {
		spi.util.requestJsonp(this._htAPI['API101'], {svcCode:"0022",url:longUrl}, callback);
	};

	/**
	 * 로그인 폼 오픈 처리
	 */
	this._openLoginForm = function(p, t, w, h) {
		this._openPopupToCenter('https://nid.naver.com/nidlogin.login?url=' + encodeURIComponent(p), t, w, h)
	};

	/**
	 * 로그인 페이지 Refirect
	 */
	this._redirectLoginPage = function(url) {
		document.location.href = "https://nid.naver.com/nidlogin.login?mode=form&url=" + encodeURIComponent(url);
	};

}

//소셜플러그인 Lazy Loading 호출
try {
	if (typeof draw_socialplugin == 'function') {
		draw_socialplugin();
	}
}
catch (e) {
	console.log(e);
}