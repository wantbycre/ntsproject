**NTS 마크업**
===

NHN NEXT
- entrance_120704 / 지원서 페이지
- lecture_120910 / 설명회 페이지
- p_121022_nextnetwork / 학생,교수 인트라넷 관리 페이지
- s_121015_nhnstory / NEXT  페이지
- s_121015_nhnstory_mobile / NEXT 모바일

NHN Deview
- src / 2013년도 DEVIEW프로모션

NHN Auto
- 120403_socialplugin / 수입차
- s_110310_new / 리뉴얼
- s_110512_dimm / 팝업
- s_110721_new / 리뉴얼
- s_110816_new / 리뉴얼
- s_110927_add / 신규 프로모션

NHN Keyword
- src / Main 홈
- 00_homemove / 메뉴1 홈
- 01_main / Left 인클루드
- 02_adguide / 메뉴2 섹션
- 03_adtally / 메뉴3 섹션
- 04_adedu / 메뉴4 섹션
- 05_infosquare / 메뉴5 섹션
- 06_customer / 메뉴6 섹션
- 07_business / 메뉴7 섹션
- 08_myinfo / 메뉴 8 섹션
- 09_error / 에러
- 10_notice / 공지사항


**다운로드**
---

https://gitlab.com/wantbycre/ntsproject/tree/master