/**
 * NEXT error UI. 
 */
var NextUI = {
	/**
	 * menu.
	 * 
	 * menu_min : 해상도에 따른 minimal & original over out event (minimal일 때 80, original일 때 170).
	 * menu_min_anim_in : menu minimal되는 애니메이션. 
	 * menu_min_in : menu minimal되는 css.
	 * menu_min_anim_out : menu original되는 애니메이션.
	 * menu_min_out : menu original되는 css.
	 */
	menu_min:function(){
		$('#header').hover(function(){
			NextUI.menu_min_out();
		}, function(){
			if($menu_hor_bool){
				if($menu_ver_bool){
					NextUI.menu_min_out();
				}else{
					NextUI.menu_min_in();
				}
			}else{
				NextUI.menu_min_out();
			}
		})
	},
	menu_min_anim_in:function(){
		$('#next_logo').stop(true).fadeOut(100);
		$('#util_menu').stop(true).fadeOut(100);
		$('#next_menu').stop(true).fadeOut(100);
		$('#header').stop(true).animate({'height':80}, 300);
		$('#gnb').stop(true).animate({'height':80}, 300);
		$('#header_container').stop(true).animate({'height':80}, {duration: 300,complete: function(){
			$('#next_logo_small').stop(true).fadeIn(100);
			$('.gnb_indicator').stop(true).fadeIn(100);
		}})
	},
	menu_min_in:function(){
		$('#next_logo').stop(true).hide()
		$('#util_menu').stop(true).hide()
		$('#next_menu').stop(true).hide()
		$('#header').stop(true).height(80);
		$('#gnb').stop(true).height(80);
		$('#header_container').stop(true).height(80);
		$('#next_logo_small').stop(true).show()
		$('.gnb_indicator').stop(true).show()
	},
	menu_min_anim_out:function(){
		$('#next_logo_small').stop(true).fadeOut(100);
		$('.gnb_indicator').stop(true).fadeOut(100);
		$('#header').stop(true).animate({'height':170}, 300);
		$('#gnb').stop(true).animate({'height':170}, 300);
		$('#header_container').stop(true).animate({'height':170}, {duration: 300,complete: function(){
			$('#next_logo').stop(true).fadeIn(100);
			$('#util_menu').stop(true).fadeIn(100);
			$('#next_menu').stop(true).fadeIn(100);
		}})
	},
	menu_min_out:function(){
		$('#next_logo_small').stop(true).hide();
		$('.gnb_indicator').stop(true).hide();
		$('#header').stop(true).height(170);
		$('#gnb').stop(true).height(170);
		$('#header_container').stop(true).height(170)
		$('#next_logo').stop(true).show()
		$('#util_menu').stop(true).show()
		$('#next_menu').stop(true).show()
	},
	/**
	 * gnb.
	 * 
	 * gnb_id : NEXT gnb element.
	 * gnb_line_w : 각각의 gnb .line의 width.
	 * gnb_hover : NEXT gnb over out event.
	 * gnb_click : NEXT gnb click event.
	 * gnb_over_anim : gnb over 애니메이션. h - gnb element
	 * gnb_out_anim : gnb out 애니메이션. h - gnb element
	 */
	gnb_id:$('#next_menu').find('[id^="gnb_"]'),
	gnb_line_w:{'about':68,'curriculum':44,'faculty':7,'admission':80,'campus':25,'post':7,'industrylink':14,'community':31},
	gnb_hover:function(){
		this.gnb_id.hover(function(){
			if(!$(this).hasClass('selected')){
				NextUI.gnb_over_anim(this, false)
				if($ag_id){
					window.location = $(this).attr('href');
				}
			}
		},function(){
			if (!$(this).hasClass('selected')){
				NextUI.gnb_out_anim(this)
			}
		});
	},
	gnb_click:function(){
		this.gnb_id.bind('click', function(e){
			e.preventDefault();
			window.location = $(this).attr('href'); 
		})
	},
	gnb_over_anim:function(h,b){
		var id = h.id.replace('gnb_','');
		var h = $(h);
		var w = this.gnb_line_w[id];
		var d = 50+h.find('.en').width()+h.find('.ko').width()*0.8;
		h.find('.en').css({'background-position':'-2px -2px'})
		h.find('.line').removeClass('line_active');
		h.find('.line').stop(true).animate({'width':w},{duration:d,complete:function(){
			h.find('.ko').css({'background-position':'-2px -16px'})
		}});
	},
	gnb_out_anim:function(h){
		var h = $(h);
		var w = 0;
		var d = 50+h.find('.en').width()+h.find('.ko').width()*0.8;
		h.find('.ko').css({'background-position':'-2px -2px'});
		h.find('.line').stop(true).animate({'width':w},{duration:d,complete:function(){
			h.find('.line').removeClass('line_active');
			h.find('.en').css({'background-position':'-2px -2px'});
		}});
	},
	search_element:$('#search_menu'),
	search_open:function(){
		this.search_element.bind('click', function(){
			$(this).animate({'left':0,'width':327}, 300)
			NextUI.search_text.animate({'width':308},{duration:300, complete:function(){
				NextUI.search_text.val("").addClass('select_input').focus()
			}})
		})
	},
	search_text:$('#search_text'),
	search_close:function(){
		this.search_text.blur(function(){
			$(this).animate({'width':80},{duration:300, complete:function(){
				NextUI.search_text.val("SEARCH").removeClass('select_input')
			}})
			NextUI.search_element.animate({'left':230,'width':97}, 300)
		})
	},
	/**
	 * error event.
	 */
	error_init:function(){
		this.menu_min();
	},
	/**
	 * common event.
	 */
	init:function(){
		if(browser.ipad||browser.iphone){
			$('body').addClass('mobile');
		}
		this.search_open();
		this.search_close();
		this.gnb_hover();
		this.gnb_click();
	}
}