/**
 * NEXT error resize. 
 */

/**
 * $m_w : 브라우저 해상도 최소 width.
 * $m_h : 브라우저 해상도 최소 height.
 * $w : 브라우저 width.
 * $h : 브라우저 height.
 * $menu_hor_bool : 브라우저 해상도 width값이 $m_w보다 작을 때 false, 클 때 true.
 * $menu_ver_bool : 브라우저 해상도 height값이 $m_h보다 작을 때 false, 클 때 true.
 */
var $m_w = 1020, $m_h = 600;
var $w, $h;
var $menu_hor_bool, $menu_ver_bool;

/**
 * resize width calc.
 * w : width.
 * e : element.
 */
var $r_w = {
	_default:function(w, e){
		return w;
	}
}

/**
 * resize overflow.
 * w : width.
 * e : element.
 */
var $r_o = {
	_default:function(w, e){
		return (w <= $m_w)? 'scroll' : 'hidden';
	}
}

/**
 * resize height calc.
 * h : height.
 * e : element.
 */
var $r_h = {
	_error:function(h, e){
		return (h>770)? h-235:535;
	}
}

/**
 * resize top(margin-top) calc.
 * h : height.
 * e : element.
 */
var $r_t = {
	_error:function(h, e){
		return ($('#error').height()>535)? Math.round((125*$('#error').height())/535):125;
	},
	_error_img:function(h, e){
		return Math.round($('#error').height()/2 - e.height()/2-180)
	}
}

/**
 * resize 객체.
 */
var res = {
	hor:[
		{element:$('html'),property:[{'width':{value:$r_w._default}},{'overflow-x':{value:$r_o._default}}]},
		{element:$('#header'),property:[{'width':{value:$r_w._default}}]}
	],
	ver:[
		{element:$('#error'),property:[{'height':{value:$r_h._error}}]},
		{element:$('#error_wrap'),property:[{'height':{value:$r_h._error}}]},
		{element:$('#error_container'),property:[{'height':{value:$r_h._error}}]},
		{element:$('#error_content'),property:[{'margin-top':{value:$r_t._error}}]},
		{element:$('#error_image img'),property:[{'margin-top':{value:$r_t._error_img}}]}
	]
}

/**
 * resize.
 */
var init_resize = function(){
	var w_width = $(window).width();
	var w_height = $(window).height();
	
	(w_width > $m_w) ? $w = w_width : $w = $m_w;
	(w_height > $m_h) ? $h = w_height : $h = $m_h;
	
	if(!$ag_id){
		if($w==$m_w){
			$menu_hor_bool = false;
			$('#error').css({'padding-bottom':0})
			$('#header').css({'position':'absolute'})
			$('#footer').css({'position':'relative','float':'left'})
		}else{
			$menu_hor_bool = true;
			$('#error').css({'padding-bottom':65})
			$('#header').css({'position':'fixed'})
			$('#footer').css({'position':'fixed'})
		}
	}else{
		$menu_hor_bool = false;
		$('#error').css({'padding-bottom':0})
		$('#header').css({'position':'absolute'})
		$('#footer').css({'position':'relative','float':'left'})
	}
	if($h==$m_h){
		$menu_ver_bool = false;
	}else{
		$menu_ver_bool = true;
	}
	
	//horizontal : width, left(margin-left).
	var hor_e, hor_p, hor_i, hor_j, hor_k;
	for(ver_i = 0; ver_i <res.ver.length; ver_i++){
		ver_e = res.ver[ver_i].element;
		for(ver_j = 0; ver_j <res.ver[ver_i].property.length; ver_j++){
			ver_p = res.ver[ver_i].property[ver_j];
			for(ver_k in ver_p){
				ver_e.css(ver_k, ver_p[ver_k].value($h, ver_e));
			}
		}
	}
	
	//vetical : height, top(margin-top).
	var ver_e, ver_p, ver_i, ver_j, ver_k;
	for(hor_i = 0; hor_i <res.hor.length; hor_i++){
		hor_e = res.hor[hor_i].element;
		for(hor_j = 0; hor_j <res.hor[hor_i].property.length; hor_j++){
			hor_p = res.hor[hor_i].property[hor_j];
			for(hor_k in hor_p){
				hor_e.css(hor_k, hor_p[hor_k].value($w, hor_e));
			}
		}
	}
	
	//minimal resize.
	if($menu_hor_bool){
		if(!$menu_ver_bool){
			NextUI.menu_min_in();
		}else{
			NextUI.menu_min_out();
		}
	}else{
		NextUI.menu_min_out();
	}
}

$(window).resize(function(){
	init_resize();
})