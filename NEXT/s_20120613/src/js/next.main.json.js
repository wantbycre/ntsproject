/**
 * NEXT main json. 
 */

$main_json_bool = false;

var Next = {
	banner_json:function(url){
		$.ajax({
			url:url,
			dataType:'json',
			success:function(next){
				Next.banner_max = next.banner.length;
				var banner = Next.banner(next.banner)
				var banner_paddle = Next.banner_paddle();
				$('#main').append(banner);
				$('.paddle_nav ul').append(banner_paddle);
				$('#main').find('img').load(Next.banner_img, function(){
					if (Next.banner_byte == Next.banner_img.length) {
						res.hor.push({element:$('.main_set'),property:[{'width':{value:$r_w._default}},{'height':{value:$r_h._main_set}},{'left':{value:$r_l._main_set}}]});
						res.ver.push({element:$('.main_link'),property:[{'margin-top':{value:$r_t._main_link}}]});
						init_resize();
						$main_json_bool = true;
						$('.loading').fadeOut(300);
						NextUI.init();
						NextUI.main_init();
						if($ag_id){
							$('body').animate({scrollTop:$('#wrap').position().top}, 150);
						}
					}
					Next.banner_byte++
				})
			}
		})
	},
	banner:function(banner){
		var h =	'';
		for(var i=0; i<banner.length;i++){
			this.banner_img.push(banner[i].src);
			h +='<section id="main'+(i+1)+'" class="main_set">'+
	    		'	<h1>'+ banner[i].h1 +'</h1>'+
	    		'	<img src="'+banner[i].src+'" alt="'+banner[i].alt+'" class="main_img"/>'+
				'	<div class="item_wrap">'+
				'		<div class="item_section">'+
				'			<div class="main_link">'+
				'				<div class="main_indicator">'+
				'					<img src="'+banner[i].indicator+'" alt="'+banner[i].indicator_alt+'"/>'+
				'				</div>'+
				'				<div class="main_title">'+
				'					<img src="'+banner[i].main_title+'" alt="'+banner[i].main_title_alt+'"/>'+
				'				</div>'+
				'				<div class="main_comment">'+
				'					<img src="'+banner[i].main_comment+'" alt="'+banner[i].main_comment_alt+'"/>'+
				'				</div>'+
				'				<div class="main_btn">'+
				'					<a href="'+banner[i].main_btn_link+'"></a>'+
				'				</div>'+
				'			</div>'+
				'		</div>'+
				'	</div>'+
	    		'</section>'
		}
		return h
	},
	banner_paddle:function(){
		var h = '';
		for(var i=0; i<Next.banner_max;i++){
			h+=	'<li>'+
				'	<a href="#" class="paddle'+(i+1)+'">'+
				'		<span>0'+(i+1)+'</span>'+
				'	</a>'+
				'	<span class="shadow_right"></span>'+
				'	<span class="shadow_bottom"></span>'+
				'	<span class="shadow_bottom_right"></span>'+
				'</li>'
		}
		return h;
	},
	banner_byte:1,
	banner_img:[],
	banner_max:0,
	update_json:function(url){
		$.ajax({
			url:url,
			dataType:'json',
			success:function(next){
				Next.update_max = next.update.length;
				var update = Next.update(next.update)
				$('#update').append(update)
				init_resize();
			}
		})
	},
	update:function(update){
		var h =	'';
		for(var i=0; i<update.length;i++){
			this.update_img.push(update[i].src);
			h +='<section>'+
				'	<h1>'+update[i].h1+'</h1>'+
				'		<a href="'+update[i].href+'">'+
				'		<div class="thumb">'+
							Next.update_color(i,update[i].number_color)+
				'			<img src="'+update[i].src+'" alt="'+update[i].alt+'"/>'+
				'		</div>'+
				'		<div class="column">'+
				'			<span class="title">'+update[i].title+'</span>'+
				'			<span class="desc">'+update[i].desc+'</span>'+
				'		</div>'+
				'	</a>'+
				'</section>'
		}
		return h
	},
	update_byte:1,
	update_img:[],
	update_max:0,
	update_color:function(i, color){
		if(color=='black'){
			return '<img src="img/update/num'+(i+1)+'_b.png" alt="0'+(i+1)+'" class="num"/>'
		}else if(color=='white'){
			return '<img src="img/update/num'+(i+1)+'_w.png" alt="0'+(i+1)+'" class="num"/>'
		}
	},
	sns_json:function(url){
		$.ajax({
			url:url,
			dataType:'json',
			success:function(next){
				Next.update_max = next.sns.length;
				var sns = Next.sns(next.sns)
				$('#header_content').find('.sns_say').append(sns)
				init_resize();
			}
		})
	},
	sns:function(sns){
		var h =	'';
		for(var i=0; i<sns.length;i++){
			this.sns_img.push(sns[i].src);
			h +='<li class="'+sns[i].type+'">'+
    			'	<div class="thumb">'+
    			'		<img src="'+sns[i].src+'" alt="'+sns[i].alt+'"/>'+
    			'	</div>'+
				'	<div class="say">'+
				'		<span class="user">'+sns[i].user+' says</span>'+
				'		<span class="comment">'+sns[i].comment+'</span>'+
				'	</div>'+
    			'</li>'
		}
		return h
	},
	sns_byte:1,
	sns_img:[],
	sns_max:0
}