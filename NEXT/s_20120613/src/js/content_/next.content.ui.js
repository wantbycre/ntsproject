/**
 * NEXT content UI. 
 */
var NextUI = {
	/**
	 * menu.
	 * 
	 * menu_min : 해상도에 따른 minimal & original over out event (minimal일 때 80, original일 때 170).
	 * intro_bool : .items 스크롤 되면서 나오는 minimal 애니메이션 boolean.
	 * menu_ani_bool : 애니메이션과 css의 중복 방지를 위한 boolean. 
	 * menu_min_anim_in : menu minimal되는 애니메이션. 
	 * menu_min_in : menu minimal되는 css.
	 * menu_min_anim_out : menu original되는 애니메이션.
	 * menu_min_out : menu original되는 css.
	 */
	menu_min:function(){
		$('#header').hover(function(){
			NextUI.menu_min_out();
		}, function(){
			if($menu_hor_bool){
				if($menu_ver_bool){
					if($p >= $('.items').offset().top - $('#module').height()/2-170){
						NextUI.menu_min_in();
					}else{
						NextUI.menu_min_out();
					}
				}else{
					NextUI.menu_min_in();
				}
			}else{
				NextUI.menu_min_out();
			}
		})
	},
	intro_bool:false,
	menu_ani_bool:false,
	menu_min_anim_in:function(){
		this.menu_ani_bool = true;
		$('#next_logo').fadeOut(100);
		$('#util_menu').fadeOut(100);
		$('#next_menu').fadeOut(100);
		$('#header').stop(true).animate({'height':80}, 300);
		$('#gnb').stop(true).animate({'height':80}, 300);
		$('#header_container').stop(true).animate({'height':80}, {duration: 300,complete: function(){
			$('#next_logo_small').fadeIn(100, function(){
				NextUI.intro_bool = true;
				NextUI.menu_ani_bool = false;
			});
			$('.gnb_indicator').fadeIn(100);
		}})
	},
	menu_min_in:function(){
		$('#next_logo').hide()
		$('#util_menu').hide()
		$('#next_menu').hide()
		$('#header').height(80);
		$('#gnb').height(80);
		$('#header_container').height(80);
		$('#next_logo_small').show()
		$('.gnb_indicator').show()
	},
	menu_min_anim_out:function(){
		$('#next_logo_small').fadeOut(100);
		$('.gnb_indicator').fadeOut(100);
		$('#header').stop(true).animate({'height':170}, 300);
		$('#gnb').stop(true).animate({'height':170}, 300);
		$('#header_container').stop(true).animate({'height':170}, {duration: 300,complete: function(){
			$('#next_logo').fadeIn(100);
			$('#util_menu').fadeIn(100);
			$('#next_menu').fadeIn(100);
			
		}})
	},
	menu_min_out:function(){
		$('#next_logo_small').hide();
		$('.gnb_indicator').hide();
		$('#header').height(170);
		$('#gnb').height(170);
		$('#header_container').height(170)
		$('#next_logo').show()
		$('#util_menu').show()
		$('#next_menu').show()
	},
	/**
	 * gnb.
	 * 
	 * gnb_id : NEXT gnb element.
	 * gnb_line_w : 각각의 gnb .line의 width.
	 * gnb_hover : NEXT gnb over out event.
	 * gnb_click : NEXT gnb click event.
	 * gnb_over_anim : gnb over 애니메이션. h - gnb element
	 * gnb_out_anim : gnb out 애니메이션. h - gnb element
	 */
	gnb_id:$('#next_menu').find('[id^="gnb_"]'),
	gnb_line_w:{'about':68,'curriculum':44,'faculty':7,'admission':80,'campus':25,'post':7,'industrylink':14,'community':31},
	gnb_hover:function(){
		this.gnb_id.hover(function(){
			if(!$(this).hasClass('selected')){
				NextUI.gnb_over_anim(this, false)
				if($ag_id){
					window.location = $(this).attr('href');
				}
			}
		},function(){
			if (!$(this).hasClass('selected')){
				NextUI.gnb_out_anim(this)
			}
		});
	},
	gnb_click:function(){
		this.gnb_id.bind('click', function(e){
			e.preventDefault();
			window.location = $(this).attr('href'); 
		})
	},
	gnb_over_anim:function(h,b){
		var id = h.id.replace('gnb_','');
		var h = $(h);
		var w = this.gnb_line_w[id];
		var d = 50+h.find('.en').width()+h.find('.ko').width()*0.8;
		h.find('.en').css({'background-position':'-2px -2px'})
		h.find('.line').removeClass('line_active');
		h.find('.line').stop(true).animate({'width':w},{duration:d,complete:function(){
			h.find('.ko').css({'background-position':'-2px -16px'})
		}});
	},
	gnb_out_anim:function(h){
		var h = $(h);
		var w = 0;
		var d = 50+h.find('.en').width()+h.find('.ko').width()*0.8;
		h.find('.ko').css({'background-position':'-2px -2px'});
		h.find('.line').stop(true).animate({'width':w},{duration:d,complete:function(){
			h.find('.line').removeClass('line_active');
			h.find('.en').css({'background-position':'-2px -2px'});
		}});
	},
	search_element:$('#search_menu'),
	search_open:function(){
		this.search_element.bind('click', function(){
			$(this).animate({'left':0,'width':327}, 300)
			NextUI.search_text.animate({'width':308},{duration:300, complete:function(){
				NextUI.search_text.val("").addClass('select_input').focus()
			}})
		})
	},
	search_text:$('#search_text'),
	search_close:function(){
		this.search_text.blur(function(){
			$(this).animate({'width':80},{duration:300, complete:function(){
				NextUI.search_text.val("SEARCH").removeClass('select_input')
			}})
			NextUI.search_element.animate({'left':230,'width':97}, 300)
		})
	},
	/**
	 * sns.
	 * 
	 * sns : sns element. (main일 때 header, module & content일 때 footer)
	 * sns_ex_id : sns의 이전 id.
	 * sns_id : sns의 현재 id.
	 * sns_bool : UI클릭 에러 방지 boolean, false일 때 애니메이션 실행, true일 때 UI 차단.
	 * sns_top : sns 애니메이션 top 값. (header일 때 60, footer일 때 40)
	 * sns_time : interval에 쓰일 변수.
	 * sns_nav : sns navigation의 <a> 태그. (main일 때 header, module & content일 때 footer)
	 * sns_nav_up : sns navigation의 .up. (main일 때 header, module & content일 때 footer)
	 * sns_nav_down : sns navigation의 .down. (main일 때 header, module & content일 때 footer)
	 * sns_nav_click : sns navigation click event.
	 * sns_nav_wheel : sns wheel event.
	 * sns_anim : sns 애니메이션 i - sns의 현재 id.
	 * sns_rolling : rolling event. trigger click을 이용한 rolling 구현.
	 * sns_interval : sns interval.
	 */
	sns:function(){
		return $('#footer').find('.sns_say');
	},
	sns_ex_id:0,
	sns_id:0,
	sns_bool:false,
	sns_top:40,
	sns_time:'',
	sns_nav:function(){
		return $('#footer').find('.sns_nav').find('a');
	},
	sns_nav_up:function(){
		return $('#footer').find('.sns_nav').find('.up');
	},
	sns_nav_down:function(){
		return $('#footer').find('.sns_nav').find('.down');
	},
	sns_nav_click:function(){
		var sns_nav = this.sns_nav();
		sns_nav.bind('click', function(e){
			e.preventDefault();
			clearInterval(NextUI.sns_time);
			if($(this).hasClass('up')){
				if(!NextUI.sns_bool){
					NextUI.sns_id--;
					if($snsMax != 1){
						NextUI.sns_anim(NextUI.sns_id);
					}
				}
			}else if($(this).hasClass('down')){
				if(!NextUI.sns_bool){
					NextUI.sns_id++;
					if($snsMax != 1){
						NextUI.sns_anim(NextUI.sns_id);
					}
				}
			}
		});
	},
	sns_nav_wheel:function(){
		var sns_up = NextUI.sns_nav_up();
		var sns_down = NextUI.sns_nav_down();
		$('.sns_say_wrap').mousewheel(function(e, d) {
			e.preventDefault();
			if(d > 0) {
				sns_up.trigger('click');
			}else if (d < 0){
				sns_down.trigger('click');
			}
		});
	},
	sns_anim:function(i){
		NextUI.sns_bool = true;
		var sns = this.sns();
		var t = this.sns_top;
		if(i >$snsMax-1){
			sns.find('li:nth-child(1)').css({'top':t*($snsMax)});
			sns.stop(true).animate({'top':-t*i}, {daration:300,complete:function(){
				NextUI.sns_id = 0;
				sns.css({'top':0});
				sns.find('li:first-child').css({'top':0});
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}else if(i<0){
			sns.find('li:last-child').css({'top':-t*1});
			sns.stop(true).animate({'top':t}, {daration:300,complete:function(){
				NextUI.sns_id = $snsMax-1;
				sns.css({'top':-t*NextUI.sns_id});
				sns.find('li:last-child').css({'top':t*NextUI.sns_id});
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}else{
			sns.stop(true).animate({'top':-t*i}, {duration:300, complete:function(){
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}
	},
	sns_rolling:function(){
		var sns = NextUI.sns_nav_down();
		sns.trigger('click');
	},
	sns_interval:function(){
		NextUI.sns_time = setInterval(function() {
			NextUI.sns_rolling();
		}, 5000);
	},
	/**
	 * sns share event.
	 */
	sns_share_btn:$('.do_share').find('.share'),
	sns_share_btn_hover:function(){
		this.sns_share_btn.hover(function(){
			$(this).addClass('select')
			$(this).find('.share_over').stop(true).fadeTo(200,1)
		}, function(){
			$(this).find('.share_over').stop(true).fadeTo(180,0)
			$(this).removeClass('select')
		});
	},
	/**
	 * top click event.
	 */
	top:function(){
		$('#top').bind('click', function(e){
			e.preventDefault();
			NextScroll.top();
		})
	},
	
	/**
	 * module.
	 * 
	 * module_in : 초기 module의 css, content에선 module의 애니메이션의 사용은 하지 않음.
	 * module_col :desc가 하단기준에 맞아야하기 때문에, 각각의 모듈마다 height 조건에 따르는 margin-top 값을 .col1, .col2, .col3, .col4로 적용. i - n번째 .desc.
	 */
	module_in: function(){
		$('.module_indicator').show()
		$('.module_menu').find('li').find('a').show();
		for (var i = 0; i < $('.module_menu').find('li').length; i++) {
			NextUI.module_col(i);
		}
	},
	module_col:function(i){
		if($('#module .desc:nth('+i+')').height()==17){
			//.desc가 1줄일때 
			$('#module .desc:nth('+i+')').addClass('col1')
		}else if($('#module .desc:nth('+i+')').height()==34){
			//.desc가 2줄일때
			$('#module .desc:nth('+i+')').addClass('col2')
		}else if($('#module .desc:nth('+i+')').height()==51){
			//.desc가 3줄일때
			$('#module .desc:nth('+i+')').addClass('col3')
		}else{
			//.desc가 4줄일때
			$('#module .desc:nth('+i+')').addClass('col4')
		}
	},
	tel:$('#policy').find('.tel_number'),
	tel_click:function(){
		this.tel.bind('click',function(){
			location.href = 'tel:+82-031-784-2990'
		})
	},
	/**
	 * module event 초기화.
	 */
	module_init:function(){
		if($snsMax != 1){
			this.sns_nav_click();
			this.sns_nav_wheel();
			clearInterval(this.sns_time);
			this.sns_interval();
		}
		if(!$ag_id){
			this.menu_min();
		}
	},
	/**
	 * content event 초기화.
	 */
	content_init:function(){
		this.top();
		NextScroll.content()
		this.sns_share_btn_hover();
		if($ag_id){
			this.tel.css({'cursor':'pointer'});
			this.tel_click();
		}
	},
	/**
	 * 공통 event 초기화.
	 */
	init:function(){
		if(browser.ipad||browser.iphone){
			$('body').addClass('mobile');
		}
		this.search_open();
		this.search_close();
		this.gnb_hover();
		this.gnb_click();
	}
}