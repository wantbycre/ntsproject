/**
 * NEXT contnet json. 
 */

var Next = {
	module_json:function(url){
		$.ajax({
			url:url,
			dataType:'json',
			success:function(next){
				Next.module_max = next[Next.module_id].module.length
				var module = Next.module(next[Next.module_id]);
				$('#module_container').append(module)
				$('#module_container').find('img').load(Next.module_img, function(){
					if (Next.module_img.length == 1) {
						Next.module_bool = true;
						$('#container').show();
						init_resize();
						$('.loading').hide();
						NextUI.module_init();
						NextUI.module_in();
						NextUI.content_init();
					}else if (Next.module_byte == Next.module_img.length) {
						Next.module_bool = true;
						init_resize();
						$('.loading').hide();
						NextUI.module_init();
						NextUI.module_in();
						NextUI.content_init();
					}else{
						$('#container').show();
					}
					Next.module_byte++
				});
			}
		})
	},
	module_bool:false,
	module:function(module){
		var h = '<div id="'+this.module_id+'_menu" class="module_menu">'+
				'	<div class="module_indicator">'+
				'		<img src="'+module.indicator.src+'" alt="'+module.indicator.alt+'"/>'+
				'	</div>'+
				'	<ul>'+this.module_btn(module.module)+'</ul>'+
				'</div>'
		return h;
	},
	module_btn:function(btn){
		var h =	'';
		for(var i=0; i<this.module_max;i++){
			this.module_img.push(btn[i].thumb);
			var br = btn[i].desc.split('<br/>').length;
			h +='<li>'+
				'	<a id="'+btn[i].id+'" href="'+btn[i].href+'">'+
				'		<div class="shadow">shadow</div>'+
				'		<div class="thumb">'+
				'			<img src="'+btn[i].thumb+'" alt="'+btn[i].alt+'"/>'+
				'		</div>'+
				'		<div class="info">'+
				'			<div class="title_wrap">'+
				'				<div class="title">'+btn[i].title+'</div>'+
				'			</div>'+
				'			<div class="desc">'+btn[i].desc+'</div>'+
				'		</div>'+
				'	</a>'+
				'</li>'
		}
		return h
	},
	module_id:'',
	module_byte:1,
	module_img:[],
	module_max:0,
	/*sns_json:function(url){
		$.ajax({
			url:url,
			dataType:'json',
			success:function(next){
				Next.update_max = next.sns.length;
				var sns = Next.sns(next.sns)
				$('#footer').find('.sns_say').append(sns)
				init_resize();
			}
		})
	},
	sns:function(sns){
		var h =	'';
		for(var i=0; i<sns.length;i++){
			this.sns_img.push(sns[i].src);
			h +='<li class="'+sns[i].type+'">'+
    			'	<div class="thumb">'+
    			'		<img src="'+sns[i].src+'" alt="'+sns[i].alt+'"/>'+
    			'	</div>'+
				'	<div class="say">'+
				'		<span class="user">'+sns[i].user+' says</span>'+
				'		<span class="comment">'+sns[i].comment+'</span>'+
				'	</div>'+
    			'</li>'
		}
		return h
	},
	sns_byte:1,
	sns_img:[],
	sns_max:0*/
}