/**
 * NEXT module resize. 
 */

var init_resize = function(){
	$('#main'+NextUI.main_id).css({'left':0});
	for(var i=0;i<6;i++){
		$('.sns_say').find('li:nth-child('+(i+1)+')').css({'top':40*i})
	}
}

$(window).resize(function(){
	init_resize();
})