/**
 * NEXT module UI. 
 */
var NextUI = {
	/**
	 * sns control,
	 */
	sns:function(){
		return $('#footer').find('.sns_say');
	},
	sns_ex_id:0,
	sns_id:0,
	sns_bool:false,
	sns_time:'',
	sns_nav:function(){
		return $('#footer').find('.sns_nav').find('a');
	},
	sns_nav_up:function(){
		return $('#footer').find('.sns_nav').find('.up');
	},
	sns_nav_down:function(){
		return $('#footer').find('.sns_nav').find('.down');
	},
	sns_nav_click:function(){
		var sns_nav = this.sns_nav();
		sns_nav.bind('click', function(e){
			e.preventDefault();
			clearInterval(NextUI.sns_time)
			if($(this).hasClass('up')){
				if(!NextUI.sns_bool){
					NextUI.sns_id--
					NextUI.sns_anim(NextUI.sns_id)
				}
			}else if($(this).hasClass('down')){
				if(!NextUI.sns_bool){
					NextUI.sns_id++
					NextUI.sns_anim(NextUI.sns_id)
				}
			}
		})
	},
	sns_nav_wheel:function(){
		var sns_up = NextUI.sns_nav_up();
		var sns_down = NextUI.sns_nav_down();
		$('.sns_say_wrap').mousewheel(function(e, d) {
			e.preventDefault();
			if(d > 0) {
				sns_up.trigger('click');
			}else if (d < 0){
				sns_down.trigger('click');
			}
		});
	},
	sns_anim:function(i){
		NextUI.sns_bool = true;
		var sns = this.sns();
		var t = 40;
		if(i >5){
			sns.find('li:nth-child(1)').css({'top':t*6})
			sns.stop(true).animate({'top':-t*i}, {daration:300,complete:function(){
				NextUI.sns_id = 0;
				sns.css({'top':0})
				sns.find('li:nth-child(1)').css({'top':0})
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}else if(i<0){
			sns.find('li:nth-child(6)').css({'top':-t*1})
			sns.stop(true).animate({'top':t}, {daration:300,complete:function(){
				NextUI.sns_id = 5;
				sns.css({'top':-t*5})
				sns.find('li:nth-child(6)').css({'top':t*5})
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}else{
			sns.stop(true).animate({'top':-t*i}, {duration:300, complete:function(){
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}})
		}
	},
	sns_rolling:function(){
		var sns = NextUI.sns_nav_down();
		sns.trigger('click');
	},
	sns_interval:function(){
		NextUI.sns_time = setInterval(function() {
			NextUI.sns_rolling();
		}, 5000);
	},
	tel:$('#policy').find('.tel_number'),
	tel_click:function(){
		this.tel.bind('click',function(){
			location.href = 'tel:+82-031-784-2990'
		})
	},
	/**
	 * footer event.
	 */
	footer_init:function(){
		this.sns_nav_click();
		this.sns_nav_wheel();
		clearInterval(this.sns_time);
		this.sns_interval();
		if($ag_id){
			this.tel.css({'cursor':'pointer'});
			this.tel_click();
		}
	}
}