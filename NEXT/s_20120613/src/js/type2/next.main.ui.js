/**
 * NEXT main UI. 
 */

var NextUI = {
	/**
	 * logo.
	 * 
	 * logo_id : NEXT logo element.
	 * logo_click : NEXT logo click event.
	 */
	logo_id:$('#next_logo').find('a'),
	logo_click:function(){
		this.logo_id.live('click', function(e){
			 window.location.reload();
		})
	},
	/**
	 * gnb.
	 * 
	 * gnb_id : NEXT gnb element.
	 * gnb_line_w : 각각의 gnb .line의 width.
	 * gnb_hover : NEXT gnb over out event.
	 * gnb_click : NEXT gnb click event.
	 * gnb_over_anim : gnb over 애니메이션. h - gnb element
	 * gnb_out_anim : gnb out 애니메이션. h - gnb element
	 */
	gnb_id:$('#next_menu').find('[id^="gnb_"]'),
	gnb_line_w:{'about':68,'curriculum':44,'faculty':7,'admission':80,'campus':25,'post':7,'industrylink':14,'community':31},
	gnb_hover:function(){
		this.gnb_id.hover(function(){
			if(!$(this).hasClass('selected')){
				NextUI.gnb_over_anim(this)
				if($ag_id){
					window.location = $(this).attr('href');
				}
			}
		},function(){
			if (!$(this).hasClass('selected')){
				NextUI.gnb_out_anim(this)
			}
		});
	},
	gnb_click:function(){
		this.gnb_id.bind('click', function(e){
			e.preventDefault();
			window.location = $(this).attr('href'); 
		})
	},
	gnb_over_anim:function(h,b){
		var id = h.id.replace('gnb_','');
		var h = $(h);
		var w = this.gnb_line_w[id];
		var d = 50+h.find('.en').width()+h.find('.ko').width()*0.8;
		h.find('.en').css({'background-position':'-2px -2px'})
		h.find('.line').removeClass('line_active');
		h.find('.line').stop(true).animate({'width':w},{duration:d,complete:function(){
			h.find('.ko').css({'background-position':'-2px -16px'})
		}});
	},
	gnb_out_anim:function(h){
		var h = $(h);
		var w = 0;
		var d = 50+h.find('.en').width()+h.find('.ko').width()*0.8;
		h.find('.ko').css({'background-position':'-2px -2px'});
		h.find('.line').stop(true).animate({'width':w},{duration:d,complete:function(){
			h.find('.line').removeClass('line_active');
			h.find('.en').css({'background-position':'-2px -2px'});
		}});
	},
	search_element:$('#search_menu'),
	search_open:function(){
		this.search_element.bind('click', function(){
			$(this).animate({'left':0,'width':327}, 300)
			NextUI.search_text.animate({'width':308},{duration:300, complete:function(){
				NextUI.search_text.val("").addClass('select_input').focus()
			}})
		})
	},
	search_text:$('#search_text'),
	search_close:function(){
		this.search_text.blur(function(){
			$(this).animate({'width':80},{duration:300, complete:function(){
				NextUI.search_text.val("SEARCH").removeClass('select_input')
			}})
			NextUI.search_element.animate({'left':230,'width':97}, 300)
		})
	},
	/**
	 * common event.
	 */
	init:function(){
		if(browser.ipad||browser.iphone){
			$('body').addClass('mobile');
		}
		this.search_open();
		this.search_close();
		this.gnb_hover();
		this.gnb_click();
	}
}