/**
 * community letter.
 */

var community = {
	list_element: $('.list_view').find('.info'),
	list_hover: function(){
		this.list_element.hover(function(){
			if(!$(this).hasClass('selected')){
				$(this).find('.thumb_container').find('.over').stop(true).fadeTo(400,1);
				$(this).find('.name').find('a').css({'font-weight':'bold','color':'#f7ae00'})
				$(this).find('.sort').find('span').css({'font-weight':'bold','color':'#f7ae00'})
			}
		},function(){
			if(!$(this).hasClass('selected')){
				$(this).find('.thumb_container').find('.over').stop(true).fadeTo(400,0);
				$(this).find('.name').find('a').css({'font-weight':'normal','color':'#666'})
				$(this).find('.sort').find('span').css({'font-weight':'normal','color':'#ccc'})
			}
		});
	},
	list_order: $('.list_order').find('a'),
	list_order_num:1,
	list_order_min:1,
	list_order_max:1,
	list_order_hover: function(){
		this.list_order.hover(function(){
			if(!$(this).hasClass('selected')){
				$(this).addClass('select')
			}
		},function(){
			if(!$(this).hasClass('selected')){
				$(this).removeClass('select')
			}
		});
	},
	list_order_click: function(){
		this.list_order.bind('click', function(e){
			e.preventDefault();
			community.list_order_num = $(this).html()
			community.list_order.removeClass('selected')
			community.list_order.removeClass('select')
			$(this).addClass('selected')
		});
	},
	list_paddle_ex_num:1,
	list_paddle_num:1,
	list_paddle_min:1,
	list_paddle_max:1,
	list_paddle: $('.list_paddle').find('a'),
	list_paddle_click: function(){
		this.list_paddle.bind('click', function(e){
			e.preventDefault();
			if($(this).parent().hasClass('prev')){
				community.list_paddle_num--
				if(community.list_paddle_num<community.list_paddle_min){
					community.list_paddle_num = community.list_paddle_min
				}
			}else if($(this).parent().hasClass('next')){
				community.list_paddle_num++
				if(community.list_paddle_num>community.list_paddle_max){
					community.list_paddle_num = community.list_paddle_max
				}
			}
			if(community.list_paddle_num == community.list_paddle_min){
				$('.list_paddle').find('.prev').find('a').css({'background-position':'0 -20px','cursor':'default'})
				$('.list_paddle').find('.next').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}else if(community.list_paddle_num == community.list_paddle_max){
				$('.list_paddle').find('.next').find('a').css({'background-position':'0 -20px','cursor':'default'})
				$('.list_paddle').find('.prev').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}else{
				$('.list_paddle').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}
			community.list_order_num = 8*community.list_paddle_num-7;
			community.list_order_move(community.list_paddle_num)
			community.list_paddle_ex_num = community.list_paddle_num
		});
	},
	list_order_move:function(i){
		var num = i-1
		if(community.list_paddle_num != community.list_paddle_ex_num){
			if(num<community.list_paddle_max){
				$('.list_order').css({'margin-left':-216*num-10})
			}
			$('.list_order').find('li:nth-child('+community.list_order_num+')').find('a').trigger('click')
		}
	},
	init: function(){
		/*this.list_order_max = $('.list_order').find('li').length
		$('.list_order').width(27*community.list_order_max);
		this.list_hover();
		this.list_order_hover();
		this.list_order_click();
		if(this.list_order_max <=8){
			$('.list_paddle').hide();
		}else{
			this.list_paddle_max = Math.ceil(this.list_order_max/8);
			this.list_paddle_click();
			$('.list_paddle').find('.prev').find('a').css({'background-position':'0 -20px','cursor':'default'})
		}*/
	}
}

$(function(){
	community.init();
})