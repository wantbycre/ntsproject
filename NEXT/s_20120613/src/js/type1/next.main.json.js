/**
 * NEXT main json. 
 */

$main_json_bool = false;

var Next = {
	sns_json:function(url){
		$.ajax({
			url:url,
			dataType:'json',
			success:function(next){
				Next.update_max = next.sns.length;
				var sns = Next.sns(next.sns)
				$('#header_content').find('.sns_say').append(sns)
				init_resize();
				NextUI.init();
				NextUI.main_init();
			}
		})
	},
	sns:function(sns){
		var h =	'';
		for(var i=0; i<sns.length;i++){
			this.sns_img.push(sns[i].src);
			h +='<li class="'+sns[i].type+'">'+
    			'	<div class="thumb">'+
    			'		<img src="'+sns[i].src+'" alt="'+sns[i].alt+'"/>'+
    			'	</div>'+
				'	<div class="say">'+
				'		<span class="user">'+sns[i].user+' says</span>'+
				'		<span class="comment">'+sns[i].comment+'</span>'+
				'	</div>'+
    			'</li>'
		}
		return h
	},
	sns_byte:1,
	sns_img:[],
	sns_max:0
}