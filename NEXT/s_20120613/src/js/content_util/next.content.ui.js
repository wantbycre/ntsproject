/**
 * NEXT content UI. 
 */
var NextUI = {
	sns_share_btn:$('.do_share').find('.share'),
	sns_share_btn_hover:function(){
		this.sns_share_btn.hover(function(){
			$(this).addClass('select')
			$(this).find('.share_over').stop(true).fadeTo(200,1)
		}, function(){
			$(this).find('.share_over').stop(true).fadeTo(180,0)
			$(this).removeClass('select')
		});
	},
	/**
	 * content event.
	 */
	content_init:function(){
		this.sns_share_btn_hover();
	}
}