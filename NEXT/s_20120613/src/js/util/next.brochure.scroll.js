/**
 * Next brochure scroll.
 */
var NextScroll = {
	p:function(){
		return (browser.safari)? $('body') : $('html, body');
	}
}