/**
 * NEXT sitemap UI. 
 */
var NextUI = {
	/**
	 * menu.
	 * 
	 * menu_min : 해상도에 따른 minimal & original over out event (minimal일 때 80, original일 때 170).
	 * menu_min_anim_in : menu minimal되는 애니메이션. 
	 * menu_min_in : menu minimal되는 css.
	 * menu_min_anim_out : menu original되는 애니메이션.
	 * menu_min_out : menu original되는 css.
	 */
	menu_min:function(){
		$('#header').hover(function(){
			NextUI.menu_min_out();
		}, function(){
			if($menu_hor_bool){
				if($menu_ver_bool){
					NextUI.menu_min_out();
				}else{
					NextUI.menu_min_in();
				}
			}else{
				NextUI.menu_min_out();
			}
		})
	},
	menu_min_anim_in:function(){
		$('#next_logo').stop(true).fadeOut(100);
		$('#util_menu').stop(true).fadeOut(100);
		$('#next_menu').stop(true).fadeOut(100);
		$('#header').stop(true).animate({'height':80}, 300);
		$('#gnb').stop(true).animate({'height':80}, 300);
		$('#header_container').stop(true).animate({'height':80}, {duration: 300,complete: function(){
			$('#next_logo_small').stop(true).fadeIn(100);
			$('.gnb_indicator').stop(true).fadeIn(100);
			$in_bool = true;
		}})
	},
	menu_min_in:function(){
		$('#next_logo').stop(true).hide()
		$('#util_menu').stop(true).hide()
		$('#next_menu').stop(true).hide()
		$('#header').stop(true).height(80);
		$('#gnb').stop(true).height(80);
		$('#header_container').stop(true).height(80);
		$('#next_logo_small').stop(true).show()
		$('.gnb_indicator').stop(true).show()
	},
	menu_min_anim_out:function(){
		$('#next_logo_small').stop(true).fadeOut(100);
		$('.gnb_indicator').stop(true).fadeOut(100);
		$('#header').stop(true).animate({'height':170}, 300);
		$('#gnb').stop(true).animate({'height':170}, 300);
		$('#header_container').stop(true).animate({'height':170}, {duration: 300,complete: function(){
			$('#next_logo').stop(true).fadeIn(100);
			$('#util_menu').stop(true).fadeIn(100);
			$('#next_menu').stop(true).fadeIn(100);
		}})
	},
	menu_min_out:function(){
		$('#next_logo_small').stop(true).hide();
		$('.gnb_indicator').stop(true).hide();
		$('#header').stop(true).height(170);
		$('#gnb').stop(true).height(170);
		$('#header_container').stop(true).height(170)
		$('#next_logo').stop(true).show()
		$('#util_menu').stop(true).show()
		$('#next_menu').stop(true).show()
	},
	/**
	 * gnb.
	 * 
	 * gnb_id : NEXT gnb element.
	 * gnb_line_w : 각각의 gnb .line의 width.
	 * gnb_hover : NEXT gnb over out event.
	 * gnb_click : NEXT gnb click event.
	 * gnb_over_anim : gnb over 애니메이션. h - gnb element
	 * gnb_out_anim : gnb out 애니메이션. h - gnb element
	 */
	gnb_id:$('#next_menu').find('[id^="gnb_"]'),
	gnb_line_w:{'about':68,'curriculum':44,'faculty':7,'admission':80,'campus':25,'post':7,'industrylink':14,'community':31},
	gnb_hover:function(){
		this.gnb_id.hover(function(){
			if(!$(this).hasClass('selected')){
				NextUI.gnb_over_anim(this, false)
				if($ag_id){
					window.location = $(this).attr('href');
				}
			}
		},function(){
			if (!$(this).hasClass('selected')){
				NextUI.gnb_out_anim(this)
			}
		});
	},
	gnb_click:function(){
		this.gnb_id.bind('click', function(e){
			e.preventDefault();
			window.location = $(this).attr('href'); 
		})
	},
	gnb_over_anim:function(h,b){
		var id = h.id.replace('gnb_','');
		var h = $(h);
		var w = this.gnb_line_w[id];
		var d = 50+h.find('.en').width()+h.find('.ko').width()*0.8;
		h.find('.en').css({'background-position':'-2px -2px'})
		h.find('.line').removeClass('line_active');
		h.find('.line').stop(true).animate({'width':w},{duration:d,complete:function(){
			h.find('.ko').css({'background-position':'-2px -16px'})
		}});
	},
	gnb_out_anim:function(h){
		var h = $(h);
		var w = 0;
		var d = 50+h.find('.en').width()+h.find('.ko').width()*0.8;
		h.find('.ko').css({'background-position':'-2px -2px'});
		h.find('.line').stop(true).animate({'width':w},{duration:d,complete:function(){
			h.find('.line').removeClass('line_active');
			h.find('.en').css({'background-position':'-2px -2px'});
		}});
	},
	search_element:$('#search_menu'),
	search_open:function(){
		this.search_element.bind('click', function(){
			$(this).animate({'left':0,'width':327}, 300)
			NextUI.search_text.animate({'width':308},{duration:300, complete:function(){
				NextUI.search_text.val("").addClass('select_input').focus()
			}})
		})
	},
	search_text:$('#search_text'),
	search_close:function(){
		this.search_text.blur(function(){
			$(this).animate({'width':80},{duration:300, complete:function(){
				NextUI.search_text.val("SEARCH").removeClass('select_input')
			}})
			NextUI.search_element.animate({'left':230,'width':97}, 300)
		})
	},
	/**
	 * sns control,
	 */
	sns:function(){
		return $('#footer').find('.sns_say');
	},
	sns_ex_id:0,
	sns_id:0,
	sns_bool:false,
	sns_time:'',
	sns_nav:function(){
		return $('#footer').find('.sns_nav').find('a');
	},
	sns_nav_up:function(){
		return $('#footer').find('.sns_nav').find('.up');
	},
	sns_nav_down:function(){
		return $('#footer').find('.sns_nav').find('.down');
	},
	sns_nav_click:function(){
		var sns_nav = this.sns_nav();
		sns_nav.bind('click', function(e){
			e.preventDefault();
			clearInterval(NextUI.sns_time)
			if($(this).hasClass('up')){
				if(!NextUI.sns_bool){
					NextUI.sns_id--
					NextUI.sns_anim(NextUI.sns_id)
				}
			}else if($(this).hasClass('down')){
				if(!NextUI.sns_bool){
					NextUI.sns_id++
					NextUI.sns_anim(NextUI.sns_id)
				}
			}
		})
	},
	sns_nav_wheel:function(){
		var sns_up = NextUI.sns_nav_up();
		var sns_down = NextUI.sns_nav_down();
		$('.sns_say_wrap').mousewheel(function(e, d) {
			e.preventDefault();
			if(d > 0) {
				sns_up.trigger('click');
			}else if (d < 0){
				sns_down.trigger('click');
			}
		});
	},
	sns_anim:function(i){
		NextUI.sns_bool = true;
		var sns = this.sns();
		var t = 40;
		if(i >5){
			sns.find('li:nth-child(1)').css({'top':t*6})
			sns.stop(true).animate({'top':-t*i}, {daration:300,complete:function(){
				NextUI.sns_id = 0;
				sns.css({'top':0})
				sns.find('li:nth-child(1)').css({'top':0})
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}else if(i<0){
			sns.find('li:nth-child(6)').css({'top':-t*1})
			sns.stop(true).animate({'top':t}, {daration:300,complete:function(){
				NextUI.sns_id = 5;
				sns.css({'top':-t*5})
				sns.find('li:nth-child(6)').css({'top':t*5})
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}else{
			sns.stop(true).animate({'top':-t*i}, {duration:300, complete:function(){
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}
	},
	sns_rolling:function(){
		var sns = NextUI.sns_nav_down();
		sns.trigger('click');
	},
	sns_interval:function(){
		NextUI.sns_time = setInterval(function() {
			NextUI.sns_rolling();
		}, 5000);
	},
	/**
	 * sitemap event 초기화.
	 */
	sitemap_init:function(){
		this.sns_nav_click();
		this.sns_nav_wheel();
		clearInterval(this.sns_time);
		this.sns_interval();
		if(!$ag_id){
			this.menu_min();
		}
	},
	/**
	 * 공통 event 초기화.
	 */
	init:function(){
		this.search_open();
		this.search_close();
		this.gnb_hover();
		this.gnb_click();
	}
}