/**
 * NEXT sitemap resize. 
 */

var $r_w = {
	_default:function(w, e){
		return w;
	}
}

var $r_o = {
	_default:function(w, e){
		return (w <= $m_w)? 'scroll' : 'hidden';
	}
}

var $r_h = {
	_sitemap:function(h, e){
		return (h>685)? h-235:450;
	}
}

var $r_t = {
	_sitemap:function(h, e){
		return Math.round($('#sitemap_wrap').height()/2-(e.height()-77)/2);
	}
}

var res = {
	hor:[
		{element:$('html'),property:[{'width':{value:$r_w._default}},{'overflow-x':{value:$r_o._default}}]},
		{element:$('#header'),property:[{'width':{value:$r_w._default}}]}
	],
	ver:[
		{element:$('#sitemap'),property:[{'height':{value:$r_h._sitemap}}]},
		{element:$('#sitemap_wrap'),property:[{'height':{value:$r_h._sitemap}}]},
		{element:$('#sitemap_column'),property:[{'margin-top':{value:$r_t._sitemap}}]}
	]
}

var $w, $h;
var $menu_hor_bool, $menu_ver_bool;

var init_resize = function(){
	clearInterval(NextUI.sns_time)
	var w_width = $(window).width();
	var w_height = $(window).height();
	
	(w_width > $m_w) ? $w = w_width : $w = $m_w;
	(w_height > $m_h) ? $h = w_height : $h = $m_h;
	
	if(!$ag_id){
		if($w==$m_w){
			$menu_hor_bool = false;
			$('#header').css({'position':'absolute'})
		}else{
			$menu_hor_bool = true;
			$('#header').css({'position':'fixed'})
		}
	}else{
		$menu_hor_bool = false;
		$('#header').css({'position':'absolute'})
	}
	if($h==$m_h){
		$menu_ver_bool = false;
	}else{
		$menu_ver_bool = true;
	}
	
	var hor_e, hor_p, hor_i, hor_j, hor_k;
	for(ver_i = 0; ver_i <res.ver.length; ver_i++){
		ver_e = res.ver[ver_i].element;
		for(ver_j = 0; ver_j <res.ver[ver_i].property.length; ver_j++){
			ver_p = res.ver[ver_i].property[ver_j];
			for(ver_k in ver_p){
				ver_e.css(ver_k, ver_p[ver_k].value($h, ver_e));
			}
		}
	}
	
	var ver_e, ver_p, ver_i, ver_j, ver_k;
	for(hor_i = 0; hor_i <res.hor.length; hor_i++){
		hor_e = res.hor[hor_i].element;
		for(hor_j = 0; hor_j <res.hor[hor_i].property.length; hor_j++){
			hor_p = res.hor[hor_i].property[hor_j];
			for(hor_k in hor_p){
				hor_e.css(hor_k, hor_p[hor_k].value($w, hor_e));
			}
		}
	}
	
	for(var i=0;i<6;i++){
		$('.sns_say').find('li:nth-child('+(i+1)+')').css({'top':40*i})
	}
	
	if($menu_hor_bool){
		if(!$menu_ver_bool){
			NextUI.menu_min_in();
		}else{
			NextUI.menu_min_out();
		}
	}else{
		NextUI.menu_min_out();
	}
}

$(window).resize(function(){
	init_resize();
})