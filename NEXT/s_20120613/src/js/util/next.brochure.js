/**
 * $m_w : window min width value.
 * $m_h : window min height value.
 */
var $m_w = 1020, $m_h = 600;

$(document).ready(function(){
	init_resize();
	NextUI.init();
	NextUI.brochure_init();
	Next.sns_json('json/sns.json')
})