/**
 * NEXT main resize. 
 */

//resize width
var $r_w = {
	_default:function(w, e){
		return w;
	},
	_main:function(w, e) {
		return w * $bigbannerMax;
	}
}


//resize left(margin-left)

var $r_l = {
	_zero:function(w, e){
		return 0;
	},
	_main_set:function(w,e){
		return w
	},
	_default:function(w, e){
		return Math.round(w/2 - e.width()/2);
	},
	_logo:function(w, e){
		return Math.round(w/2-480)-2;
	},
	_paddle:function(w, e){
		return Math.round(w/2-510+35);
	},
	_update:function(w, e){
		return Math.round(w/2+53);
	}
}
//resize height.
var $r_h = {
	_default:function(h, e){
		return h;
	},
	_main_container:function(h, e){
		return h-$b_h;
	},
	_main_set:function(h, e){
		return $('#wrap').height()-$b_h;
	},
	_update_content:function(h, e){
		if($bannerMax == 1){
			return 90;
		}else if($bannerMax == 2){
			if(h == 600){
				return 90;
			}else if(h>600){
				return 180;
			}
		}else{
			if(h == 600){
				return 90;
			}else if(h>600 && h<700){
				return 180;
			}else{
				return 270;
			}
		}
	},
	_update:function(h,e){
		if($bannerMax <= 3){
			return $bannerMax*90;
		}else{
			return 270;
		}
	}
}

//resize top(margin-top)
var $r_t = {
	_zero:function(h, e){
		return 0;
	},
	_default:function(h, e){
		return Math.round(h/2 - e.height()/2);
	},
	_main_container:function(h, e){
		return 0;
	},
	_main_link:function(h, e){
		if(h<700){
			return (h-170)/2-e.height()/2+25;
		}else{
			return 180;
		}
	},
	_header_container:function(h, e){
		return h-$b_h;
	},
	_update:function(h, e){
		if($bannerMax == 1){
			NextUI.update_max = $bannerMax-1;
		}else if($bannerMax == 2){
			if($h == 600){
				//배너 1개 보임
				NextUI.update_max = $bannerMax-1;
			}else if($h>600){
				//배너 2개 보임
				NextUI.update_max = $bannerMax-2;
			}
		}else{
			if($h == 600){
				//배너 1개 보임
				NextUI.update_max = $bannerMax-1;
			}else if($h>600 && $h<700){
				//배너 2개 보임
				NextUI.update_max = $bannerMax-2;
			}else{
				//배너 3개 보임
				NextUI.update_max = $bannerMax-3;
			}
		}
		
		if(NextUI.update_id<0){
			NextUI.update_id = 0;
		}else if(NextUI.update_id>NextUI.update_max){
			NextUI.update_id = NextUI.update_max;
		}else{
			(NextUI.update_id == 0)? NextUI.update_nav_up.css({'background-position':'0 -19px'}):NextUI.update_nav_up.css({'background-position':'0 0'}); 
			(NextUI.update_id == NextUI.update_max)? NextUI.update_nav_down.css({'background-position':'0 -19px'}):NextUI.update_nav_down.css({'background-position':'0 0'});
		}
		return -90*NextUI.update_id
	}
}

//resize display
var $r_d = {
	_update:function(h, e){
		if($bannerMax == 1){
			return 'none';
		}else if($bannerMax == 2){
			if(h == 600){
				return 'block';
			}else if(h>600){
				return 'none';
			}
		}else if($bannerMax == 3){
			if(h<700){
				return 'block';
			}else{
				return 'none';
			}
		}
	}
}

//resize 객체.
var res = {
	hor:[
		{element:$('#wrap'),property:[{'width':{value:$r_w._default}}]},
		{element:$('#header'),property:[{'width':{value:$r_w._default}}]},
		{element:$('#main'),property:[{'width':{value:$r_w._main}}]},
		{element:$('.paddle_content'),property:[{'left':{value:$r_l._paddle}}]},
		{element:$('#update_content'),property:[{'left':{value:$r_l._update}}]}
	],
	ver:[
		{element:$('#wrap'),property:[{'height':{value:$r_h._default}}]},
		{element:$('#header'),property:[{'height':{value:$r_h._default}}]},
		{element:$('#main_container'),property:[{'height':{value:$r_h._main_container}}]},
		{element:$('#main_view'),property:[{'height':{value:$r_h._main_container}}]},
		{element:$('#main'),property:[{'height':{value:$r_h._main_container}}]},
		{element:$('#main_column'),property:[{'height':{value:$r_h._main_container}}]},
		{element:$('.paddle_container'),property:[{'height':{value:$r_h._main_container}}]},
		{element:$('#update_container'),property:[{'height':{value:$r_h._main_container}}]},
		{element:$('#lnb_container'),property:[{'height':{value:$r_h._main_container}}]},
		{element:$('#header_container'),property:[{'top':{value:$r_t._header_container}}]},
		{element:$('#update_wrap'),property:[{'height':{value:$r_h._update_content}}]},
		{element:$('#update_content'),property:[{'height':{value:$r_h._update_content}}]},
		{element:$('#update'),property:[{'height':{value:$r_h._update},'top':{value:$r_t._update}}]},
		{element:$('#update_nav'),property:[{'display':{value:$r_d._update}}]}
	]
}

var $w, $h;
var $resize_bool=false;

var init_resize = function(){
	clearInterval(NextUI.main_time);
	var w_width = $(window).width();
	var w_height = $(window).height();
	
	(w_width > $m_w) ? $w = w_width : $w = $m_w;
	(w_height > $m_h) ? $h = w_height : $h = $m_h;
	
	var hor_e, hor_p, hor_i, hor_j, hor_k;
	for(ver_i = 0; ver_i <res.ver.length; ver_i++){
		ver_e = res.ver[ver_i].element;
		for(ver_j = 0; ver_j <res.ver[ver_i].property.length; ver_j++){
			ver_p = res.ver[ver_i].property[ver_j];
			for(ver_k in ver_p){
				ver_e.css(ver_k, ver_p[ver_k].value($h, ver_e));
			}
		}
	}
	
	var ver_e, ver_p, ver_i, ver_j, ver_k;
	for(hor_i = 0; hor_i <res.hor.length; hor_i++){
		hor_e = res.hor[hor_i].element;
		for(hor_j = 0; hor_j <res.hor[hor_i].property.length; hor_j++){
			hor_p = res.hor[hor_i].property[hor_j];
			for(hor_k in hor_p){
				hor_e.css(hor_k, hor_p[hor_k].value($w, hor_e));
			}
		}
	}
	
	$('.main_img').width($w).height(Math.round($i_oh*$w/$i_ow))
	$('.main_img').css({'top':Math.round(($h-$b_h)/2-$('.main_img').height()/2),'left':Math.round($w/2-$('.main_img').width()/2)})
	if($h-170 > $('.main_img').height()){
		$('.main_img').height($h-170).width(Math.round($i_ow*($h-170)/$i_oh));
		$('.main_img').css({'top':Math.round(($h-$b_h)/2-$('.main_img').height()/2),'left':Math.round($w/2-$('.main_img').width()/2)})
	}
	
	$('#main'+NextUI.main_ex_id).css({'left':0});
	$('#main'+NextUI.main_id).css({'left':0});
	NextUI.main_bool = false;
	for(var i=0;i<6;i++){
		$('.sns_say').find('li:nth-child('+(i+1)+')').css({'top':60*i})
	}
	NextUI.main_interval();
	$resize_bool = false;
}

$(window).resize(function() {
	$resize_bool = true;
	init_resize();
});