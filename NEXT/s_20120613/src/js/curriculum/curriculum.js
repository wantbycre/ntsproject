/**
 * admission pick.
 */
var curruculum = {
    element: $('.interview_btn'),
    toggle: function(){
        this.element.toggle(function(){
			$(this).find('.btn_open').hide();
			$(this).find('.btn_default').show();
			$('.interview_column').removeClass('default')
			$('.deem').fadeOut();
			curruculum.interview_scroll()
		},function(){
			$(this).find('.btn_default').hide();
			$(this).find('.btn_open').show();
			$('.interview_column').addClass('default')
			$('.deem').fadeIn();
			curruculum.interview_scroll()
		})
    },
	interview_scroll:function(){
		var p = NextScroll.p();
		p.animate({scrollTop : $('.interview_info_item').offset().top-$h_h}, {duration:500});
	},
    init: function(){
        this.toggle();
    }
}

$(function(){
	curruculum.init()
})