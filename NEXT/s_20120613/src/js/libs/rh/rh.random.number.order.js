/**
 * ramdom number order
 */
var Random = {
	temp:[],
	result:[],
	random_int:function(s,e){
		return Math.round(s-0.5+(e-s+1)*Math.random());
	},
	random_ints:function(s,e){
		var i, r;
		for(i = s; i<e+1; i++){
			this.temp.push(i);
		}
		for(i = 0; i<e; i++){
			r = this.random_int(0, this.temp.length-1);
			this.result.push(this.temp[r]);
			this.temp.splice(r,1);
		}
	}
}