//TODO : 인터셉터에서 브라우저 버전을 체크하도록 수정 필요
var $ap = navigator.appVersion;
var $ua = navigator.userAgent.toLowerCase();
var browser = {
	ie : $ap.indexOf('MSIE') != -1,
	ie6 : $ap.indexOf('MSIE 6') != -1,
	ie7 : $ap.indexOf('MSIE 7') != -1,
	ie8 : $ap.indexOf('MSIE 8') != -1,
	opera : !!window.opera,
	safari : $ua.indexOf('safari') != -1,
	safari3 : $ua.indexOf('applewebkit/5') != -1,
	mac : $ua.indexOf('mac') != -1,
	chrome : $ua.match(/chrome/),
	firefox : $ua.match(/firefox/),
	mobile: $ua.match(/mobile/),
	ipod : $ua.match(/ipod/),
	ipad : $ua.match(/ipad/), 
	iphone : $ua.match(/iphone/),
	galaxytab : $ua.match(/shw-m180/),
	galaxytab_101 : $ua.match(/shw-m380/),
	android : $ua.match(/android/)
};

var $ag_id = (browser.ipod||browser.ipad||browser.iphone||browser.android);
var $ios = (browser.ipod||browser.ipad||browser.iphone);
var $android = (browser.android);
var $pc = (browser.ie||browser.opera||browser.safari||browser.chrome||browser.firefox);
var $hls = (browser.ipod||browser.ipad||browser.iphone||$ua.match(/android [4-9]/));
var $rtsp = ($ua.match(/android [0-3]/));
var $ie7 = (browser.ie7);
