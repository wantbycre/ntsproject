/**
 * admission pick.
 */
var brochure = {
	popup_area:$('#post_popup_wrap'),
	popup_alert:$('.popup_alert'),
	popup:function(){
		this.popup_area.show();
		var popup_text = this.popup_text('post_number');
		this.popup_alert.html(popup_text);
	},
	popup_text:function(check){
		if(check == 'sort'){
			return '구분을 선택해주세요'
		}else if(check == 'name'){
			return '이름을 입력해주세요'
		}else if(check == 'post_number'){
			return '우편번호를 입력해주세요'
		}else if(check == 'address'){
			return '주소를 입력해주세요'
		}else if(check == 'tel'){
			return '전화번호를 입력해주세요'
		}else if(check == 'mobile'){
			return '휴대전화를 입력해주세요'
		}else if(check == 'email'){
			return '이메일을 입력해주세요'
		}
	},
	popup_btn:$('#confirm_btn'),
	popup_btn_click:function(){
		this.popup_btn.bind('click', function(e){
			e.preventDefault();
			brochure.popup_area.hide();
		})
	},
	app_confirm_btn:$('#application'),
	app_confirm_btn_click:function(){
		this.app_confirm_btn.bind('click', function(e){
			e.preventDefault();
			brochure.popup();
		})
	},
	app_cancel_btn:$('#cancel'),
	app_cancel_btn_click:function(){
		this.app_cancel_btn.bind('click', function(e){
			e.preventDefault();
			$('#post').slideUp(400);
		})
	},
    post_btn: $('.btn').find('.post'),
	close_btn: $('#close'),
    open:function(){
        this.post_btn.bind('click', function(){
			$('#post').slideDown(400)
			brochure.brochure_open_scroll();
		})
    },
	close:function(){
		this.close_btn.bind('click', function(){
			$('#post').slideUp(400);
		})
	},
	brochure_open_scroll:function(){
		var p = NextScroll.p();
		p.delay(350).animate({scrollTop : $('#post').offset().top}, 800);
	},
    init: function(){
        this.open();
		this.close();
		this.app_confirm_btn_click();
		this.app_cancel_btn_click();
		this.popup_btn_click();
    }
}

$(function(){
	brochure.init();
})