/**
 * NEXT main UI. 
 */

var NextUI = {
	/**
	 * logo.
	 * 
	 * logo_id : NEXT logo element.
	 * logo_click : NEXT logo click event.
	 */
	logo_id:$('#next_logo').find('a'),
	logo_click:function(){
		this.logo_id.live('click', function(e){
			 window.location.reload();
		});
	},
	/**
	 * gnb.
	 * 
	 * gnb_id : NEXT gnb element.
	 * gnb_line_w : 각각의 gnb .line의 width.
	 * gnb_hover : NEXT gnb over out event.
	 * gnb_click : NEXT gnb click event.
	 * gnb_over_anim : gnb over 애니메이션. h - gnb element
	 * gnb_out_anim : gnb out 애니메이션. h - gnb element
	 */
	gnb_id:$('#next_menu').find('[id^="gnb_"]'),
	gnb_line_w:{'about':68,'curriculum':44,'faculty':29,'admission':80,'campus':25,'post':9,'industrylink':14,'community':31},
	gnb_hover:function(){
		this.gnb_id.hover(function(){
			if(!$(this).hasClass('selected')){
				NextUI.gnb_over_anim(this);
				if($ag_id){
					window.location = $(this).attr('href');
				}
			}
		},function(){
			if (!$(this).hasClass('selected')){
				NextUI.gnb_out_anim(this);
			}
		});
	},
	gnb_click:function(){
		this.gnb_id.bind('click', function(e){
			e.preventDefault();
			window.location = $(this).attr('href'); 
		});
	},
	gnb_over_anim:function(h,b){
		var id = h.id.replace('gnb_','');
		var h = $(h);
		var w = this.gnb_line_w[id];
		var d = 50+h.find('.en').width()+h.find('.ko').width()*0.8;
		h.find('.en').css({'background-position':'-2px -2px'})
		h.find('.line').removeClass('line_active');
		h.find('.line').stop(true).animate({'width':w},{duration:d,complete:function(){
			h.find('.ko').css({'background-position':'-2px -16px'})
		}});
	},
	gnb_out_anim:function(h){
		var h = $(h);
		var w = 0;
		var d = 50+h.find('.en').width()+h.find('.ko').width()*0.8;
		h.find('.ko').css({'background-position':'-2px -2px'});
		h.find('.line').stop(true).animate({'width':w},{duration:d,complete:function(){
			h.find('.line').removeClass('line_active');
			h.find('.en').css({'background-position':'-2px -2px'});
		}});
	},
	/**
	 * update.
	 * 
	 * update_ex_id : update의 이전 id.
	 * update_id : update의 현재 id.
	 * update_bool : UI클릭 에러 방지 boolean, false일 때 애니메이션 실행, true일 때 UI 차단.
	 * update_max : update의 스크롤 최대값, height 600일 때 5, 600~700일 때 4, 700이상일 때 3. 
	 * update : update element.
	 * update_nav : update navigation의 <a> 태그.
	 * update_nav_up : update navigation의 .up.
	 * update_nav_down : update navigation의 .down.
	 * update_nav_click : update navigation click event.
	 * update_nav_wheel : update wheel event.
	 * update_anim : update 스크롤링 애니메이션 i - update의 현재 id.
	 */
	update_ex_id:0,
	update_id:0,
	update_bool:false,
	update_max:$bannerMax-1,
	update:$('#update'),
	update_nav:$('#update_nav').find('a'),
	update_nav_up:$('#update_nav').find('.up'),
	update_nav_down:$('#update_nav').find('.down'),
	update_nav_click:function(){
		this.update_nav.bind('click', function(e){
			e.preventDefault();
			if(!NextUI.update_bool){
				if($(this).hasClass('up')){
					NextUI.update_id--;
				}else if($(this).hasClass('down')){
					NextUI.update_id++;
				}
				NextUI.update_anim(NextUI.update_id);
			}
		});
	},
	update_nav_wheel:function(){
		this.update.mousewheel(function(e, d) {
			e.preventDefault();
			if(d > 0) {
				NextUI.update_nav_up.trigger('click');
			}else if (d < 0){
				NextUI.update_nav_down.trigger('click');
			}
		});
	},
	update_anim:function(i){
		NextUI.update_bool = true;
		var update = this.update;
		if($h == 600){
			//배너 1개 보임
			NextUI.update_max = $bannerMax-1;
		}else if($h>600 && $h<700){
			//배너 2개 보임
			NextUI.update_max = $bannerMax-2;
		}else{
			//배너 3개 보임
			NextUI.update_max = $bannerMax-3;
		}	
		if(i<0){
			NextUI.update_id = 0;
			NextUI.update_bool = false;
		}else if(i>NextUI.update_max){
			NextUI.update_id = NextUI.update_max;
			NextUI.update_bool = false;
		}else{
			(i == 0)? NextUI.update_nav_up.css({'background-position':'0 -19px'}):NextUI.update_nav_up.css({'background-position':'0 0'}); 
			(i == NextUI.update_max)? NextUI.update_nav_down.css({'background-position':'0 -19px'}):NextUI.update_nav_down.css({'background-position':'0 0'});
			update.stop(true).animate({'top':-90*i}, {duration:300, complete:function(){
				NextUI.update_bool = false;
			}});
		}
	},
	/**
	 * sns.
	 * 
	 * sns : header의 sns element.
	 * sns_ex_id : sns의 이전 id.
	 * sns_id : sns의 현재 id.
	 * sns_bool : UI클릭 에러 방지 boolean, false일 때 애니메이션 실행, true일 때 UI 차단.
	 * sns_time : interval에 쓰일 변수.
	 * sns_nav : 
	 */
	sns:function(){
		return $('#header').find('.sns_say');
	},
	sns_ex_id:0,
	sns_id:0,
	sns_bool:false,
	sns_top:60,
	sns_time:'',
	sns_nav:function(){
		return $('#header').find('.sns_nav').find('a');
	},
	sns_nav_up:function(){
		return $('#header').find('.sns_nav').find('.up');
	},
	sns_nav_down:function(){
		return $('#header').find('.sns_nav').find('.down');
	},
	sns_nav_click:function(){
		var sns_nav = this.sns_nav();
		sns_nav.bind('click', function(e){
			e.preventDefault();
			clearInterval(NextUI.sns_time);
			if($(this).hasClass('up')){
				if(!NextUI.sns_bool){
					NextUI.sns_id--;
					if($snsMax != 1){
						NextUI.sns_anim(NextUI.sns_id);
					}
				}
			}else if($(this).hasClass('down')){
				if(!NextUI.sns_bool){
					NextUI.sns_id++;
					if($snsMax != 1){
						NextUI.sns_anim(NextUI.sns_id);
					}
				}
			}
		});
	},
	sns_nav_wheel:function(){
		var sns_up = NextUI.sns_nav_up();
		var sns_down = NextUI.sns_nav_down();
		$('.sns_say_wrap').mousewheel(function(e, d) {
			e.preventDefault();
			if(d > 0) {
				sns_up.trigger('click');
			}else if (d < 0){
				sns_down.trigger('click');
			}
		});
	},
	sns_anim:function(i){
		//alert("i="+i+"\nsns_id="+NextUI.sns_id+"\nsnsmax="+$snsMax);
		NextUI.sns_bool = true;
		var sns = this.sns();
		var t = this.sns_top;
		if(i >$snsMax-1){
			sns.find('li:nth-child(1)').css({'top':t*($snsMax)});
			sns.stop(true).animate({'top':-t*i}, {daration:300,complete:function(){
				NextUI.sns_id = 0;
				sns.css({'top':0});
				sns.find('li:first-child').css({'top':0});
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}else if(i<0){
			sns.find('li:last-child').css({'top':-t*1});
			sns.stop(true).animate({'top':t}, {daration:300,complete:function(){
				NextUI.sns_id = $snsMax-1;
				sns.css({'top':-t*NextUI.sns_id});
				sns.find('li:last-child').css({'top':t*NextUI.sns_id});
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}else{
			sns.stop(true).animate({'top':-t*i}, {duration:300, complete:function(){
				NextUI.sns_bool = false;
				NextUI.sns_interval();
			}});
		}
	},
	sns_rolling:function(){
		var sns = NextUI.sns_nav_down();
		sns.trigger('click');
	},
	sns_interval:function(){
		NextUI.sns_time = setInterval(function() {
			NextUI.sns_rolling();
		}, 5000);
	},
	main_nav:function(){
		return $('#header').find('.paddle_nav').find('[class^="paddle"]');
	},
	main_ex_id:1,
	main_id:1,
	main_bool:false,
	main_time:'',
	main_nav_click:function(){
		var main_nav = this.main_nav();
		this.main_nav_anim(1,false);
		main_nav.bind('click', function(e){
			e.preventDefault();
			var id = $(this).attr('class').replace('paddle','');
			var d = $w*0.1;
			if(!NextUI.main_bool){
				if(NextUI.main_ex_id != id){
					NextUI.main_bool = true;
					clearInterval(NextUI.main_time);
					var ex_id = NextUI.main_ex_id;
					NextUI.main_nav_anim(ex_id,true);
					$('#main'+ex_id).css({'z-index':'550'});
					$('#main'+ex_id).stop(true).animate({'left':$w*-1}, {duration:1400+d,easing:'easeInQuart'});
					NextUI.main_id = id;
					NextUI.main_nav_anim(id,false);
					$('#main'+id).css({'z-index':'600'});
					$('#main'+id).stop(true).animate({'left':0}, {duration:800+d,easing:'easeInQuart',complete:function(){
						$('#main'+ex_id).stop(true).css({'left':$w});
						NextUI.main_bool = false;
						NextUI.main_ex_id = id;
						NextUI.main_interval();
					}});
				}
			}
		});
	},
	main_nav_anim:function(i,b){
		if(b){
			$('.paddle'+i).parent().removeClass('selected').stop(true).animate({'width':24},300);
			$('.paddle'+i).stop(true).animate({'width':21},300);
			$('.paddle'+i).parent().find('.shadow_bottom').stop(true).animate({'width':21},300);
		}else{
			$('.paddle'+i).parent().addClass('selected').stop(true).animate({'width':63},300);
			$('.paddle'+i).stop(true).animate({'width':60},300);
			$('.paddle'+i).parent().find('.shadow_bottom').stop(true).animate({'width':60},300);
		}
	},
	main_rolling:function(){
		NextUI.main_id++
		if(NextUI.main_id > $bigbannerMax){
			NextUI.main_id = 1;
		}
		$('.paddle'+NextUI.main_id).trigger('click');
		this.main_interval();
	},
	main_interval:function(){
		clearInterval(NextUI.main_time);
		NextUI.main_time = setInterval(function() {
			if(!$resize_bool){
				NextUI.main_rolling();
			}
		}, 4000);
	},
	/**
	 * main event.
	 */
	main_init:function(){
		this.main_ex_id = 1;
		this.main_id = 1;
		if($bigbannerMax != 1){
			this.main_nav_click();
			clearInterval(this.main_time);
			this.main_interval();
		}
		if($bannerMax != 1){
			this.update_nav_click();
			this.update_nav_wheel();
		}
		
		if($snsMax != 1){
			this.sns_nav_click();
			this.sns_nav_wheel();
			clearInterval(this.sns_time);
			this.sns_interval();
		}
		
		
	},
	/**
	 * common event.
	 */
	init:function(){
		$(window).bind("orientationchange", function() {
			init_resize();
		});
		if(browser.ipad||browser.iphone){
			$('body').addClass('mobile');
		}
		this.logo_click();
		this.gnb_hover();
		this.gnb_click();
	}
};