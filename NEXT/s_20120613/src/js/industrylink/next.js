/**
 * industrylink next.
 */

var industrylink = {
	list_element: $('.list_view').find('.info'),
    list_hover: function(){
        this.list_element.hover(function(){
            if(!$(this).hasClass('selected')){
				$(this).addClass('select')
			}
        },function(){
			if(!$(this).hasClass('selected')){
				$(this).removeClass('select')
			}
        });
    },
	list_order: $('.list_order').find('a'),
	list_order_num:1,
	list_order_min:1,
	list_order_max:1,
	list_order_hover: function(){
        this.list_order.hover(function(){
            if(!$(this).hasClass('selected')){
				$(this).addClass('select')
			}
        },function(){
			if(!$(this).hasClass('selected')){
				$(this).removeClass('select')
			}
        });
    },
	list_order_click: function(){
        this.list_order.bind('click', function(e){
			e.preventDefault();
			industrylink.list_order_num = $(this).html()
			industrylink.list_order.removeClass('selected')
			industrylink.list_order.removeClass('select')
			$(this).addClass('selected')
        });
    },
	list_paddle_ex_num:1,
	list_paddle_num:1,
	list_paddle_min:1,
	list_paddle_max:1,
	list_paddle: $('.list_paddle').find('a'),
	list_paddle_click: function(){
        this.list_paddle.bind('click', function(e){
			e.preventDefault();
            if($(this).parent().hasClass('prev')){
				industrylink.list_paddle_num--
				if(industrylink.list_paddle_num<industrylink.list_paddle_min){
					industrylink.list_paddle_num = industrylink.list_paddle_min
				}
			}else if($(this).parent().hasClass('next')){
				industrylink.list_paddle_num++
				if(industrylink.list_paddle_num>industrylink.list_paddle_max){
					industrylink.list_paddle_num = industrylink.list_paddle_max
				}
			}
			if(industrylink.list_paddle_num == industrylink.list_paddle_min){
				$('.list_paddle').find('.prev').find('a').css({'background-position':'0 -20px','cursor':'default'})
				$('.list_paddle').find('.next').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}else if(industrylink.list_paddle_num == industrylink.list_paddle_max){
				$('.list_paddle').find('.next').find('a').css({'background-position':'0 -20px','cursor':'default'})
				$('.list_paddle').find('.prev').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}else{
				$('.list_paddle').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}
			industrylink.list_order_num = 8*industrylink.list_paddle_num-7;
			industrylink.list_order_move(industrylink.list_paddle_num)
			industrylink.list_paddle_ex_num = industrylink.list_paddle_num
        });
    },
	list_order_move:function(i){
		var num = i-1
		if(industrylink.list_paddle_num != industrylink.list_paddle_ex_num){
			if(num<industrylink.list_paddle_max){
				$('.list_order').css({'margin-left':-216*num-10})
			}
			$('.list_order').find('li:nth-child('+industrylink.list_order_num+')').find('a').trigger('click')
		}
	},
    init: function(){
		this.list_order_max = $('.list_order').find('li').length
		$('.list_order').width(27*industrylink.list_order_max);
        this.list_hover();
		this.list_order_hover();
		this.list_order_click();
		if(this.list_order_max <=8){
			$('.list_paddle').hide();
		}else{
			this.list_paddle_max = Math.ceil(this.list_order_max/8);
			this.list_paddle_click();
			$('.list_paddle').find('.prev').find('a').css({'background-position':'0 -20px','cursor':'default'})
		}
    }
}

$(function(){
	industrylink.init();
})