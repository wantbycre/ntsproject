/**
 * admission pick.
 */
var s2013 = {
	schedule_id:'',
	schedule_ex_id:'',
	schedule_chart_element:$('#s2013_schedule').find('.schedule_chart').find('a'),
	schedule_chart_hover:function(){
		var id
		this.schedule_chart_element.hover(function(){
			id = $(this).attr('class');
			if(s2013.schedule_ex_id != ''){
				if(s2013.schedule_ex_id != id){
					$('.calendar_day').find('.'+s2013.schedule_ex_id).removeClass('selected');
					s2013.calendar_out(s2013.schedule_ex_id)
				}
			}
			if(!$(this).parent().hasClass('selected')){
				$(this).parent().addClass('select');
				s2013.calendar_over(id)
			}
        },function(){
			id = $(this).attr('class');
			if(!$(this).parent().hasClass('selected')){
				$(this).parent().removeClass('select');
				s2013.calendar_out(id)
			}
			if(s2013.schedule_ex_id != ''){
				if(s2013.schedule_ex_id != id){
					$('.calendar_day').find('.'+s2013.schedule_ex_id).addClass('selected');
					s2013.calendar_over(s2013.schedule_ex_id)
				}
			}
        });
	},
	schedule_chart_click:function(){
		this.schedule_chart_element.bind('click', function(e){
			e.preventDefault();
			s2013.schedule_id = $(this).attr('class');
			if(s2013.schedule_id != s2013.schedule_ex_id){
				if(s2013.schedule_ex_id != ''){
					$('.calendar_day').find('.'+s2013.schedule_ex_id).removeClass('selected');
					s2013.calendar_out(s2013.schedule_ex_id)
				}
				$('.calendar_day').find('.'+s2013.schedule_id).addClass('selected');
				s2013.calendar_over(s2013.schedule_id)
				$('.schedule_chart').find('.selected').removeClass('selected select')
				$(this).parent().addClass('selected');
				s2013.schedule_ex_id = s2013.schedule_id;
			}
        });
	},
	calendar_over:function(id){
		var el = $('.calendar_day').find('.'+id)
		el.addClass('select')
		el.find('.ov').stop(true).fadeTo(180,1)
	},
	calendar_out:function(id){
		var el = $('.calendar_day').find('.'+id)
		el.removeClass('select')
		el.find('.ov').stop(true).fadeTo(180,0)
	},
    init: function(){
		this.schedule_chart_hover();
		this.schedule_chart_click();
    }
}

$(function(){
	s2013.init();
})