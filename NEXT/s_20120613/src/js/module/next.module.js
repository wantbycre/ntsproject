/**
 * $m_w : window min width value.
 * $m_h : window min height value.
 */
var $m_w = 1020, $m_h = 600;

$(document).ready(function(){
	init_resize();
	$('.loading').show();
	NextUI.init();
	Next.module_id = $('.selected').attr('id').replace('gnb_','');
	var url = 'json/module/'+ Next.module_id +'.json';
	Next.module_json(url);
	Next.sns_json('json/sns.json')
})