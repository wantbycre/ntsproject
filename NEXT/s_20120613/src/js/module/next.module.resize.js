/**
 * NEXT module resize. 
 */

var $r_w = {
	_default:function(w, e){
		return w;
	}
}

var $r_o = {
	_default:function(w, e){
		return (w <= $m_w)? 'scroll' : 'hidden';
	}
}

var $r_h = {
	_module:function(h, e){
		if(Next.module_max>5){
			return (h>885)? h-235:650;
		}else{
			return (h>690)? h-235:455;
		}
	}
}

var $r_t = {
	_module:function(h, e){
		if(Next.module_max>5){
			return (h>885)? Math.round((h-235)/2-(e.height()-75)/2):Math.round(650/2-(e.height()-75)/2);
		}else{
			return (h>690)? Math.round((h-235)/2-(e.height()-75)/2):Math.round(455/2-(e.height()-75)/2);
		}
	}
}

var res = {
	hor:[
		{element:$('html'),property:[{'width':{value:$r_w._default}},{'overflow-x':{value:$r_o._default}}]},
		{element:$('#header'),property:[{'width':{value:$r_w._default}}]}
	],
	ver:[
		{element:$('#module'),property:[{'height':{value:$r_h._module}}]},
		{element:$('#module_wrap'),property:[{'height':{value:$r_h._module}}]},
		{element:$('#module_container'),property:[{'height':{value:$r_h._module}}]}
	]
}

var $w, $h;
var $menu_hor_bool, $menu_ver_bool;

var init_resize = function(){
	clearInterval(NextUI.sns_time)
	var w_width = $(window).width();
	var w_height = $(window).height();
	
	$('.loading').height(w_height-235);
	
	(w_width > $m_w) ? $w = w_width : $w = $m_w;
	(w_height > $m_h) ? $h = w_height : $h = $m_h;
	
	if(!$ag_id){
		if($w==$m_w){
			$menu_hor_bool = false;
			$('.items').css({'padding-bottom':0})
			$('#header').css({'position':'absolute'})
			$('#footer').css({'position':'relative','float':'left'})
			
		}else{
			$menu_hor_bool = true;
			$('.items').css({'padding-bottom':65})
			$('#header').css({'position':'fixed'})
			$('#footer').css({'position':'fixed'})
		}
	}else{
		$menu_hor_bool = false;
		$('.items').css({'margin-bottom':0})
		$('#header').css({'position':'absolute'})
		$('#footer').css({'position':'relative','float':'left'})
	}
	if($h==$m_h){
		$menu_ver_bool = false;
	}else{
		$menu_ver_bool = true;
	}
	
	var hor_e, hor_p, hor_i, hor_j, hor_k;
	for(ver_i = 0; ver_i <res.ver.length; ver_i++){
		ver_e = res.ver[ver_i].element;
		for(ver_j = 0; ver_j <res.ver[ver_i].property.length; ver_j++){
			ver_p = res.ver[ver_i].property[ver_j];
			for(ver_k in ver_p){
				ver_e.css(ver_k, ver_p[ver_k].value($h, ver_e));
			}
		}
	}
	
	var ver_e, ver_p, ver_i, ver_j, ver_k;
	for(hor_i = 0; hor_i <res.hor.length; hor_i++){
		hor_e = res.hor[hor_i].element;
		for(hor_j = 0; hor_j <res.hor[hor_i].property.length; hor_j++){
			hor_p = res.hor[hor_i].property[hor_j];
			for(hor_k in hor_p){
				hor_e.css(hor_k, hor_p[hor_k].value($w, hor_e));
			}
		}
	}
	
	if(Next.module_bool){
		module_resize();
	}
	
	$('#main'+NextUI.main_id).css({'left':0});
	for(var i=0;i<6;i++){
		$('.sns_say').find('li:nth-child('+(i+1)+')').css({'top':40*i})
	}
	if($menu_hor_bool){
		if(!$menu_ver_bool){
			NextUI.menu_min_in();
		}else{
			NextUI.menu_min_out();
		}
	}else{
		NextUI.menu_min_out();
	}
	
	NextUI.sns_interval();
}

var module_resize = function(){
	(Next.module_max <=5)? $('.module_menu').height(280):$('.module_menu').height(576);
	$('.module_menu').css({'top':$r_t._module($h,$('.module_menu'))})
	var module_w = (Next.module_max <=5)? 200*Next.module_max:200*5;
	var module_h = (Next.module_max <=5)? 286:576;
	$('.module_menu').find('ul').width(module_w).height(module_h);
	(Next.module_max <5)? $('.module_menu').find('ul').css({'margin-left':Math.round($m_w/2-$('.module_menu').find('ul').width()/2)}):$('.module_menu').find('ul').css({'margin-left':26});
}

$(window).resize(function(){
	init_resize();
})