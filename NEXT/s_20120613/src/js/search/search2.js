/**
 * search.
 */
var search = {
    search_chart_element: $('.result_desc'),
    search_chart_hover: function(){
        this.search_chart_element.hover(function(){
            $(this).css({'background-color':'#fafafa'});
        },function(){
            $(this).css({'background-color':'transparent'});
        });
    },
    init: function(){
        this.search_chart_hover();
    }
}

$(function(){
	search.init();
})