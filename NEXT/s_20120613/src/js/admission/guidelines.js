/**
 * admission guidelines.
 */
var guidelines = {
	p:NextScroll.p(),
	print_btn:$('.print_btn'),
	print_btn_click:function(){
		this.print_btn.bind('click', function(e){
			e.preventDefault()
			var url = $(this).attr('href')
			var l = Math.round(screen.width/2 - 322)
			var t = Math.round(screen.height/2 - 400)
			
			window.open(url, 'print', 'scrollbars=yes,menubar=no,statue=no,resizeable=yes,left='+l+',top='+t+',width=645,height=800').focus();
		})
	},
	menu_id:'',
	menu_ex_id:'',
	menu_list_element:$('#guidelines_menu').find('.guidelines_btn'),
	menu_list_click:function(){
		this.menu_list_element.bind('click', function(e){
			e.preventDefault();
			guidelines.menu_id = $(this).parent().parent().attr('id').replace('_menu','')
			if(guidelines.menu_id != guidelines.menu_ex_id){
				$('.guidelines_btn').removeClass('selected')
				$(this).addClass('selected')
				$('.guidelines_body').hide();
				$('#'+guidelines.menu_id).show();
				guidelines.p.stop(true).animate({scrollTop : $('#guidelines_y2013').offset().top-$h_h}, {duration:300});
				guidelines.menu_ex_id = guidelines.menu_id
			}
		})
	},
    chart_element: $('.guidelines_chart').find('.column'),
    chart_hover: function(){
        this.chart_element.hover(function(){
            $(this).css({'background-color':'#fafafa'});
        },function(){
            $(this).css({'background-color':'transparent'});
        });
    },
    init: function(){
		this.print_btn_click();
        this.chart_hover();
		this.menu_list_click();
    }
}

$(function(){
	guidelines.init();
	$('#guidelines_schedule_menu').find('.guidelines_btn').trigger('click')
})