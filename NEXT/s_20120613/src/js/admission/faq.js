/**
 * admission faq.
 */
var faq = {
	search_btn:$('#faq_search_btn'),
	search_click:function(){
		this.search_btn.bind('click',function(e){
			e.preventDefault();
			var id= $('#faq_category').attr('value');
			$('.board_desc').hide();
			$('.board_content').hide();
			$('#board_list').find('.selected').removeClass('selected')
			$('.board_title').removeClass('selected select')
			$('#search_result').show();
			if(id != 'all'){
				$('#'+id).parent().addClass('selected');
			}
			faq.list_id = ''
			faq.list_ex_id = ''
			faq.content_id = ''
			faq.content_ex_id = ''
		})
	},
	list_id:'',
	list_ex_id:'',
    list_btn:$('#board_list').find('.list_btn'),
	list_hover: function(){
        this.list_btn.hover(function(){
			$(this).parent().addClass('select');
        },function(){
			$(this).parent().removeClass('select');
        });
    },
	list_click:function(){
		this.list_btn.bind('click',function(e){
			e.preventDefault();
			faq.list_id = this.id
			if(faq.list_ex_id != faq.list_id){
				$('.board_desc').hide()
				$('.board_content').hide();
				$('#board_list').find('.selected').removeClass('selected')
				$(this).parent().addClass('selected')
				$('#faq_'+faq.list_id).show().addClass('board_selected');
				$('.board_title').removeClass('selected select')
				faq.list_ex_id = faq.list_id
				faq.content_id = ''
				faq.content_ex_id = ''
			}
		})
	},
	content_current_id:'',
	content_ex_id:'',
	content_btn: $('.board_title').find('a'),
    content_hover: function(){
        this.content_btn.hover(function(){
			if(!$(this).parent().hasClass('selected')){
				$(this).parent().addClass('select');
			}
        },function(){
			if(!$(this).parent().hasClass('selected')){
				$(this).parent().removeClass('select');
			}
        });
    },
	content_click:function(){
		this.content_btn.bind('click', function(e){
			e.preventDefault();
			faq.content_current_id = $(this).parent().attr('id').replace('list_','')
			if(faq.content_current_id != faq.content_ex_id){
				if(!$(this).parent().hasClass('selected')){
					$(this).parent().addClass('selected')
				}
				$('#'+faq.content_current_id+'_desc').slideDown(200)
				$('#'+faq.content_ex_id).removeClass('selected select');
				$('#'+faq.content_ex_id+'_desc').slideUp(200)
				faq.content_ex_id = faq.content_current_id
			}else{
				$('#'+faq.content_ex_id).removeClass('selected select');
				$('#'+faq.content_ex_id+'_desc').slideUp(200)
				faq.content_ex_id = ''
			}
		})
	},
    init: function(){
		this.search_click();
		this.list_hover();
		this.list_click();
        this.content_hover();
		this.content_click();
    }
}

$(function(){
	$('#faq_common').show().addClass('board_selected')
	faq.list_ex_id = 'common'
	faq.init();
})