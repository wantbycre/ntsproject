/**
 * admission sizar.
 */

var siazr = {
	scroll:function(){
		scroll_y = [
				{element:$('.total').find('.shadow'),property:[{'top':{e_begin:-30,e_finish:30, s_start:$('.total').find('.shadow').offset().top-630, s_end:$('.total').find('.shadow').offset().top-230, value:$s_t.shadow}}]},
				{element:$('.notebook').find('.shadow'),property:[{'top':{e_begin:-30,e_finish:30, s_start:$('.notebook').find('.shadow').offset().top-630, s_end:$('.notebook').find('.shadow').offset().top-230, value:$s_t.shadow}}]},
				{element:$('.space').find('.shadow'),property:[{'top':{e_begin:-30,e_finish:30, s_start:$('.space').find('.shadow').offset().top-630, s_end:$('.space').find('.shadow').offset().top-230, value:$s_t.shadow}}]},
				{element:$('.supervise').find('.shadow'),property:[{'top':{e_begin:-30,e_finish:30, s_start:$('.supervise').find('.shadow').offset().top-630, s_end:$('.supervise').find('.shadow').offset().top-230, value:$s_t.shadow}}]}
		];
		
		var y_el, y_pr, y_i, y_j, y_l;
		
		var p = NextScroll.p();
		var st = p.scrollTop();
		
		for (y_i = 0; y_i < scroll_y.length; y_i++) {
			y_el = scroll_y[y_i].element;
			for (y_j = 0; y_j < scroll_y[y_i].property.length; y_j++) {
				y_pr = scroll_y[y_i].property[y_j];
				for (y_l in y_pr) {
					if (st <= y_pr[y_l].s_start) {
						y_el.css(y_l, y_pr[y_l].e_begin);
					} else if (st >= y_pr[y_l].s_end) {
						y_el.css(y_l, y_pr[y_l].e_finish);
					} else {
						y_el.css(y_l, y_pr[y_l].value(y_pr[y_l].e_begin, y_pr[y_l].e_finish, y_pr[y_l].s_start, y_pr[y_l].s_end, st));
					}
				}
			}
		}
	}
}

var $s_l = {
	shadow: function (e_begin, e_finish, s_start, s_end, st) {
		return  (5*((((st-s_start)/(s_end-s_start))<=0.5) ? ((st-s_start)/(s_end-s_start)) : 1-((st-s_start)/(s_end-s_start)))*2)+e_begin;
	}
}

var $s_t = {
	shadow:function (e_begin, e_finish, s_start, s_end, st) {
		return ((e_finish-e_begin)*((st-s_start)/(s_end-s_start)))+e_begin;
	}
}

$(function(){
	if(!$ag_id){
		$(window).bind('scroll', siazr.scroll);
	}
})