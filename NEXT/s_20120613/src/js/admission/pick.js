/**
 * admission pick.
 */
var pick = {
	nonscheduled_id:'',
	nonscheduled_ex_id:'',
	nonscheduled_chart_element:$('#pick_schedule').find('.screening_chart').find('a'),
	nonscheduled_chart_hover:function(){
		var id
		this.nonscheduled_chart_element.hover(function(){
			id = $(this).attr('class');
			if(pick.nonscheduled_ex_id != ''){
				if(pick.nonscheduled_ex_id != id){
					$('.calendar_day').find('.'+pick.nonscheduled_ex_id).removeClass('selected');
					pick.calendar_out(pick.nonscheduled_ex_id)
				}
			}
			if(!$(this).parent().hasClass('selected')){
				$(this).parent().addClass('select');
				pick.calendar_over(id)
			}
        },function(){
			id = $(this).attr('class');
			if(!$(this).parent().hasClass('selected')){
				$(this).parent().removeClass('select');
				pick.calendar_out(id)
			}
			if(pick.nonscheduled_ex_id != ''){
				if(pick.nonscheduled_ex_id != id){
					$('.calendar_day').find('.'+pick.nonscheduled_ex_id).addClass('selected');
					pick.calendar_over(pick.nonscheduled_ex_id)
				}
			}
        });
	},
	nonscheduled_chart_click:function(){
		this.nonscheduled_chart_element.bind('click', function(e){
			e.preventDefault();
			pick.nonscheduled_id = $(this).attr('class');
			if(pick.nonscheduled_id != pick.nonscheduled_ex_id){
				if(pick.nonscheduled_ex_id != ''){
					$('.calendar_day').find('.'+pick.nonscheduled_ex_id).removeClass('selected');
					pick.calendar_out(pick.nonscheduled_ex_id)
				}
				$('.calendar_day').find('.'+pick.nonscheduled_id).addClass('selected');
				pick.calendar_over(pick.nonscheduled_id)
				$('.screening_chart').find('.selected').removeClass('selected select')
				$(this).parent().addClass('selected');
				pick.nonscheduled_ex_id = pick.nonscheduled_id;
			}
        });
	},
	calendar_over:function(id){
		var el = $('.calendar_day').find('.'+id)
		el.addClass('select')
		el.find('.ov').stop(true).fadeTo(180,1)
	},
	calendar_out:function(id){
		var el = $('.calendar_day').find('.'+id)
		el.removeClass('select')
		el.find('.ov').stop(true).fadeTo(180,0)
	},
    pick_chart_element: $('#pick_evaluation_column').find('.column'),
    pick_chart_important_element: $('#pick_evaluation_column').find('.evaluation_important'),
    pick_chart_hover: function(){
        this.pick_chart_element.hover(function(){
            $(this).css({'background-color':'#fafafa'});
			$(this).find('.desc').find('span').css({'color':'#333'});
        },function(){
            $(this).css({'background-color':'transparent'});
			$(this).find('.desc').find('span').css({'color':'#555'});
        });
        this.pick_chart_important_element.hover(function(){
			$(this).css({'background-color':'#fafafa'});
			$(this).find('.desc').find('span').css({'color':'#333'});
            $('#pick_evaluation_column').find('.deem').stop(true).fadeTo(100, 1);
        },function(){
			$(this).css({'background-color':'transparent'});
			$(this).find('.desc').find('span').css({'color':'#555'});
            $(this).css({'background-color': 'transparent','color': '#555'});
            $('#pick_evaluation_column').find('.deem').stop(true).fadeTo(100, 0);
        })
    },
    init: function(){
        this.pick_chart_hover();
		this.nonscheduled_chart_hover();
		this.nonscheduled_chart_click();
    }
}

$(function(){
	$('.deem').stop(true).fadeTo(0,0);
	pick.init();
})