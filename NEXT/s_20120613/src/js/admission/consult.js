/**
 * admission consult.
 */
var consult = {
	search_board_list_order: $('#consult_search_board').find('.list_order').find('a'),
	search_board_list_order_num:1,
	search_board_list_order_min:1,
	search_board_list_order_max:1,
	search_board_list_order_hover: function(){
        this.search_board_list_order.hover(function(){
            if(!$(this).hasClass('selected')){
				$(this).addClass('select')
			}
        },function(){
			if(!$(this).hasClass('selected')){
				$(this).removeClass('select')
			}
        });
    },
	search_board_list_order_click: function(){
        this.search_board_list_order.bind('click', function(e){
			e.preventDefault();
			consult.search_board_list_order_num = $(this).html()
			consult.search_board_list_order.removeClass('selected')
			consult.search_board_list_order.removeClass('select')
			$(this).addClass('selected')
        });
    },
	search_board_list_paddle_ex_num:1,
	search_board_list_paddle_num:1,
	search_board_list_paddle_min:1,
	search_board_list_paddle_max:1,
	search_board_list_paddle: $('#consult_search_board').find('.list_paddle').find('a'),
	search_board_list_paddle_click: function(){
        this.search_board_list_paddle.bind('click', function(e){
			e.preventDefault();
            if($(this).parent().hasClass('prev')){
				consult.search_board_list_paddle_num--
				if(consult.search_board_list_paddle_num<consult.search_board_list_paddle_min){
					consult.search_board_list_paddle_num = consult.search_board_list_paddle_min
				}
			}else if($(this).parent().hasClass('next')){
				consult.search_board_list_paddle_num++
				if(consult.search_board_list_paddle_num>consult.search_board_list_paddle_max){
					consult.search_board_list_paddle_num = consult.search_board_list_paddle_max
				}
			}
			if(consult.search_board_list_paddle_num == consult.search_board_list_paddle_min){
				$('#consult_search_board').find('.list_paddle').find('.prev').find('a').css({'background-position':'0 -20px','cursor':'default'})
				$('#consult_search_board').find('.list_paddle').find('.next').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}else if(consult.board_list_paddle_num == consult.board_list_paddle_max){
				$('#consult_search_board').find('.list_paddle').find('.next').find('a').css({'background-position':'0 -20px','cursor':'default'})
				$('#consult_search_board').find('.list_paddle').find('.prev').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}else{
				$('#consult_search_board').find('.list_paddle').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}
			consult.search_board_list_order_num = 8*consult.search_board_list_paddle_num-7;
			consult.search_board_list_order_move(consult.search_board_list_paddle_num)
			consult.search_board_list_paddle_ex_num = consult.search_board_list_paddle_num
        });
    },
	search_board_list_order_move:function(i){
		var num = i-1
		if(consult.search_board_list_paddle_num != consult.search_board_list_paddle_ex_num){
			if(num<consult.search_board_list_paddle_max){
				$('#consult_search_board').find('.list_order').css({'margin-left':-216*num-10})
			}
			$('#consult_search_board').find('.list_order').find('li:nth-child('+consult.search_board_list_order_num+')').find('a').trigger('click')
		}
	},
	search_board_list_order_init: function(){
		this.search_board_list_order_max = $('#consult_search_board').find('.list_order').find('li').length
		$('#consult_search_board').find('.list_order').width(27*consult.search_board_list_order_max);
		this.search_board_list_order_hover();
		this.search_board_list_order_click();
		if(this.search_board_list_order_max <=8){
			$('#consult_search_board').find('.list_paddle').hide();
		}else{
			this.search_board_list_paddle_max = Math.ceil(this.search_board_list_order_max/8);
			this.search_board_list_paddle_click();
			$('#consult_search_board').find('.list_paddle').find('.prev').find('a').css({'background-position':'0 -20px','cursor':'default'})
		}
    },
	board_list_order: $('#consult_board').find('.list_order').find('a'),
	board_list_order_num:1,
	board_list_order_min:1,
	board_list_order_max:1,
	board_list_order_hover: function(){
        this.board_list_order.hover(function(){
            if(!$(this).hasClass('selected')){
				$(this).addClass('select')
			}
        },function(){
			if(!$(this).hasClass('selected')){
				$(this).removeClass('select')
			}
        });
    },
	board_list_order_click: function(){
        this.board_list_order.bind('click', function(e){
			e.preventDefault();
			consult.board_list_order_num = $(this).html()
			consult.board_list_order.removeClass('selected')
			consult.board_list_order.removeClass('select')
			$(this).addClass('selected')
        });
    },
	board_list_paddle_ex_num:1,
	board_list_paddle_num:1,
	board_list_paddle_min:1,
	board_list_paddle_max:1,
	board_list_paddle: $('#consult_board').find('.list_paddle').find('a'),
	board_list_paddle_click: function(){
        this.board_list_paddle.bind('click', function(e){
			e.preventDefault();
            if($(this).parent().hasClass('prev')){
				consult.board_list_paddle_num--
				if(consult.board_list_paddle_num<consult.board_list_paddle_min){
					consult.board_list_paddle_num = consult.board_list_paddle_min
				}
			}else if($(this).parent().hasClass('next')){
				consult.board_list_paddle_num++
				if(consult.board_list_paddle_num>consult.board_list_paddle_max){
					consult.board_list_paddle_num = consult.board_list_paddle_max
				}
			}
			if(consult.board_list_paddle_num == consult.board_list_paddle_min){
				$('#consult_board').find('.list_paddle').find('.prev').find('a').css({'background-position':'0 -20px','cursor':'default'})
				$('#consult_board').find('.list_paddle').find('.next').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}else if(consult.board_list_paddle_num == consult.board_list_paddle_max){
				$('#consult_board').find('.list_paddle').find('.next').find('a').css({'background-position':'0 -20px','cursor':'default'})
				$('#consult_board').find('.list_paddle').find('.prev').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}else{
				$('#consult_board').find('.list_paddle').find('a').css({'background-position':'0 0','cursor':'pointer'})
			}
			consult.board_list_order_num = 8*consult.board_list_paddle_num-7;
			consult.board_list_order_move(consult.board_list_paddle_num)
			consult.board_list_paddle_ex_num = consult.board_list_paddle_num
        });
    },
	board_list_order_move:function(i){
		var num = i-1
		if(consult.board_list_paddle_num != consult.board_list_paddle_ex_num){
			if(num<consult.board_list_paddle_max){
				$('#consult_board').find('.list_order').css({'margin-left':-216*num-10})
			}
			$('#consult_board').find('.list_order').find('li:nth-child('+consult.board_list_order_num+')').find('a').trigger('click')
		}
	},
	board_list_order_init: function(){
		this.board_list_order_max = $('#consult_board').find('.list_order').find('li').length
		$('#consult_board').find('.list_order').width(27*consult.board_list_order_max);
		this.board_list_order_hover();
		this.board_list_order_click();
		if(this.board_list_order_max <=8){
			$('#consult_board').find('.list_paddle').hide();
		}else{
			this.board_list_paddle_max = Math.ceil(this.board_list_order_max/8);
			this.board_list_paddle_click();
			$('#consult_board').find('.list_paddle').find('.prev').find('a').css({'background-position':'0 -20px','cursor':'default'})
		}
    },
	p:NextScroll.p(),
	inquiry_write:$('.post_write'),
	inquiry_btn:$('#inquiry_btn'),
	inquiry_click:function(){
		this.inquiry_btn.bind('click', function(e){
			e.preventDefault();
			consult.board.show();
			consult.search_board.hide();
			consult.board_list_ex_id = ''; 
			consult.board_list_id = '';
			consult.board.find('.post_view').hide();
			consult.board.find('.selected').removeClass('selected');
			consult.popup_area.hide();
			consult.inquiry_write.show();
			consult.p.stop(true).animate({scrollTop : $('.post_write').offset().top-$h_h-45}, {duration:300});
		})
	},
	write_complete:$('#write_complete'),
	write_complete_click:function(){
		this.write_complete.bind('click', function(e){
			e.preventDefault();
			consult.p.scrollTop($('.post_write').offset().top-$h_h-45);
			/**
			 * value 체크 - 팝업창 띄우기.
			 */
			consult.popup();
			/**
			 * 완료일때 - form 전송, 스크롤. 
			 */
			//NextScroll.content();
		})
	},
	write_cancel:$('#write_cancel'),
	write_cancel_click:function(){
		this.write_cancel.bind('click', function(e){
			e.preventDefault();
			consult.board_init();
		})
	},
	popup_area:$('#popup_area'),
	popup_alert:$('.popup_alert'),
	popup:function(){
		this.popup_area.show();
		var popup_text = this.popup_text('security');
		this.popup_alert.html(popup_text);
	},
	popup_text:function(check){
		if(check == 'name'){
			return '이름을 입력해주세요'
		}else if(check == 'email_value'){
			return '이메일 주소를 입력해주세요'
		}else if(check == 'email_incorrect'){
			return '이메일 주소가 올바르지 않습니다'
		}else if(check == 'title'){
			return '제목을 입력해주세요'
		}else if(check == 'content'){
			return '문의내용을 입력해주세요'
		}else if(check == 'pw_value'){
			return '비밀번호를 입력해주세요'
		}else if(check == 'pw_incorrect'){
			return '비밀번호가 일치하지 않습니다<br/>다시 입력해주세요'
		}else if(check == 'security'){
			return '자동 생성 방지를 위한 문자를 잘못 입력하셨습니다<br/>다시 입력해주세요'
		}
	},
	popup_confirm_btn:$('#confirm_btn'),
	popup_confirm_click:function(){
		this.popup_confirm_btn.bind('click', function(e){
			e.preventDefault();
			consult.popup_area.hide();
			consult.p.scrollTop($('.post_write').offset().top-$h_h-45);
		})
	},
	search_board:$('#consult_search_board'),
	search_board_btn:$('#consult_search_btn'),
	search_board_click:function(){
		this.search_board_btn.bind('click',function(e){
			e.preventDefault();
			/**
			 * form 전송.
			 */
			consult.search_board.show();
			consult.board.hide();
			consult.search_board_init();
		})
	},
	search_board_close_btn:$('.close_btn'),
	search_board_close_click:function(){
		this.search_board_close_btn.bind('click', function(e){
			e.preventDefault();
			consult.board.show();
			consult.search_board.hide();
			consult.board_init();
		})
	},
	search_board_list_id:'',
	search_board_list_ex_id:'',
	search_board_list_btn:$('#consult_search_board').find('.list_btn'),
	search_board_list_hover: function(){
        this.search_board_list_btn.hover(function(){
			$(this).parent().addClass('select');
        },function(){
			$(this).parent().removeClass('select');
        });
    },
	search_board_list_click:function(){
		this.search_board_list_btn.bind('click',function(e){
			e.preventDefault();
			consult.search_board_list_id = this.id.replace('list_','')
			if(consult.search_board_list_ex_id != consult.search_board_list_id){
				consult.search_board.find('.list_container').find('.selected').removeClass('selected')
				$(this).parent().addClass('selected');
				$('#'+consult.search_board_list_ex_id).hide();
				$('#'+consult.search_board_list_id).show();
				consult.search_board_list_ex_id = consult.search_board_list_id
				NextScroll.content();
			}
		})
	},
	search_board_init:function(){
		NextScroll.content();
		consult.search_board_list_ex_id = ''; 
		consult.search_board_list_id = '';
		consult.search_board_list_id = 'search_post1'
		this.search_board.find('.post_view').hide();
		$('#'+consult.search_board_list_id).show();
		this.search_board.find('.list_container').find('.selected').removeClass('selected')
		$('#list_'+consult.search_board_list_id).parent().addClass('selected');
		consult.search_board_list_ex_id = consult.search_board_list_id
		
	},
	board:$('#consult_board'),
	board_list_id:'',
	board_list_ex_id:'',
    board_list_btn:$('#consult_board').find('.list_btn'),
	board_list_hover: function(){
        this.board_list_btn.hover(function(){
			$(this).parent().addClass('select');
        },function(){
			$(this).parent().removeClass('select');
        });
    },
	board_list_click:function(){
		this.board_list_btn.bind('click',function(e){
			e.preventDefault();
			consult.inquiry_write.hide();
			consult.board_list_id = this.id.replace('list_','')
			if(consult.board_list_ex_id != consult.board_list_id){
				consult.board.find('.list_container').find('.selected').removeClass('selected')
				$(this).parent().addClass('selected');
				$('#'+consult.board_list_ex_id).hide();
				$('#'+consult.board_list_id).show();
				consult.board_list_ex_id = consult.board_list_id;
				NextScroll.content();
			}
		})
	},
	board_init:function(){
		consult.inquiry_write.hide();
		consult.board_list_ex_id = ''; 
		consult.board_list_id = '';
		consult.board_list_id = 'post1'
		this.board.find('.post_view').hide();
		$('#'+consult.board_list_id).show();
		this.board.find('.list_container').find('.selected').removeClass('selected');
		$('#list_'+consult.board_list_id).parent().addClass('selected');
		consult.board_list_ex_id = consult.board_list_id
		NextScroll.content();
	},
    init: function(){
		this.search_board_list_order_init();
		this.board_list_order_init();
		this.inquiry_click();
		this.popup_confirm_click();
		this.write_complete_click();
		this.write_cancel_click();
		this.search_board_click();
		this.search_board_close_click();
		this.search_board_list_hover();
		this.search_board_list_click();
		this.board_list_hover();
		this.board_list_click();
    }
}

$(function(){
	consult.board_init();
	consult.init();
})