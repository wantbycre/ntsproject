/**
 * Next content scroll.
 */
var NextScroll = {
	/**
	 * p : 브라우저별 scroll.
	 * top : top으로 스크롤.
	 * top_position : 해상도 크기, 스크롤에 따른 top 위치, 속성 변경.
	 * content : .items으로 스크롤.
	 */
	p:function(){
		return (browser.safari)? $('body') : $('html, body');
	},
	top:function(){
		var p = this.p();
		var d = Math.round(p.scrollTop()/10) + 500;
		p.stop(true).animate({scrollTop:0}, {duration:d});
	},
	o_h:function(){
		if($('.overview_section').html() == null){
			return 0;
		}else{
			return 600;
		}
	},
	m_top:function(){
		$('#top_wrap').css({'position':'absolute'})
		$('#top_wrap').removeClass('top_down')
		$('#top_wrap').addClass('top_m_up')
		$('#top').removeClass('top_ver_right')
		$('#top').addClass('top_m_right')
	},
	top_position:function(){
		if(!$menu_hor_bool){
			if($p >= $('.items').offset().top){
				$('#top_wrap').css({'position':'fixed'});
				$('#top').removeClass('top_right');
				$('#top').addClass('top_ver_right');
			}else{
				$('#top_wrap').css({'position':'absolute'});
				$('#top').removeClass('top_ver_right');
				$('#top').addClass('top_right');
			}
			if(NextScroll.o_h() == 0){
				$('#top_wrap').removeClass('top_o_down');
			}else{
				$('#top_wrap').removeClass('top_down');
			}
			$('#top_wrap').addClass('top_up');
		}else{
			if($p >= $('.items').offset().top+NextScroll.o_h() + 80 - ($(window).height()-40-90)){
				if($p >= $('.items').offset().top + $('.items').height()-($(window).height())){
					$('#top_wrap').css({'position':'absolute'});
				}else{
					$('#top_wrap').css({'position':'fixed'});
				}
				if(NextScroll.o_h() == 0){
					$('#top_wrap').removeClass('top_o_down');
				}else{
					$('#top_wrap').removeClass('top_down');
				}
				$('#top_wrap').addClass('top_up');
				$('#top').removeClass('top_ver_right');
				$('#top').addClass('top_right');
			}else{
				$('#top_wrap').css({'position':'absolute'});
				$('#top_wrap').removeClass('top_up');
				if(NextScroll.o_h() == 0){
					$('#top_wrap').addClass('top_o_down');
				}else{
					$('#top_wrap').addClass('top_down');
				}
				$('#top').removeClass('top_ver_right');
				$('#top').addClass('top_right');
			}
		}
	},
	content:function(){
		var p = this.p();
		p.stop(true).delay(300).animate({scrollTop : $('.items').offset().top-$h_h}, {duration:500});
	}
}

var $in_bool = false;
var $menu_min_scroll_bool = true;
var $p = 0;
var $ex = 0;

$(window).scroll(function(){
	var p = NextScroll.p();
	$p = p.scrollTop();
	//top 위치.
	if(!$ag_id){
		NextScroll.top_position();
	}
	
	if($ex > $p){
		//위로.
		if($menu_hor_bool){
			if($menu_ver_bool){
				if ($p < $('.items').offset().top - $('#module').height()/2-170) {
					NextUI.menu_min_out();
				}
				/*if(NextUI.intro_bool){
					
				}*/
			}else{
				NextUI.menu_min_in();
			}
		}else{
			NextUI.menu_min_out();
		}
	}else{
		//아래로.
		if($menu_hor_bool){
			if($menu_ver_bool){
				//if(!NextUI.intro_bool){
					if ($p >= $('.items').offset().top - $('#module').height()/2-170) {
						NextUI.menu_min_in();
					}
				/*}else{
					if ($p >= $('.items').offset().top - $('#module').height()/2-170) {
						NextUI.menu_min_in();
					}
				}*/
			}else{
				NextUI.menu_min_in();
			}
		}else{
			NextUI.menu_min_out();
		}
	}
	$ex = $p;
})