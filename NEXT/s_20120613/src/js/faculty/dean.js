/**
 * faculty dean.
 */

var dean = {
	shadow_max:0,
	scroll_y:[],
	shadow_push:function(){
		this.scroll_y = [];
		for(var i=1;i<=dean.shadow_max;i++){
			dean.scroll_y.push({element:$('#dean'+i).find('.shadow'),property:[{'top':{e_begin:-30,e_finish:30, s_start:$('#dean'+i).find('.shadow').offset().top-Math.round($h/2)-200, s_end:$('#dean'+i).find('.shadow').offset().top-Math.round($h/2)+400, value:$s_t.shadow}}]})
		}
	},
	scroll:function(){
		dean.shadow_push();
		var y_el, y_pr, y_i, y_j, y_l;
		
		var p = NextScroll.p();
		var st = p.scrollTop();
		
		for (y_i = 0; y_i < dean.scroll_y.length; y_i++) {
			y_el = dean.scroll_y[y_i].element;
			for (y_j = 0; y_j < dean.scroll_y[y_i].property.length; y_j++) {
				y_pr = dean.scroll_y[y_i].property[y_j];
				for (y_l in y_pr) {
					if (st <= y_pr[y_l].s_start) {
						y_el.css(y_l, y_pr[y_l].e_begin);
					} else if (st >= y_pr[y_l].s_end) {
						y_el.css(y_l, y_pr[y_l].e_finish);
					} else {
						y_el.css(y_l, y_pr[y_l].value(y_pr[y_l].e_begin, y_pr[y_l].e_finish, y_pr[y_l].s_start, y_pr[y_l].s_end, st));
					}
				}
			}
		}
	}
}

var $s_t = {
	shadow:function (e_begin, e_finish, s_start, s_end, st) {
		return ((e_finish-e_begin)*((st-s_start)/(s_end-s_start)))+e_begin;
	}
}

$(function(){
	if(!$ag_id){
		dean.shadow_max = $('#dean_list').find('.shadow').length;
		$(window).bind('scroll', dean.scroll);
	}else{
		$('#dean_list').find('.shadow').css({'top':30})
	}
})