var guide = {
	object_name:'guide',
	menu:{file_list:'',icon:'',guide:''},
	menu_id:'',
	menu_ex_id:'',
	guide_menu:$('[id^="menu_"]'),
	guide_menu_click:function(){
		var o = this.object_name;
		this.guide_menu.bind('click', function(){
			window[o].menu_id = this.id.replace('menu_','');
			window[o].content.hide();
			window[o].load_html(window[o].menu_id);
		})
	},
	content:$('#content'),
	load_html:function(url){
		var o = this.object_name;
		if(this.menu_ex_id != this.menu_id){
			if(window[o].menu[url] == ''){
				$.ajax({
					url:url+'.html',
					success:function(data){
						window[o].menu[url] = data;
						window[o].success_html(data);
					}
				});
			}else{
				window[o].success_html(window[o].menu[url]);
			}
		}
	},
	success_html:function(html){
		var o = this.object_name;
		this.content.html(html);
		this.content.fadeIn(180)
		window[o].menu_ex_id = window[o].menu_id;
	},
	init:function(){
		this.content.hide();
		this.guide_menu_click();
		$('#menu_file_list').trigger('click');
	}
};

$(function(){
	guide.init();
});