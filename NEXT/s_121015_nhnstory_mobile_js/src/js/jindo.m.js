if(typeof Node != "undefined"){
	var ___Old__addEventListener___ = Node.prototype.addEventListener;
	Node.prototype.addEventListener = function(type, listener, useCapture){ 
		    var callee = arguments.callee;
		    if(callee && type === "click" && this.tagName === "A"){
		        (this.___listeners___ || (this.___listeners___=[]) ).push({
		        	listener : listener,
		        	useCapture : useCapture
		        });
		    }   
		    return ___Old__addEventListener___.apply(this, arguments);	
	};
	var ___Old__removeEventListener___ = Node.prototype.removeEventListener;
	Node.prototype.removeEventListener = function(type, listener, useCapture){ 
	        var callee = arguments.callee;
		    if(callee && type === "click" && this.tagName === "A"){
		    	if(this.___listeners___) {
	        		this.___listeners___.pop();
	        	}
		    }   
		    return ___Old__removeEventListener___.apply(this, arguments);	
	};
}
if (typeof window.jindo == "undefined") {
		window.jindo = {};
}
window.jindo.m = (function() {
	var __M__ = jindo.$Class({
		$init : function() {
			this._initVar();
			this._initDeviceInfo();
			this._attachEvent();
		},
		_initVar : function() {
			this.MOVETYPE = {
				0 : 'hScroll',
				1 : 'vScroll',
				2 : 'dScroll',
				3 : 'tap',
				4 : 'longTap',
				5 : 'doubleTap'
			};
			this._isVertical = null;
			this._nPreWidth = -1;
			this._nRotateTimer = null;
			this._htHandler = {};
			this._htDeviceInfo = {};
		},
		_getOrientationChangeEvt : function(){
			var bEvtName = 'onorientationchange' in window ? 'orientationchange' : 'resize';	
			var htInfo = this.getDeviceInfo();
			if(htInfo.android && htInfo.version === "2.1") {
				bEvtName = 'resize';
			}
			return bEvtName;
		},
		_getVertical : function() {
			var bVertical = null;
			if(this._getOrientationChangeEvt() === "resize") {
				var screenWidth = document.documentElement.clientWidth;
				if(this._nPreWidth > 0) {
					if (screenWidth < this._nPreWidth) {
						bVertical = true;
					} else if (screenWidth == this._nPreWidth) {
						bVertical = this._isVertical;
					} else {
						bVertical = false;
					}
				} else {
					var screenHeight =  document.documentElement.clientHeight;	
					if (screenWidth < screenHeight) {
						bVertical = true;
					} else {
						bVertical = false;
					}
				}
				this._nPreWidth = screenWidth;
			} else {
				var windowOrientation = window.orientation;
				if (windowOrientation === 0 || windowOrientation == 180) {
					bVertical = true;
				} else if (windowOrientation == 90 || windowOrientation == -90) {
					bVertical = false;
				}			
			}
			return bVertical;
		},
		_attachEvent : function() {
			this._rotateEvent = jindo.$Fn(this._onOrientationChange, this).attach(window, this._getOrientationChangeEvt()).attach(window, "load");		
			this._pageShowEvent = jindo.$Fn(this._onPageshow, this).attach(window, "pageshow");		
		},
		_initDeviceInfo : function() {
			var sName = navigator.userAgent;
			var ar = null;
			function f(s,h) {
				return ((h||"").indexOf(s) > -1); 
			}
			this._htDeviceInfo.iphone = f('iPhone', sName);
			this._htDeviceInfo.ipad = f('iPad', sName);
			this._htDeviceInfo.android = f('Android', sName);
			this._htDeviceInfo.galaxyTab = f('SHW-M180S', sName) || f('SHW-M180K', sName) || f('SHW-M180L', sName);
			this._htDeviceInfo.galaxyK = f('SHW-M130K',sName);
			this._htDeviceInfo.galaxyU = f('SHW-M130L',sName);			
			this._htDeviceInfo.galaxyS = f('SHW-M110S',sName) ||  f('SHW-M110K',sName) ||  f('SHW-M110L',sName);
			this._htDeviceInfo.galaxyS2 = f('SHW-M250S',sName) || f('SHW-M250K',sName) || f('SHW-M250L',sName);
			this._htDeviceInfo.version = '';
			if(this._htDeviceInfo.iphone || this._htDeviceInfo.ipad){
				ar = sName.match(/OS\s([\d|\_]+\s)/i);				
				if(ar !== null&& ar.length > 1){
					this._htDeviceInfo.version = ar[1];			
				}		
			} else if(this._htDeviceInfo.android){
				ar = sName.match(/Android\s(\d\.\d)/i);
				if(ar !== null&& ar.length > 1){
					this._htDeviceInfo.version = ar[1];
				}	
			}
			this._htDeviceInfo.version = this._htDeviceInfo.version.replace(/\_/g,'.');
			for(var x in this._htDeviceInfo){
				if (typeof this._htDeviceInfo[x] == "boolean" && this._htDeviceInfo[x] && this._htDeviceInfo.hasOwnProperty(x)) {
					this._htDeviceInfo.name = x;
				}
			}
		},
		_onOrientationChange : function(we) {
			var self = this;
			if (this._getOrientationChangeEvt() === "resize") {	
				setTimeout(function(){
					self._orientationChange(we);
				}, 0);
			} else {	
				var nTime = 200;
				if(this.getDeviceInfo().android) {	
					nTime = 500;							
				} 
				clearTimeout(this._nRotateTimer);
					this._nRotateTimer = setTimeout(function() {
						self._orientationChange(we);						
				},nTime);
			}
		},
		_orientationChange : function(we) {
			var nPreVertical = this._isVertical;
			this._isVertical = this._getVertical();
			if(we.type === "load") {
				return;
			}
			if (jindo.$Agent().navigator().mobile || jindo.$Agent().os().ipad) {
				if (nPreVertical !== this._isVertical) {
					this.fireEvent("mobilerotate", {
						isVertical: this._isVertical
					});
				}
			} else {	
				this.fireEvent("mobilerotate", {
					isVertical: this._isVertical
				});
			}
		},
		bindRotate : function(fHandlerToBind) {
			var aHandler = this._htHandler["mobilerotate"];
			if (typeof aHandler == 'undefined'){
				aHandler = this._htHandler["mobilerotate"] = [];
			}
			aHandler.push(fHandlerToBind);
			this.attach("mobilerotate", fHandlerToBind);
		},
		unbindRotate : function(fHandlerToUnbind) {
			var aHandler = this._htHandler["mobilerotate"];
			if (aHandler) {
				for (var i = 0, fHandler; (fHandler = aHandler[i]); i++) {
					if (fHandler === fHandlerToUnbind) {
						aHandler.splice(i, 1);
						this.detach("mobilerotate", fHandlerToUnbind);
						break;
					}
				}
			}			
		},
		_onPageshow : function(we) {
			var self = this;
			setTimeout(function() {
				self.fireEvent("mobilePageshow", {
				});	
			},300);
		},
		bindPageshow : function(fHandlerToBind) {
			var aHandler = this._htHandler["mobilePageshow"];
			if (typeof aHandler == 'undefined'){
				aHandler = this._htHandler["mobilePageshow"] = [];
			}
			aHandler.push(fHandlerToBind);
			this.attach("mobilePageshow", fHandlerToBind);
		},
		unbindPageshow : function(fHandlerToUnbind) {
			var aHandler = this._htHandler["mobilePageshow"];
			if (aHandler) {
				for (var i = 0, fHandler; (fHandler = aHandler[i]); i++) {
					if (fHandler === fHandlerToUnbind) {
						aHandler.splice(i, 1);
						this.detach("mobilePageshow", fHandlerToUnbind);
						break;
					}
				}
			}			
		},		
		getDeviceInfo : function(){
			return this._htDeviceInfo;
		}, 
		isVertical : function() {
			return this._isVertical;
		},
		getNodeElement : function(el){
			while(el.nodeType != 1){
				el = el.parentNode;
			}
			return el;
		},
		getCssOffset : function(element){
			var curTransform  = new WebKitCSSMatrix(window.getComputedStyle(element).webkitTransform);
			return {
				top : curTransform.m42,
				left: curTransform.m41
			};
		},		
		attachTransitionEnd : function(element,fHandlerToBind) {
			var nVersion = + jindo.$Jindo().version.replace(/[a-z.]/gi,"");
			if(nVersion >= 151) {	
 				element._jindo_fn_ = jindo.$Fn(fHandlerToBind,this).attach(element, "transitionend");
			} else {
				element.addEventListener('webkitTransitionEnd', fHandlerToBind, false);	
			}
		},
		detachTransitionEnd : function(element, fHandlerToUnbind) {
			var nVersion = + jindo.$Jindo().version.replace(/[a-z.]/gi,"");
			if(nVersion >= 151) {	
 				if(element._jindo_fn_) {
 					element._jindo_fn_.detach(element, "transitionend");
 					delete element._jindo_fn_;
 				}
			} else {
				element.removeEventListener('webkitTransitionEnd', fHandlerToUnbind, false);	
			}
		},
		getCssPrefix : function() {
			var sCssPrefix = "";
			if(typeof document.body.style.MozTransition !== "undefined") {
				sCssPrefix = "Moz";
			} else if(typeof document.body.style.webkitTransition !== "undefined") {
				sCssPrefix = "webkit";
			} else if(typeof document.body.style.OTransition !== "undefined") {
				sCssPrefix = "O";
			}
			return sCssPrefix;
		}
	}).extend(jindo.Component);
	return new __M__();
})();
