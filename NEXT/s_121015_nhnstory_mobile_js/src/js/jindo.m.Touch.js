jindo.m.Touch = jindo.$Class({
	$init : function(sId, htUserOption){
		this._el = jindo.$(sId);
		var htDefaultOption = {
			nMomentumDuration :350,
			nMoveThreshold : 7,
			nSlopeThreshold : 25,
			nLongTapDuration : 1000,
			nDoubleTapDuration : 400,
			nTapThreshold : 6,
			bActivateOnload : true	
		};
		this.option(htDefaultOption);
		this.option(htUserOption || {});
		this._initVariable();		
		this._setSlope();
		if(this.option("bActivateOnload")) {
			this.activate(); 
		}
	},
	_initVariable : function(){
		this._hasTouchEvent = 'ontouchstart' in window;
		this._htMoveInfo={
			nStartX : 0,
			nStartY :0,
			nBeforeX : 0,
			nBeforeY : 0,
			nStartTime :0,
			nBeforeTime : 0
		};
		this.bStart = false;
		this.bMove = false;
		this.nMoveType = -1;		
		this.htEndInfo ={};
		this._nVSlope = 0;
		this._nHSlope = 0;
		this.bSetSlope = false;
	},
	_attachEvents : function(){
		this._htEvent = {};
		var bTouch = this._hasTouchEvent;
		this._htEvent[bTouch? 'touchstart':'mousedown'] = {
			ref: jindo.$Fn(this._onStart, this).attach(this._el, (bTouch? 'touchstart':'mousedown')),
			el : this._el
		};
		this._htEvent[bTouch? 'touchmove':'mousemove'] = {
			ref: jindo.$Fn(this._onMove, this).attach(this._el, (bTouch? 'touchmove':'mousemove')),
			el : this._el
		};
		this._htEvent[bTouch? 'touchend':'mouseup'] = {
			ref: jindo.$Fn(this._onEnd, this).attach(this._el, (bTouch? 'touchend':'mouseup')),
			el : this._el
		};
		this._htEvent["rotate"] = jindo.$Fn(this._onResize, this).bind();
		jindo.m.bindRotate(this._htEvent["rotate"]);	
		if(bTouch){
			this._htEvent['touchcancel'] = {
				ref: jindo.$Fn(this._onCancle, this).attach(this._el, 'touchcancel'),
				el : this._el
			};
		}
	},
	_detachEvents : function(){		
		for(p in this._htEvent){
			var htTargetEvent = this._htEvent[p];
			if (htTargetEvent.ref) {
				htTargetEvent.ref.detach(htTargetEvent.el, p);
			}
		}
		jindo.m.unbindRotate(this._htEvent["rotate"]);
		this._htEvent = null;
	},
	_onCancle : function(oEvent){
		this._onEnd(oEvent);		
	},
	_onStart : function(oEvent){		
		this._resetTouchInfo();
		var htInfo = this._getTouchInfo(oEvent);
		if(!this._fireCustomEvent('touchStart', {
			element : htInfo.el,
			nX : htInfo.nX,
			nY : htInfo.nY,
			oEvent : oEvent
		})){
			return;
		}
		this.bStart = true;
		//move info update
		this._htMoveInfo.nStartX = htInfo.nX;
		this._htMoveInfo.nBeforeX = htInfo.nX;
		this._htMoveInfo.nStartY = htInfo.nY;
		this._htMoveInfo.nBeforeY = htInfo.nY;
		this._htMoveInfo.nStartTime = htInfo.nTime;		
		this._startLongTapTimer(htInfo, oEvent);
	},
	_onMove : function(oEvent){		
		if(!this.bStart){
			return;	
		}
		this.bMove = true;
		var htInfo = this._getTouchInfo(oEvent);
		if(this.nMoveType < 0 || this.nMoveType == 3 || this.nMoveType == 4){
			this.nMoveType = this._getMoveType(htInfo.nX, htInfo.nY);
		}
		if((typeof this._nLongTapTimer != 'undefined') && this.nMoveType != 3){
			this._deleteLongTapTimer();
		}
		var htParam = this._getCustomEventParam(htInfo, false);		
		htParam.oEvent = oEvent;
		var nDis = 0;
		if(this.nMoveType == 0){ 
			nDis = Math.abs(htParam.nVectorX);
		}else if(this.nMoveType == 1){ 
			nDis = Math.abs(htParam.nVectorY);
		}else{ 
			nDis = Math.abs(htParam.nVectorX) + Math.abs(htParam.nVectorY);
		}		
		//move간격이 옵션 설정 값 보다 작을 경우에는 커스텀이벤트를 발생하지 않는다
		if(nDis < this.option('nMoveThreshold')){
			return; 
		}		
		if(!this.fireEvent('touchMove', htParam)){
			this.bStart = false;
			return;
		}		
		this._htMoveInfo.nBeforeX = htInfo.nX;
		this._htMoveInfo.nBeforeY = htInfo.nY;
		this._htMoveInfo.nBeforeTime = htInfo.nTime;
	},
	_onEnd : function(oEvent){
		if(!this.bStart){
			return;
		}		
		this._deleteLongTapTimer();
		if(!this.bMove && (this.nMoveType != 4)){
			this.nMoveType = 3;
		}
		if(this.nMoveType < 0){
			return;
		}
		var htInfo = this._getTouchInfo(oEvent);
		if(this._isDblTap(htInfo.nX, htInfo.nY, htInfo.nTime)){			
			clearTimeout(this._nTapTimer);
			delete this._nTapTimer;
			this.nMoveType = 5; 
		}
		var htParam = this._getCustomEventParam(htInfo, true);
		htParam.oEvent = oEvent;
		var sMoveType = htParam.sMoveType;
		if( (typeof this._htEventHandler[jindo.m.MOVETYPE[5]] != 'undefined' && (this._htEventHandler[jindo.m.MOVETYPE[5]].length > 0))&& (this.nMoveType == 3) ){
			var self = this;			
			this._nTapTimer = setTimeout(function(){
				self.fireEvent('touchEnd', htParam);
				self._fireCustomEvent(sMoveType, htParam);				
				delete self._nTapTimer;
			}, this.option('nDoubleTapDuration'));	
		}else{
			this.fireEvent('touchEnd', htParam);
			if(this.nMoveType != 4){
				this._fireCustomEvent(sMoveType, htParam);
			}
		}		
		this._updateTouchEndInfo(htInfo);		
		this._resetTouchInfo();
	},
	_fireCustomEvent :  function(sEvent, htOption){
		return this.fireEvent(sEvent, htOption);		
	},
	_getCustomEventParam : function(htTouchInfo, bTouchEnd){
		var sMoveType = jindo.m.MOVETYPE[this.nMoveType];
		var nDuration = htTouchInfo.nTime - this._htMoveInfo.nStartTime;		
		var nVectorX = nVectorY = nMomentumX = nMomentumY = nSpeedX= nSpeedY = nDisX= nDisY= 0;
		nDisX = (this.nMoveType === 1)? 0 : htTouchInfo.nX - this._htMoveInfo.nStartX; 
		nDisY = (this.nMoveType === 0)? 0 : htTouchInfo.nY -this._htMoveInfo.nStartY ; 
		nVectorX = htTouchInfo.nX - this._htMoveInfo.nBeforeX;
		nVectorY = htTouchInfo.nY - this._htMoveInfo.nBeforeY;
		if(bTouchEnd && (this.nMoveType == 0 || this.nMoveType == 1 || this.nMoveType == 2 )){
			if(nDuration <= this.option('nMomentumDuration')){
				nSpeedX = Math.abs(nDisX)/nDuration ;
				nMomentumX = (nSpeedX*nSpeedX) / 2;
				nSpeedY = Math.abs(nDisY)/nDuration ;
				nMomentumY =  (nSpeedY*nSpeedY) / 2;
			}
		}
		var htParam  = {
			element : htTouchInfo.el,
			nX : htTouchInfo.nX,
			nY : htTouchInfo.nY,
			nVectorX : nVectorX,
			nVectorY : nVectorY,
			nDistanceX : nDisX,
			nDistanceY : nDisY,
			sMoveType : sMoveType,
			nStartX : this._htMoveInfo.nStartX,
			nStartY : this._htMoveInfo.nStartY,
			nStartTimeStamp : this._htMoveInfo.nStartTime
		};
		if(bTouchEnd){
			htParam.nMomentumX = nMomentumX;
			htParam.nMomentumY = nMomentumY;
			htParam.nSpeedX = nSpeedX;
			htParam.nSpeedY = nSpeedY;
			htParam.nDuration = nDuration;
		}		
		return htParam;	
	},
	_updateTouchEndInfo : function(htInfo){
		this.htEndInfo = {
			element: htInfo.el,
			time : htInfo.nTime,
			movetype : this.nMoveType,
			nX : htInfo.nX,
			nY : htInfo.nY
		};
	},
	_deleteLongTapTimer : function(){		
		if(typeof this._nLongTapTimer != 'undefined'){
			clearTimeout(this._nLongTapTimer);
			delete this._nLongTapTimer;
		}
	},	
	_startLongTapTimer : function(htInfo, oEvent){
		var self = this;
		if((typeof this._htEventHandler[jindo.m.MOVETYPE[4]] != 'undefined') && (this._htEventHandler[jindo.m.MOVETYPE[4]].length > 0)){
			self._nLongTapTimer = setTimeout(function(){
				self.fireEvent('longTap',{
					element :  htInfo.el,
					oEvent : oEvent,
					nX : htInfo.nX,
					nY : htInfo.nY
				});				
				delete self._nLongTapTimer;
				self.nMoveType = 4;
			}, self.option('nLongTapDuration'));
		}
	},
	_onResize : function(){
		this._setSlope();
	},
	_isDblTap : function(nX, nY, nTime){
		if((typeof this._nTapTimer != 'undefined') && this.nMoveType == 3){
			var nGap = this.option('nTapThreshold');
			if( (Math.abs(this.htEndInfo.nX - nX) <= nGap) && (Math.abs(this.htEndInfo.nY-nY) <= nGap) ){
				return true;
			}
		}		
		return false;
	},
	_setSlope : function(){
		if(!this.bSetSlope){
			this._nHSlope = ((window.innerHeight/2) / window.innerWidth).toFixed(2)*1;
			this._nVSlope = (window.innerHeight / (window.innerWidth/2)).toFixed(2)*1;
		}
	},
	setSlope : function(nVSlope, nHSlope){
		this._nHSlope = nHSlope;
		this._nVSlope = nVSlope;
		this.bSetSlope = true;
	},
	getSlope : function(){
		return{
			nVSlope :  this._nVSlope,
			nHSlope : this._nHSlope
		}
	},
	_resetTouchInfo : function(){		
		for(var x in this._htMoveInfo){
			this._htMoveInfo[x] = 0;
		}		
		this.bStart = false;
		this.bMove = false;
		this.nMoveType = -1;		
	},
	_getMoveType : function(x,y){
		var nType = this.nMoveType;
		var nX = Math.abs(this._htMoveInfo.nStartX - x);
		var nY = Math.abs(this._htMoveInfo.nStartY - y);		
		var nDis = nX + nY;
		var nGap = this.option('nTapThreshold');
		if((nX <= nGap) && (nY <= nGap)){
			nType = 3;
		}else{
			nType = -1;
		}
		if(this.option('nSlopeThreshold') <= nDis){
			var nSlope = parseFloat((nY/nX).toFixed(2),10);
			if(nSlope <= this._nHSlope){
				nType = 0;
			}else if(nSlope >= this._nVSlope){
				nType = 1;
			}else {
				nType = 2;
			}			
		}
		return nType;		
	},
	_getTouchInfo : function(oEvent){
		var e = this._hasTouchEvent? oEvent.$value().changedTouches[0] : oEvent;
		var el =  this._hasTouchEvent? jindo.m.getNodeElement(e.target) : e.element;
		var x = this._hasTouchEvent? e.pageX : e.pos().pageX;
		var y = this._hasTouchEvent? e.pageY : e.pos().pageY;
		return {
			el: el,
			nX : x,
			nY : y,
			nTime : oEvent.$value().timeStamp
		}
	},
	getBaseElement : function(el){
		return this._el;
	},
	_onDeactivate : function(){
		this._detachEvents();
	},
	_onActivate : function(){
		this._attachEvents();
	},
	destroy: function() {
		this.deactivate();
		this._el = null;
		for(var p in this._htMoveInfo){
			this._htMoveInfo[p] = null;
		}
		this._htMoveInfo = null;
		for(var p in this.htEndInfo){
			this.htEndInfo[p] = null;
		}
		this.htEndInfo = null;
		this.bStart = null;
		this.bMove = null;
		this.nMoveType = null;		
		this._nVSlope = null;
		this._nHSlope = null;
		this.bSetSlope = null;
	}	
}).extend(jindo.UIComponent);
