jindo.m.book = jindo.$Class({
	sPageTemplate : '<span class="page_num" id="page_num_{idx}"></span>',
	nLength : jindo.$$(".flick-ct").length,
	nDefaultPage : 0,
	
	$init : function(){
		//this._writePage();
		//this._setPageHighlight(this.nDefaultPage);
	},
	
	/**
	 * 최초 페이징 영역 Draw
	 */
	_writePage : function(){
		var sHtml = "";
		
		for(var i=0; i<this.nLength; i++){
			sHtml +=  this.sPageTemplate.replace(/\{idx\}/g, i);
		}
		
		jindo.$Element('paging').appendHTML(sHtml);
	},
	
	/**
	 * 선택된 페이지에 대한 하이라이트 처리
	 */
	_setPageHighlight : function(nIdx){
		//this._removePageHighlight();
		this.nDefaultPage = nIdx;
		var aPageNum = jindo.$$(".page_num", jindo.$('paging'));
		
		//jindo.$Element(aPageNum[nIdx]).addClass('selected');
	},
	
	/**
	 * 선택된 페이지 스타일 제거
	 */
	_removePageHighlight : function(){
		var aPageNum = jindo.$$(".page_num", jindo.$('paging'));

		jindo.$Element(aPageNum[this.nDefaultPage]).removeClass('selected');
		//jindo.$Element(jindo.$('.page_num')).removeClass('selected');
	},
	
	/**
	 * 클로즈 버튼 노출처리 
	 */
	_showCloseBtn : function(){
		jindo.$Element('closeBtn').show();
	},
	
	/**
	 * 클로즈 버튼 숨김처리 
	 */
	_hideCloseBtn : function(){
		jindo.$Element('closeBtn').hide();
	},

	/**
	 * 좌우 전환 네비게이션 버튼 노출/제거 
 	 */
	_setNavBtn : function(nIdx){
		if(nIdx == 0){
			jindo.$Element("aprev").hide();
		} else {
			jindo.$Element("aprev").show();
		}
		
		if(nIdx == this.nLength - 1){
			jindo.$Element("anext").hide();
		} else {
			jindo.$Element("anext").show();
		}		
	},
	
	/**
	 * 플리킹 시작 관련 처리
	 */
	beforeFlicking : function(){
		this._hideCloseBtn();
	},	
	
	/**
	 * 플리킹 종료 관련 처리 
	 */
	afterFlicking : function(nIndex){
		this._setPageHighlight(nIndex);
		this._setNavBtn(nIndex);
	},
	
	/**
	 * 전체영역 클릭시 클로즈 버튼 노출 여부  
	 */
	clickFlickArea : function(bFlicking){
		if(bFlicking == false){
			this._showCloseBtn();
		} 	
	},
	
	/**
	 * 페이지 클릭 
	 */
	clickPage : function(evt){
		var sId = evt.element.id;
		var nSelectValue = sId.replace("page_num_", "");
		
		this._setPageHighlight(nSelectValue);
		
		return nSelectValue;
	}
});