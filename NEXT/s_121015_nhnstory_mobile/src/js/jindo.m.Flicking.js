jindo.m.Flicking = jindo.$Class({
	$init : function(sId,htUserOption) {
		this.option({
			bHorizontal : true,
			sClassPrefix : 'flick-',
			bActivateOnload : true,
			nDefaultIndex: 0,
			bAutoResize : true,
			nFlickThreshold : 40,
			nDuration : 200,
			bUsePreserve3dForAndorid : false
		});
		this.option(htUserOption || {});		
		this._setWrapperElement(sId);		
		this._initVar();
		this._setSize();
		this._initScroll();
		this._updateSizeInfo();		
		if(this.option("bActivateOnload")) {
			this.activate();
			this.moveTo(this.option('nDefaultIndex'), 0);
		}
	},
	_initScroll : function(){
		this._oCore = new jindo.m.CoreScroll(this._htWElement.base.$value(),{
			 bUseHScroll : this.option('bHorizontal'),
			 bUseVScroll : !this.option('bHorizontal'),
			 bUseMomentum : false,
			 bUsePreserve3dForAndorid : this.option('bUsePreserve3dForAndorid'),
			 bActivateOnload : false
		});
		this._oCore.attach({
			'beforeScroll' : jindo.$Fn(this._onBeforeScrollEnd, this).bind(),
			'afterScroll' : jindo.$Fn(this._onScrollEnd, this).bind()
		});
	},
	_initVar: function() {
		this.bHorizontal = (this.option('bHorizontal'))? true : false;	
		this.nCurrentIndex = this.option('nDefaultIndex');
		this.nNextIndex = this.nCurrentIndex;
		this.bResizeing = false;
	},
	_setWrapperElement: function(el) {
		this._htWElement = {};
		el = jindo.$(el);
		var sClass = '.'+ this.option('sClassPrefix');
		this._htWElement.base = jindo.$Element(el);
		this._htWElement.wrapper = jindo.$Element(jindo.$$.getSingle(sClass+'container',el));
		var aContents = jindo.$$(sClass+"ct", el);
		this._htWElement.aContent = jindo.$A(aContents).forEach(function(value,index, array){
			array[index] = jindo.$Element(value);
		}).$value();		
	},
	_setSize : function(){
		if(!this.option('bAutoResize')) return;
		var nLen = this._htWElement.aContent.length;
		if(this.bHorizontal){
			var nW = this._htWElement.base.width();
			this._htWElement.wrapper.width(nW *nLen);
			jindo.$A(this._htWElement.aContent).forEach(function(value){
				value.width(nW);
			});
		}else{
			var nH = this._htWElement.aContent[0].height();
			this._htWElement.wrapper.height(nH*nLen);			
		}
	},
	_updateSizeInfo : function(){		
		var sLen = this.bHorizontal? 'width' : 'height';
		this._htPosition = [];
		var nPos = 0;
		for(var i=0,nLen = this._htWElement.aContent.length; i<nLen;i++){
			if(i != 0){
				nPos += this._htWElement.aContent[i-1][sLen]()*-1;
			}			
			this._htPosition.push(nPos);			
		}		
	},
	_onBeforeScrollEnd :function(oCustomEvt){
		this.fireEvent('beforeFlicking',{
		});
		var nOrignalTime = oCustomEvt.nTime;
		var htPos = this._getSnap(oCustomEvt.nLeft, oCustomEvt.nTop, oCustomEvt.nDistanceX, oCustomEvt.nDistanceY,oCustomEvt.nMomentumX,oCustomEvt.nMomentumY);
		oCustomEvt.nNextLeft = htPos.nX;
		oCustomEvt.nNextTop = htPos.nY;
		oCustomEvt.nTime = htPos.nTime;
		this.nNextIndex = htPos.nIndex;
	},
	_onScrollEnd : function(){
		if(!this.bResizeing && (this.nNextIndex != this.nCurrentIndex)){
			this._setContentIndex(this.nNextIndex);
			this.fireEvent('afterFlicking',{
				nContentsIndex : this.getContentIndex()
			});
		}
		if(this.bResizeing){
			this.bResizeing = false;
		}
	},
	_getSnap : function(nX, nY, nDisX, nDisY,nMomX, nMomY){
		var nPosX = nPosY = nNewPos = 0;
		var nIndex = this._htWElement.aContent.length-1;
		var nCurrent = this.bHorizontal? nX : nY;
		var nDis = this.bHorizontal? nDisX : nDisY;
		var nMom = this.bHorizontal? nMomX : nMomY;
		for(var i=0,nLen = this._htPosition.length; i<nLen; i++){				
			if(nCurrent >= (this._htPosition[i])){
				nIndex = i;
				break;
			}				
		}
		if (nIndex == this.nCurrentIndex && nIndex > 0 && nDis > 0) nIndex--;
		if((Math.abs(nDis) <=  this.option('nFlickThreshold')) ){
			if(!(!jindo.m.getDeviceInfo().android && nMom != 0)){
				nIndex = this.nCurrentIndex;
			}
		}
		nNewPos = this._htPosition[nIndex];
		var nSize = Math.abs(this._htPosition[nIndex] -  this._htPosition[this.nCurrentIndex]); 	
		var nGap = nSize? Math.abs((nNewPos - nCurrent)/nSize) : 0;
		var nTime = ( !jindo.m.getDeviceInfo().android && (nMom != 0))? Math.round(this.option('nDuration')*nGap*0.5) : this.option('nDuration');		
		nTime = Math.min(nTime, this.option('nDuration'));
		return {
			nX : this.bHorizontal? nNewPos : nPosX,
			nY : this.bHorizontal? nPosY : nNewPos,
			nTime : Math.round(nTime),
			nIndex : nIndex
		}
	},
	_getPosition :function(nIndex){
		var nPosX = nPosY = 0;
		if(typeof nIndex == 'undefined'){
			nIndex = this.nCurrentIndex;
		}
		nIndex = Math.max(0,nIndex);
		nIndex = Math.min(this._htPosition.length-1, nIndex);
		return {
			nX : this.bHorizontal? this._htPosition[nIndex]: nPosX,
			nY : this.bHorizontal? nPosY : this._htPosition[nIndex]
		}		
	},
	_setContentIndex : function(nIndex){
		this.nCurrentIndex = nIndex;
	},
	getContentIndex : function(){
		return this.nCurrentIndex;
	},
	getPrevIndex : function(){
		return Math.max(this.nCurrentIndex-1,0);
	},
	getNextIndex : function(){
		return Math.min(this.nCurrentIndex+1, this._htWElement.aContent.length-1);
	},
	moveTo : function(nIndex, nTime){
		if(nIndex < 0 || nIndex > this._htWElement.aContent.length-1){
			return;
		}
		if(typeof nTime == 'undefined'){
			nTime = this.option('nDuration');
		}
		var htPos = this._getPosition(nIndex);
		this.nNextIndex = nIndex;
		this._oCore.scrollTo(htPos.nX, htPos.nY, nTime);
	},
	movePrev : function(nTime){
		this.moveTo(this.getPrevIndex());
	},
	moveNext : function(nTime){
		this.moveTo(this.getNextIndex());		
	},
	isAnimating : function(){
		return this._oCore.isMoving();
	},
	_onResize : function(evt){
		var self = this;
		this._setSize();
		this._updateSizeInfo();
		this._oCore.refresh(true);
		setTimeout(function(){
			self.bResizeing = true;
			self.moveTo(self.nCurrentIndex, 0);			
		},100);		
	},
	_onActivate : function() {
		this._oCore.activate();
		this._attachEvent();
	},
	_onDeactivate : function() {
		this._oCore.deactivate();
		this._detachEvent();
	},
	_attachEvent : function() {
		this._htEvent = {};		
		this._htEvent["rotate"] = jindo.$Fn(this._onResize, this).bind();
		jindo.m.bindRotate(this._htEvent["rotate"]);
	},
	_detachEvent : function() {
		for(p in this._htEvent) {
			var htTargetEvent = this._htEvent[p];
			if (htTargetEvent.ref) {
				htTargetEvent.ref.detach(htTargetEvent.el, p);
			}
		}
		jindo.m.unbindRotate(this._htEvent["rotate"]);
		this._htEvent = null;
	},
	destroy: function() {
		this.deactivate();
		for(var p in this._htWElement) {
			this._htWElement[p] = null;
		}
		this._htWElement = null;
		this.bHorizontal = null;
		this.nCurrentIndex = null;	
		this._htPosition = null;
	}
}).extend(jindo.UIComponent);
