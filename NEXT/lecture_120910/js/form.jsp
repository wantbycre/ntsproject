<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file ="/WEB-INF/jsp/common/taglibs.jsp" %>
<jsp:useBean id="nc" class="com.nhncorp.next.common.util.NHNCaptcha"/>
<c:set var="key"><jsp:getProperty property="key" name="nc"/></c:set>
<html lang="ko">
<head>
<title>NHN NEXT :: NHN institute for the NEXT network</title>
</head>
<body class="lecture admission2">
<div class="bg_l"></div>
<div class="bg_r"></div>
<div id="wrap">
	<!-- header -->
	<c:import url="includeHeader.jsp">
		<c:param name="tab" value="2"/>
	</c:import>
    <!-- //header -->
    <!-- container -->
	<div id="container">
		<!-- <div class="bg_ct bg_ct_l"></div><div class="bg_ct bg_ct_r"></div> -->
		<div class="bg_ct bg_ct_r"></div>
        <!-- content -->
        <div id="content">
        
        	<c:set var="saveUrl"><lucy:data path='url/schoolsession'/></c:set>
			<form name="form" method="post" action="${saveUrl}">
			
			<input type="hidden" name="seasonId" id="seasonId" value="${seasonId}"/>
			<div class="inner">
				<div class="dsc_form"><span class="blind">(필수)</span>표시는 필수입력 사항입니다.</div>
				<h2>신청서 유형 및 초청일</h2>
				<div class="tbl_type tbl_type2">
					<table cellpadding="0" cellspacing="0">
					<col width="148">
					<col width="*">
					<tbody>
					<tr>
					<th scope="row"><span>신청서 유형</span></th>
					<td>
						<ul class="lst_rdo">
						<c:forEach var="degreeCount" items="${degreeCountList}" varStatus="stat">
							<li>
								<c:if test="${degreeCount.isAdmissionDate}">
									<input type="radio" name="applyType" value="${degreeCount.degreeCountType}" id="input_rdo${stat.count}" onclick="applyTypeChange(this.value)">
									<label for="input_rdo${stat.count}">${degreeCount.degreeCountName}</label>
								</c:if>
							</li>
						</c:forEach>
						</ul>
					</td>
					</tr>
					<tr>
					<th scope="row"><span>초청일</span></th>
					<td>
						<strong id="invitationDate"></strong><input type="hidden" id="invitationDateHidden" name="invitationDate"/>
					</td>
					</tr>
					</tbody>
					</table>
				</div>

				<h2>기본 인적사항</h2>
				<div class="tbl_type">
					<table cellpadding="0" cellspacing="0">
					<col width="148">
					<col width="*">
					<tbody>
					<tr>
					<th scope="row"><label for="input_txt"><span class="blind">(필수)</span><span class="ico_ess">이름</span></label></th>
					<td>
						<input type="text" id="personName" name="personName" maxlength="20" class="input_txt" style="width:140px">
					</td>
					</tr>
					<tr>
					<th scope="row"><label for="input_txt2"><span class="blind">(필수)</span><span class="ico_ess">이메일</span></label></th>
					<td>
						<input type="hidden" id="personEmail" name="personEmail"/>
						<input type="text" id="personEmailId" name="personEmailId" maxlength="50" class="input_txt" style="width:140px">
						<span class="at">@</span>
						<div id="emailDomainSelect" class="selectbox-noscript selbox_area">
							<select id="emailDomain" onchange="emailDomainSelect(this);" class="selectbox-source">
								<option value="직접입력">직접입력</option>
								<option value="naver.com">naver.com</option>
								<option value="nate.com">nate.com</option>
								<option value="dreamwiz.com">dreamwiz.com</option>
								<option value="yahoo.co.kr">yahoo.co.kr</option>
								<option value="empal.com">empal.com</option>
								<option value="unitel.com">unitel.com</option>
								<option value="gmail.com">gmail.com</option>
								<option value="korea.com">korea.com</option>
								<option value="chol.com">chol.com</option>
								<option value="paran.com">paran.com</option>
								<option value="freechal.com">freechal.com</option>
								<option value="hanmail.net">hanmail.net</option>
								<option value="hotmail.com">hotmail.com</option>
						    </select>
							<div class="selectbox-box"><div class="selectbox-label"></div></div>
							<div class="selectbox-layer"><div class="selectbox-list"></div></div>
						</div>
						<input type="text" id="personEmailDomain" maxlength="20" class="input_txt eof" value="직접입력" onblur="helpTextOnBlur(this, '직접입력')" onfocus="helpTextOnFocus(this, '직접입력')" style="width:140px">
					</td>
					</tr>
					<tr>
					<th scope="row"><label for="sel"><span class="blind">(필수)</span><span class="ico_ess">연락처</span></label></th>
					<td>
						<div id="personContactFirstSelect" class="selectbox-noscript selbox_area">
							<select id="personContactFirst" class="selectbox-source">
								<option value="010">010</option>
								<option value="011">011</option>
								<option value="016">016</option>
								<option value="017">017</option>
								<option value="018">018</option>
								<option value="019">019</option>
						   </select>
							<div class="selectbox-box"><div class="selectbox-label"></div></div>
							<div class="selectbox-layer"><div class="selectbox-list"></div></div>
						</div>
						<span class="bar">-</span>
						<input type="text" id="personContactSecond" name="personContactSecond" maxlength="4" class="input_txt" style="width:57px">
						<span class="bar">-</span>
						<input type="text" id="personContactThird" name="personContactThird" maxlength="4" class="input_txt" style="width:57px">
						<input type="hidden" name="personContact" id="personContact"/>
					</td>
					</tr>
					<tr>
					<th scope="row"><label for="input_txt"><span class="blind">(필수)</span>
						<span class="ico_ess" id="schoolGuide">학교/전공/학년</span>
					</label></th>
					<td>
						<input type="hidden" name="schoolCode" id="schoolCode"/> 
						<input type="text" name="schoolName" id="schoolName" readonly="readonly" class="input_txt" style="width:116px">
						<button type="button" title="학교 찾기" class="btn_spr btn_find" onclick="openSchoolSearch()"><span class="blind">학교 찾기</span></button>
						<input type="text" id="majorName" name="majorName" maxlength="50" class="input_txt" style="width:116px;margin-left:-5px">
						<div id="schoolYearCodeSelect" class="selectbox-noscript selbox_area">
							<select class="selectbox-source" name="schoolYearCode">
								<option value="1학년">1학년</option>
								<option value="2학년">2학년</option>
								<option value="3학년">3학년</option>
								<option value="4학년">4학년</option>
								<option value="졸업">졸업</option>
						    </select>
							<div class="selectbox-box"><div class="selectbox-label"></div></div>
							<div class="selectbox-layer"><div class="selectbox-list"></div></div>
						</div>
					</td>
					</tr>
					<tr>
					<th scope="row"><span class="blind">(필수)</span><span class="ico_ess">관심영역</span></th>
					<td>
						<ul class="lst_chk">
						<li><input type="checkbox" name="concernCodes" id="input_chk" value="CNRN001"><label for="input_chk">취업</label></li>
						<li><input type="checkbox" name="concernCodes" id="input_chk2" value="CNRN002"><label for="input_chk2">진학</label></li>
						<li><input type="checkbox" name="concernCodes" id="input_chk3" value="CNRN003"><label for="input_chk3">전공변경</label></li>
						<li><input type="checkbox" id="etcConcernCode" name="concernCodes" value="CNRN004"><label for="etcConcernCode">기타</label></li>
						</ul>
						<input type="text" id="concernDesc" name="concernDesc" style="width: 322px;" class="input_txt eof" maxlength="50" readonly="readonly" value="기타를 선택할 경우 직접 입력해주세요." onblur="helpTextOnBlur(this, '기타를 선택할 경우 직접 입력해주세요.')" onfocus="helpTextOnFocus(this, '기타를 선택할 경우 직접 입력해주세요.')">
					</td>
					</tr>
					</tbody>
					</table>
				</div>

				<h2>경력사항</h2>
				<div class="tbl_type" style="padding-bottom:4px">
					<table cellpadding="0" cellspacing="0">
					<col width="148">
					<col width="*">
					<tbody>
					<tr>
					<th scope="row"><label for="input_txt9"><span>직장명/총 경력</span></label></th>
					<td>
						<input type="text" name="workplaceName" maxlength="50" class="input_txt eof" value="직장명" onblur="helpTextOnBlur(this, '직장명')" onfocus="helpTextOnFocus(this, '직장명')" style="width:142px">
						<input type="text" id="wholeCarrer" name="wholeCareer" maxlength="2" class="input_txt" style="width:17px">
						<span class="txt">년차</span>
					</td>
					</tr>
					<tr>
					<th scope="row" class="prize"><label for="sel2"><span>수상경력</span></label></th>
					<td>
						<ul class="lst_prize" id="awardArea">
							<li class="awardAreaClass">
								<div id="awardTypesSelect" class="selectbox-noscript selbox_area">
									<select name="awardTypes" class="selectbox-source">
										<option value="AWD001">IT</option>
										<option value="AWD002">비IT</option>
								   </select>
									<div class="selectbox-box"><div class="selectbox-label"></div></div>
									<div class="selectbox-layer"><div class="selectbox-list"></div></div>
								</div>
								<input type="text" name="awardDates" class="input_txt eof" maxlength="8" value="수상날짜" onblur="helpTextOnBlur(this, '수상날짜')" onfocus="helpTextOnFocus(this, '수상날짜')">
								<input type="text" name="awardChampionships" class="input_txt eof" maxlength="50" value="수상대회명" onblur="helpTextOnBlur(this, '수상대회명')" onfocus="helpTextOnFocus(this, '수상대회명')">
								<input type="text" name="awardGrades" class="input_txt eof" maxlength="50" value="수상등급" onblur="helpTextOnBlur(this, '수상등급')" onfocus="helpTextOnFocus(this, '수상등급')">
								<button type="button" class="btn_spr btn_formdel" title="삭제" onclick="delAwardArea(this)"><span class="blind">삭제</span></button>
								<button type="button" class="btn_spr btn_formadd" title="추가" onclick="addAwardArea(this)"><span class="blind">추가</span></button>
							</li>
						</ul>
						<p class="dsc_prize">*수상날짜는 YYYYMMDD형식으로 입력해주세요.</p>
					</td>
					</tr>
					</table>
				</div>

				<h2>참가 목적 및 지원동기</h2>
				<div class="txt_area">
					<!-- <p class="dsc_txt ico_ess"><span class="blind">(필수)</span>강연회 참가 목적 및 지원동기를 자유롭게 입력해주세요.</p> -->
					<p class="dsc_txt ico_ess"><span class="blind">(필수)</span>학교 설명회 참가 목적 및 지원동기를 자유롭게 입력해주세요.</p>
					<span class="letter" id="applyContentNum"><strong>0</strong> / 1000</span>
					<textarea id="applyContent" name="applyContent" cols="30" rows="5" class="eof" onblur="helpTextOnBlur(this, '지원동기 입력(1000자 이내)')" onfocus="helpTextOnFocus(this, '지원동기 입력(1000자 이내)')" >지원동기 입력(1000자 이내)</textarea>
				</div>

				<h2>NEXT에 궁금한 점</h2>
				<div class="txt_area" style="margin-bottom:62px">
					<p class="dsc_txt">학교 설명회를 통해 알고싶은 내용을 자유롭게 입력해주세요.</p>
					<!-- <p class="dsc_txt">강연회를 통해 알고싶은 내용을 자유롭게 입력해주세요.</p> -->
					<span class="letter" id="questionContentNum"><strong>0</strong> / 500</span>
					<textarea id="questionContent" name="questionContent" cols="30" rows="5" class="eof" onblur="helpTextOnBlur(this, '궁금한 점 입력(500자 이내)')" onfocus="helpTextOnFocus(this, '궁금한 점 입력(500자 이내)')">궁금한 점 입력(500자 이내)</textarea>
				</div>

				<h2 class="type2">* 개인정보 동의</h2>
				<p class="dsc_agree">여러분께서 기재해 주신 개인정보는 NHN NEXT의 행사초청 및 최신 정보를 제공하기 위해 전자메일, 우편 또는 전화 등을 통한 마케팅 활동 목적으로 활용될 수 있습니다. 개인정보 수집 목적달성에 필요한 기간 및 관련법령에 의한 보유기간 동안 보유하며 위 기간이 각 경과한 경우 즉시 파기합니다.<br>어떠한 경우에도 기재하신 개인정보를 제 3자에게 제공하지 않으며 작성자는 개인정보 수집 및 이용 정책에 동의하지 않을 수 있습니다.</p>
				<em class="agree"><input type="checkbox" name="personalInfoCheck" id="personalInfoCheck"><label for="personalInfoCheck">동의합니다.</label></em>

				<h2 class="type2">* 넥스트 클래스는 프로그램을 이용한 자동 신청방지를 위해서 보안절차를 거치고 있습니다.</h2>
				<div class="security">
					<div class="img"><img id="captchaimg" src="http://captcha.nhnnext.org/nhncaptcha2.gif?key=${key}"  width="269" height="145" alt=""></div>
					<p class="dsc_security">보이는 순서대로<br>숫자 및 문자를 모두 입력해 주세요.</p>
					<p class="input_area">
						<input type="hidden" id="key" name="key" value="${key}"/>
						<input type="text" id="captchavalue" name="captchavalue" class="input_txt" style="width:145px"><button type="button" class="btn_spr btn_imgrfsh" onclick="javascript:recaptcha();"><span class="blind">이미지 새로고침</span></button>
					</p>
				</div>
			</div>
			<input type="hidden" id="schoolType" value="ALL"/>
			<div class="section_btn" id="submitButton">
				<a href='javascript:save();'><img src='http://static.naver.com/b.gif' class='btn_spr2 btn_sbm3' alt='신청서 제출'></a>
			</div>
			</form>
		</div>
        <!-- //content -->
		<div class="bg_ct_f"></div>
	</div>
    <!-- //container -->
</div>
<span style="display: none;">
	<c:forEach var="item" items="${degreeCountList}">
		<span id="${item.degreeCountType}">${item.invitationDate}</span>
	</c:forEach>
</span>



<ul id="awardAreaHidden" style="display: none;">
	<li class="awardAreaClass">
		<div class="selectbox-noscript selbox_area">
			<select name="awardTypes" class="selectbox-source" >
				<option value="AWD001">IT</option>
				<option value="AWD002">비IT</option>
		   </select>
			<div class="selectbox-box"><div class="selectbox-label"></div></div>
			<div class="selectbox-layer"><div class="selectbox-list"></div></div>
		</div>
		<input type="text" name="awardDates" class="input_txt eof" value="수상날짜" maxlength="8" onblur="helpTextOnBlur(this, '수상날짜')" onfocus="helpTextOnFocus(this, '수상날짜')" >
		<input type="text" name="awardChampionships" class="input_txt eof" maxlength="50" value="수상대회명" onblur="helpTextOnBlur(this, '수상대회명')" onfocus="helpTextOnFocus(this, '수상대회명')" >
		<input type="text" name="awardGrades" class="input_txt eof" maxlength="50" value="수상등급" onblur="helpTextOnBlur(this, '수상등급')" onfocus="helpTextOnFocus(this, '수상등급')" >
		<button type="button" class="btn_spr btn_formdel" title="삭제" onclick="delAwardArea(this)"><span class="blind">삭제</span></button>
		<button type="button" class="btn_spr btn_formadd" title="추가" onclick="addAwardArea(this)"><span class="blind">추가</span></button>
	</li>
</ul>						
<script type="text/javascript" charset="utf-8" src="/js/jindo/jindo2.js"></script>
<script type="text/javascript" charset="utf-8" src="/js/jindo/jindo.Component.js"></script>
<script type="text/javascript" charset="utf-8" src="/js/jindo/jindo.UIComponent.js"></script>
<script type="text/javascript" charset="utf-8" src="/js/jindo/jindo.HTMLComponent.js"></script>
<script type="text/javascript" charset="utf-8" src="/js/jindo/jindo.RolloverArea.js"></script>
<script type="text/javascript" charset="utf-8" src="/js/jindo/jindo.RolloverClick.js"></script>
<script type="text/javascript" charset="utf-8" src="/js/jindo/jindo.Timer.js"></script>
<script type="text/javascript" charset="utf-8" src="/js/jindo/jindo.LayerManager.js"></script>
<script type="text/javascript" charset="utf-8" src="/js/jindo/jindo.LayerPosition.js"></script>
<script type="text/javascript" charset="utf-8" src="/js/jindo/jindo.SelectBox.js"></script>
<script src="/js/libs/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/session/schoolsession.js"></script>

</body>
</html>