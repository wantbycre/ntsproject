<?php include '00_ua_incl.html'; ?>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="<?php echo $uametaValue; ?>">
<title>자동차 :: 마크업 산출물</title> 
<style>
/* Common */
body,p,h1,h2,h3,h4,h5,h6,ul,ol,li,dl,dt,dd,table,th,td,form,fieldset,legend,input,textarea,button,select{margin:0;padding:0}
body,input,textarea,select,button,table{font-size:14px;font-family:'나눔고딕',NanumGothic,'맑은 고딕','Malgun Gothic','돋움',Dotum,'굴림',Gulim,Helvetica,AppleGothic,sans-serif;line-height:1.25em}
body.s,.s input,.s textarea,.s select,.s button,.s table{font-family:Helvetica}
body{position:relative;background-color:#f4f4f4;color:#000;-webkit-text-size-adjust:none}
img,fieldset{border:0}
ul,ol{list-style:none}
em,address{font-style:normal}
a{color:#000;text-decoration:none}
table{border-collapse:collapse}
hr{display:none !important}
.u_hc{visibility:hidden;overflow:hidden;position:absolute;left:-999em;width:0;height:0;font-size:0;line-height:0}
.u_vc,.u_skip{position:absolute;*left:-9999em;z-index:-1;font-size:4px;line-height:1px;color:transparent}
#ct{clear:both;width:100%;background-color:#fff}
#ct:after{display:block;clear:both;height:1px;margin-top:-1px;content:''}

/* Index */
body{padding:0 14px;background-color:#fff;word-wrap:break-word;word-break:break-all}
header{padding:1em 4em .9em .1em;border-bottom:3px solid #222}
.hd_h1{font-size:1.2em}
.hd_svc{display:block;position:absolute;top:0;right:4px;width:3em;padding:.8em 24px .7em .6em;background:url(http://static.naver.com/www/m/cm/im/bu_lk.gif) no-repeat 100% 50%;font-size:.86em;line-height:1.1em;color:#666}
.hd_wr{white-space:nowrap}
.sec_h{clear:both;padding:2em .1em .4em;border-bottom:1px solid #000;font-size:1em}
.sec_h2{clear:both;padding:1em .1em .4em 1em;border-bottom:1px dotted #888;font-size:1em;background:#e7e7e7}
.sec_h2::before{display:inline-block;margin-right:.25em;border:2px solid transparent;border-left-color:#000;vertical-align:3px;content:''}
.sec_wp{padding:0 10px 10px;border:dotted #888;border-width:0 1px 1px}
ul{font-size:.9em}
li{border-bottom:1px solid #dfdfdf}
li a{display:block;padding:.7em 31px .6em .1em;background:url(http://static.naver.com/www/m/cm/im/bu_lk.gif) no-repeat 100% 50%;_zoom:1}
.lidv{margin-top:1px;border-top:1px solid #dfdfdf}
li span{margin-left:.4em;font-size:.86em;color:#909090}
li.n a{color:#900}
footer{clear:both;margin:2.5em 0 4em;padding:6px 2px;background:#f4f4f4;font-size:.86em;line-height:1.6em;color:#333;text-align:center}
dt,dd{display:inline-block;color:#333}
dt:after{display:inline;content:': '}
dd:last-of-type{color:#aaa}
.ft_a{display:inline-block;padding:2px 2px 5px;color:#16d;letter-spacing:-1px;text-decoration:underline}
</style>
</head> 
<body class="<?php echo $uacssValue; ?>">

<header role="banner">
	<h1 class="hd_h1">자동차 :: 마크업 산출물</h1> 
	<p><a href="/ms/" title="전체 서비스" class="hd_svc">전체 <span class="hd_wr">서비스</span></a></p>
</header>

<div id="ct">
	 
	<section role="main">
		<h1 class="u_vc">마크업</h1>
		<h2 class="sec_h2">자동차스페셜 1.5차 개선(2012.11.08 최종 배포)</h2>
		<div class="sec_wp">
			<h3 class="sec_h">자동차 홈</h3>
			<ul>
			<li><a href="src/main/main.html">트렌드 강조형A(시승기,리뷰,테마 적용)<span>/ main.html</span></a></li>
			<li class="n"><a href="src/main/main_trend_2.html">트렌드 강조형B(시승기,리뷰,테마 적용)<span>/ main_trend_2.html</span></a></li>
			<li class="n"><a href="src/main/main_trend_3.html">트렌드 강조형C(테마 적용)<span>/ main_trend_3.html</span></a></li>
			<li class="n"><a href="src/main/main_trend_4.html">트렌드 강조형D(뉴스 적용)<span>/ main_trend_4.html</span></a></li>
			</ul>			
		</div>

		<h2 class="sec_h2">자동차스페셜 1차 개선(2012.11.01 배포)</h2>
		<div class="sec_wp">
			<h3 class="sec_h">시승기/뉴스</h3>
			<ul>
			<li class="n"><a href="src/lst/lst_ride.html">시승기 > 리스트 <span>/ lst_ride.html</span></a></li>
			<li class="n"><a href="src/lst/lst_news.html">뉴스 > 리스트 <span>/ lst_news.html</span></a></li>
			<li class="n"><a href="src/end/end_ride.html">시승기 > 엔드 페이지<span>/ end_ride.html</span></a></li>
			<li class="n"><a href="src/end/end_news.html">뉴스 > 엔드 페이지<span>/ end_news.html</span></a></li>
			</ul>

			<h3 class="sec_h">스페셜</h3>
			<ul>
			<li class="n"><a href="src/lst/lst_theme.html">테마 > 리스트 <span>/ lst_theme.html</span></a></li>
			<li class="n"><a href="src/lst/lst_evt.html">이벤트 > 리스트 <span>/ lst_evt.html</span></a></li>
			<li class="n"><a href="src/end/end_theme.html">테마 > 엔드 페이지<span>/ end_theme.html</span></a></li>
			<li class="n"><a href="src/end/end_evt.html">이벤트 > 엔드 페이지<span>/ end_evt.html</span></a></li>
			</ul>
		</div>

		<h2 class="sec_h2">모터쇼/레이싱(2012.11.08 최종 배포)</h2>
		<div class="sec_wp">		
			<h3 class="sec_h">메인</h3>
			<ul> 
			<li><a href="src/main/show/main_show.html">메인 <span>/ main_show.html</span></a></li> 
			<li><a href="src/main/show/main_show_layer.html">메인_탭(레이어 포함) <span>/ main_show_layer.html</span></a></li> 
			<li><a href="src/main/show/main_show_off.html">메인_탭2(레이어 포함) <span>/ main_show_off.html</span></a></li> 
			<li><a href="src/main/show/main_show_notalk.html">메인_ 토크(데이터 없는 경우) <span>/ main_show_notalk.html</span></a></li> 
			</ul>
			
			<h3 class="sec_h">탐방기</h3>
			<ul> 
			<li><a href="src/show/lst_show_tour.html">탐방기 > 리스트 <span>/ lst_show_tour.html</span></a></li> 
			<li><a href="src/end/show/end_show_tour.html">탐방기 > 엔드 페이지 <span>/ end_show_tour.html</span></a></li> 
			</ul>  
			 
			<h3 class="sec_h">이미지</h3>
			<ul> 
			<li><a href="src/end/show/end_show_img.html">이미지 > 전체 <span>/ end_show_img.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t2.html">이미지 > 자동차 <span>/ end_show_img_t2.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t2_layer.html">이미지 > 자동차(레이어 펼침) <span>/ end_show_img_t2_layer.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t2_selected.html">이미지 > 자동차(레이어 선택) <span>/ end_show_img_t2_selected.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t3.html">이미지 > 피플 <span>/ end_show_img_t3.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t3_layer.html">이미지 > 피플(레이어 펼침) <span>/ end_show_img_t3_layer.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t3_selected.html">이미지 > 피플(레이어 선택) <span>/ end_show_img_t2_selected.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t4.html">이미지 > 브랜드 <span>/ end_show_img_t4.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t4_layer.html">이미지 > 브랜드(레이어 펼침) <span>/ end_show_img_t4_layer.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t4_selected.html">이미지 > 브랜드(레이어 선택) <span>/ end_show_img_t4_selected.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t5.html">이미지 > 현장 <span>/ end_show_img_t5.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t5_layer.html">이미지 > 현장(레이어 펼침) <span>/ end_show_img_t5_layer.html</span></a></li> 
			<li><a href="src/end/show/end_show_img_t5_selected.html">이미지 > 현장(레이어 선택) <span>/ end_show_img_t5_selected.html</span></a></li> 
			</ul>

			<h3 class="sec_h">동영상</h3>
			<ul> 
			<li><a href="src/end/show/end_show_movie.html">동영상 <span>/ end_show_movie.html</span></a></li> 
			<li><a href="src/end/show/end_show_movie_lon.html">동영상 > 이전 동영상 있는 경우<span>/ end_show_movie_lon.html</span></a></li> 
			<li><a href="src/end/show/end_show_movie_ron.html">동영상 > 다음 동영상 있는 경우<span>/ end_show_movie_ron.html</span></a></li> 
			<li><a href="src/end/show/end_show_movie_on.html">동영상 > 이전/다음 동영상 있는 경우 <span>/ end_show_movie_on.html</span></a></li> 
			</ul>

			<h3 class="sec_h">뉴스</h3>
			<ul> 
			<li><a href="src/show/lst_show_news.html">뉴스 > 주요뉴스 리스트 <span>/ lst_show_news.html</span></a></li> 
			<li><a href="src/show/lst_show_news1.html">뉴스 > 관련뉴스 리스트 <span>/ lst_show_news1.html</span></a></li> 
			<li><a href="src/end/show/end_show_news.html">뉴스 > 엔드 페이지 <span>/ end_show_news.html</span></a></li> 
			</ul>

			<h3 class="sec_h">토크</h3>
			<ul> 
			<li><a href="src/end/show/end_show_talk.html">토크 > 엔드 페이지 <span>/ end_show_talk.html</span></a></li> 
			</ul>
		</div>
		
		<h2 class="sec_h2">자동차</h2>
		<div class="sec_wp">
			<h3 class="sec_h">자동차</h3>
			<ul>
			<li><a href="src/main/main.html">자동차 홈<span>/ main.html</span></a></li>
			</ul>
			
			<h3 class="sec_h">검색</h3>
			<ul>
			<li><a href="src/search/search_layer.html">조건검색 > 레이어 <span>/ search_layer.html</span></a></li> 
			<li><a href="src/search/search_result.html">조건검색 > 결과 리스트 <span>/ search_result.html</span></a></li>
			<li><a href="src/search/search_result2.html">조건검색 > 결과 리스트2 <span>/ search_result2.html</span></a></li>
			<li><a href="src/search/search_no_result.html">조건검색 > 결과 없는 경우 리스트 <span>/ search_no_result.html</span></a></li>
			<li><a href="src/search/search_no_result2.html">조건검색 > 결과 없는 경우 리스트2 <span>/ search_no_result2.html</span></a></li>
			</ul>

			<h3 class="sec_h">시판모델&단종모델</h3>
			<ul>
			<li><a href="src/product/product_sale_kind.html">시판모델(차종 별)<span>/ product_sale_kind.html</span></a></li> 
			<li><a href="src/product/product_nosale_kind.html">단종모델(차종 별)<span>/ product_nosale_kind.html</span></a></li> 
			<li><a href="src/product/product_sale_make.html">시판모델(제조사 별)<span>/ product_sale_make.html</span></a></li> 
			<li><a href="src/product/product_nosale_make.html">단종모델(제조사 별)<span>/ product_nosale_make.html</span></a></li> 
			</ul>
					
			<h3 class="sec_h">모델비교</h3>
			<ul>
			<li><a href="src/model/model_vs.html">상세 모델명 > 레이어<span>/ model_vs.html</span></a></li> 
			<li><a href="src/model/model_vs_layer1.html">다른 모델 선택 > 레이어<span>/ model_vs_layer1.html</span></a></li> 
			<li><a href="src/model/model_vs2.html">인기모델 비교하기(요약정보,제원/성능,옵션) > 레이어<span>/ model_vs2.html</span></a></li> 
			<li><a href="src/product/product_nosale_make.html">단종모델(제조사 별)<span>/ product_nosale_make.html</span></a></li> 
			</ul>
			
			<h3 class="sec_h">엔드 페이지 > 전체</h3>
			<ul>
			<li><a href="src/end/end_all.html">전체 > 이미지,제원/가격,전문가 리뷰,블로거 리뷰,자동차 토크(없는 경우 포함),인기모델  <span>/ end_all.html</span></a></li>
			<li><a href="src/end/end_all_1.html">전체 > 이미지,제원/가격,블로거 리뷰,자동차 토크(없는 경우 포함),인기모델 <span>/ end_all_1.html</span></a></li>
			<li><a href="src/end/end_all_2.html">전체 > 이미지,제원/가격,전문가 리뷰,자동차 토크(없는 경우 포함),동급모델 <span>/ end_all_2.html</span></a></li>
			<li><a href="src/end/end_all_3.html">전체 > 이미지,제원/가격,자동차 토크(없는 경우 포함),동급모델 <span>/ end_all_3.html</span></a></li>
			</ul>

			<h3 class="sec_h">엔드 페이지 > 제원/가격</h3>
			<ul>
			<li><a href="src/end/end_price.html">제원/가격(레이어 포함) <span>/ end_price.html</span></a></li>
			<li><a href="src/end/end_price_pop.html">제원/가격 > 상세모델 별 가격 및 옵션(팝업)<span>/ end_price_pop.html</span></a></li>
			</ul>

			<h3 class="sec_h">엔드 페이지 > 이미지</h3>
			<ul>
			<li><a href="src/end/end_image.html">이미지 <span>/ end_image.html</span></a></li>
			</ul>

			<h3 class="sec_h">엔드 페이지 > 동영상</h3>
			<ul>
			<li><a href="src/end/end_movie.html">동영상 <span>/ end_movie.html</span></a></li>
			<li><a href="src/end/end_movie_2.html">동영상 > 하위 탭 메뉴 <span>/ end_movie_2.html</span></a></li>
			</ul>

			<h3 class="sec_h">엔드 페이지 > 360view</h3>
			<ul>
			<li><a href="src/end/end_360view.html">360view <span>/ end_360view.html</span></a></li>
			</ul>

			<h3 class="sec_h">엔드 페이지 > 리뷰</h3>
			<ul>
			<li><a href="src/end/end_review.html">리뷰 > 전체(전문가/블로거) <span>/ end_review.html</span></a></li>
			<li><a href="src/end/end_review_1.html">리뷰 > 전문가 <span>/ end_review_1.html</span></a></li>
			<li><a href="src/end/end_review_2.html">리뷰 > 블로거 <span>/ end_review_2.html</span></a></li>
			</ul>

			<h3 class="sec_h">엔드 페이지 > 자동차 토크</h3>
			<ul>
			<li><a href="src/end/end_talk.html">자동차 토크(레이어 포함) <span>/ end_talk.html</span></a></li>
			<li><a href="src/end/end_notalk.html">자동차 토크 > 데이터 없는 경우 <span>/ end_notalk.html</span></a></li>
			</ul>

			<h3 class="sec_h">엔드 페이지 > 동영상</h3>
			<ul>
			<li><a href="src/end/end_image.html">이미지 <span>/ end_movie.html</span></a></li>
			</ul>

			<h3 class="sec_h">공지사항</h3>
			<ul>
			<li><a href="src/end/end_image.html">공지사항 > 리스트 <span>/ end_movie.html</span></a></li>
			<li><a href="src/end/end_image.html">공지사항 > 엔드 페이지 <span>/ end_movie.html</span></a></li>
			</ul>
		</div>
	</section>
	
	<section>
		<h1 class="sec_h">가이드</h1> 
		<ul> 
		<li><a href="00_uio.html">Wire frame <span>/ 00_uio.html</span></a></li>
		<li>
			<?php
			$getUri = explode("/", $_SERVER['REQUEST_URI']);
			$svn = 'http://svn.nhndesign.com/svnfiles/N_mobile/services/'.$getUri[4].'/'.$getUri[5].'/';
			?>
			<a href="<?php echo $svn;?>"><?php echo $svn;?></a>
		</li>
		</ul>
	</section>
</div>
 
<footer role="contentinfo">
	<dl>
	<dt>마크업 담당자</dt>
	<dd>박종호</dd>
	</dl>
</footer> 

</body>
</html>